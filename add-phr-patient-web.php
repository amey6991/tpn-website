<?php

//require_once 'checkSession.php'; //Loading AdminSession class File
//require_once 'classes/class.EmrUser.php'; // Loading AdminUser Class File
//require_once 'class.PhrUser.php';
require_once 'function.php';
//require_once 'commonFunctions.php'; // Loading common functions file
//$userObj = new EmrUser(); // Initilazing AdminUser Class

//$addPHR = isset($aFields['phrSubmit'])   ? $aFields['phrSubmit'] : ''; // checking add type
$sIdBeforePayment = isset($_GET['paymentid']) ? $_GET['paymentid'] : '';

$phrObj = new custom_PHR();
$temp = []; // declaring array variable

$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
$conn=db_connect('tpn');
$conn   =  $DBMan->getConnInstance(); // Requiring connection instance
$aFields =array();
$aFields=$phrObj->getDataBeforePayment($sIdBeforePayment);
//var_dump($sIdBeforePayment);
	if(!empty($aFields)){ // creating temp array for data processing
		$temp['ePhrType']          = inputPostWeb($aFields[0]['phrType']);
		$temp['sChildPrivilege']   = inputPostWeb($aFields[0]['childPrivilege']);
		$temp['iPlan']             = inputPostWeb($aFields[0]['phrPlan']);
		$temp['sName']             = inputPostWeb($aFields[0]['phrName']);

		$temp['sBdate']            = date('Y-m-d',strtotime($aFields[0]['phrBdate'])); 
		//var_dump($temp['sBdate']);
		$temp['iAge']              = calculateAge($temp['sBdate']);
		//var_dump($temp['iAge']);
		$temp['eGender']           = inputPostWeb(isset($aFields[0]['phrGender']) ? $aFields[0]['phrGender']: '');
		$temp['sUserImage']        = inputPostWeb($aFields[0]['sUserImage']);
		$temp['sContact']          = inputPostWeb($aFields[0]['phrContact']);
		$temp['sEmail']            = inputPostWeb($aFields[0]['phrEmail']);
		$temp['sAddress']          = inputPostWeb($aFields[0]['phrAddress']);
		$temp['sArea']             = inputPostWeb($aFields[0]['phrArea']);
		$temp['iPincode']             = inputPostWeb($aFields[0]['pincode']);
		$temp['iCountry']          = inputPostWeb($aFields[0]['country']);
		$temp['iState']            = inputPostWeb($aFields[0]['state']);
		$temp['iCity']             = inputPostWeb($aFields[0]['city']);
		$temp['sSecParentName']    = inputPostWeb($aFields[0]['phrSecParentName']);
		$temp['sSecParentBdate']   = date('Y-m-d',strtotime($aFields[0]['phrSecParentBdate']));
		//var_dump($temp['sSecParentBdate']);
		$temp['iSecParentAge']     = calculateAge($temp['sSecParentBdate']);
		//var_dump($temp['iSecParentAge']);
		$temp['eSecParentGender']  = inputPostWeb(isset($aFields[0]['phrSecParentGender'])?$aFields[0]['phrSecParentGender']: '');
		$temp['sSecParentImage']   = inputPostWeb($aFields[0]['sSecParentImage']);
		$temp['sSecParentContact'] = inputPostWeb($aFields[0]['phrSecParentContact']);
		$temp['sSecParentEmail']   = inputPostWeb($aFields[0]['phrSecParentEmail']);
		$temp['sGuardianName']     = inputPostWeb($aFields[0]['phrGuardianName']);
		$temp['sGuardianBdate']    = date('Y-m-d',strtotime($aFields[0]['phrGuardianBdate']));
		//var_dump($temp['sGuardianBdate']);
		$temp['iGuardianAge']      = calculateAge($temp['sGuardianBdate']);
		//var_dump($temp['iGuardianAge']);
		$temp['eGuardianGender']   = inputPostWeb(isset($aFields[0]['phrGuardianGender'])?$aFields[0]['phrGuardianGender']:'');
		$temp['sGuardianImage']    = inputPostWeb($aFields[0]['sGuardianImage']);
		$temp['sGuardianContact']  = inputPostWeb($aFields[0]['phrGuardianContact']);
		$temp['sGuardianEmail']    = inputPostWeb($aFields[0]['phrGuardianEmail']);
	}
	
	
	$addPhrResult = $phrObj->addPhrUser($temp); //calling function to add user
	
	$sMsg = null;
	if($addPhrResult){		
		$sMsg = $sIdBeforePayment;		
	}		 
	else {		
		$sMsg = "E4";		
	}

	redirectWithAlert("success-signup.php", $sMsg);
	//var_dump($sMsg);
	//exit();


   	

?>