<?php

class custom_PHR{

	public 	$iPhrId; // Logged in Id of Phr
	public  $sPhrName; //Phr name
	public  $iPhrPlan; //Phr name
	public 	$sPhrUserName; // Phr User name
	public 	$eIsPhrMember; // Phr member/non-member
	public 	$sPhrChildPriviage; // Phr child privilage of adding childrens single/multiple
	private $sPhrPassword; // Phr Password
	public  $sPhrBdate;// Phr Bdate
	public  $ePhrGender; // Phr Gender
	//public  $sUserImage;// Phr User Image 
	public  $sContact; // Phr contact no
	public  $sEmail; // Phr Email
	public  $iCountry; // Phr Country
	public  $iState; // Phr State
	public  $iCity; // Phr city 
	public  $iZone; // Phr Zone;
	public  $iPhrStatus; // Status of Phr Active/Inactive
	public  $sPhrMemberRegNo; //Registration No of Phr
	public  $sCreatedOn; // User created date time
	public  $sLoggedInUserType; //Loggined in user type PHR
	public  $sLastError; // Last Error Occured
	public  $iChildId; //Logged in user's first child id
	public  $sPhrTable = 'tpn_phr_user'; // Phr user Table
	public  $sPhrProfileTable ='tpn_phr_member_info'; // Phr member info table
	public  $sPhrMemberTable ='tpn_phr_member'; // Phr member Plan validity table.
	public  $sPhrTableBPayment = 'tpn_phr_user_before_payment'; // Phr user Table
	public  $sPhrProfileTableBPayment ='phr_member_info_before_payment'; // Phr member info table
	//public  $sUserImage;


/* Function save user image in UserImage Folder
    * return a Unique name for the image
    */
    public function saveNgetImage($sImage,$iUserId,$sLastImage=''){

    	$target = 'UserImages'; // destination folder

		if(!empty($sLastImage)){
		    $sFilename = $target.$sLastImage;
		    if(file_exists($sFilename))
		        unlink($sFilename); 
		}

	    if(!empty($sImage['name'])){
	       // $name   ='PHR-'.'USER_'.$iUserId.'-'.time().'-'.$sImage['name'];
	        $name     = $sImage['name'];
	        $tmp_name = $sImage['tmp_name'];

            $pathAndName = $target."/".$name;

	        $upload = move_uploaded_file($tmp_name,$pathAndName);

	    
	        if($upload==true)
	          return $name;
	        else
	          return FALSE;    
	    }else {
	        $sImage  = '';
	        return $sImage;
	    }

    } // EOF

/* Function Rename user image with their unize id after Payment was done, UserImage Folder
    * return a Unique name for the image
    */
    public function renameUserImage($sOldName,$iUserId){

    	$target = 'UserImages/'; // destination folder
    	
    	
		if(!empty($sOldName)){
		    $sFilename = $target.$sOldName;

			$aSymbol=array(" ","(",")",".","|",":",",","-");
			//$sText=str_replace($aSymbol, $aSymbol[0], $aPostData[1]);	
			$aOldName=explode($aSymbol[3], $sOldName);
			$sOldName=$aOldName[0].'.'.$aOldName[1];

			$sNewName   ='PHR-'.'USER_'.$iUserId.'-'.time().'-'.$aOldName[0].'.'.$aOldName[1];
			$sNewName=$target.$sNewName;
			//var_dump($sNewName);
			$result=rename($sFilename, $sNewName);

			//var_dump($result);
			
		    if($result==true ||$result == 1){
		    	$iValue=$sNewName;
		    }else{
		    	$iValue=0;
		    }	    
		        
		}else{
				$iValue=0;
		}
		return $iValue;

    } // EOF


/* Function call to generate random password
   	 */

   	function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = []; //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}

	  

/* Function to generate member phr registartion no dynamically
* return $phrMemRegNo
*/
function createDynamicPhrMemberRegNo(){

        $DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
        $conn   =  $DBMan->getConnInstance(); // Requiring connection instance

        $prefix = "Phr.mem";

        $sPhrTableFields = $this->getPhrProfileTableNFields();
        extract($sPhrTableFields);

        $sQuery = "SELECT COUNT(`{$regNo}`) FROM `{$sProfileTable}` WHERE `{$regNo}` LIKE '%$prefix%'";
        $result  = $conn->query($sQuery);
        $count   = $result->fetch_all();

        //! Displaying Count in 3 digits
        $suffix   = str_pad($count[0][0],3,'0',STR_PAD_LEFT);
        
        //! $phrMemRegNo = concatenate($prefix,$suffix )
        $phrMemRegNo = $prefix.$suffix;
        return $phrMemRegNo;
   }


 /* Function get phr Table name 
	* @param iType
	* return Table name & field name.
	*/

	public function getPhrProfileTableNFields(){
		  $temp2 = []; //Declaring Empty Variable 
		
		  $temp2['sProfileTable']    = $this->sPhrProfileTable; //storing phr user table
		  $temp2['phrUserId']        = 'phr_user_id'; 
		  $temp2['regNo']            = 'phr_member_registration_no';
		  $temp2['name']             = 'phr_mem_name_signinparent';
		  $temp2['gender']           = 'phr_mem_gender_signinparent';
		  $temp2['bdate']            = 'phr_mem_dob_signinparent';
		  $temp2['age']              = 'phr_mem_age_signinparent';
		  $temp2['contact']          = 'phr_mem_mob_signinparent';
		  $temp2['email']            = 'phr_mem_email_signinparent';
		  $temp2['address']          = 'phr_mem_address_signinparent';
		  $temp2['area']             = 'phr_mem_area_signinparent';
		  $temp2['state']            = 'phr_mem_state_id_signinparent';
		  $temp2['city']             = 'phr_mem_city_id_signinparent';
		  $temp2['country']          = 'phr_mem_country_id_signinparent';
		 	/*
		  $temp2['image']            = 'phr_mem_image_signinparent';
		  $temp2['secParentName']    = 'phr_2ndparent_name';
		  $temp2['secParentGender']  = 'phr_2ndparent_gender';
		  $temp2['secParentBdate']   = 'phr_2ndparent_dob';
		  $temp2['secParentAge']     = 'phr_2ndparent_age';
		  $temp2['secParentContact'] = 'phr_2ndparent_mob';
		  $temp2['secParentEmail']   = 'phr_2ndparent_email';
		  $temp2['secParentImage']   = 'phr_2ndparent_image';
		  $temp2['guardianName']     = 'phr_guardian_name';
		  $temp2['guardianGender']   = 'phr_guardian_gender';
		  $temp2['guardianBdate']    = 'phr_guardian_dob';
		  $temp2['guardianAge']      = 'phr_guardian_age';
		  $temp2['guardianContact']  = 'phr_guardian_mob';
		  $temp2['guardianEmail']    = 'phr_guardian_email';
		  $temp2['guardianImage']    = 'phr_guardian_image';
		  */
		  $temp2['phrCreatedOn']     = 'phr_member_createdatetime';

		  return $temp2;

	} //EOF


 /* Function to add PHR Patient Users
   	* @param array $record
	* return TRUE/FALSE
	*/

	public function addPhrUser($record = []){


        $DBMan = new DBConnManager(); // Initialzing DBConnManager() Class;
		$conn  = $DBMan->getConnInstance(); // Requiring connection insatnce

        $sPhrMemberRegNo      = ''; //member registration no
        $iPhrAge              = ''; //phr age
        $sPhrAddress          = ''; //phr Address
        $sPhrArea             = ''; //phr area
        $sPhrCreatedOn        = ''; // phr created date time
		/*
		$sPhrSecParentName	  = ''; // Second Parent name
		$ePhrSecParentGender  = ''; // Second Parent Gender
		$sPhrSecParentBdate	  = ''; // Second Parent B'day
		$sPhrSecParentAge 	  = ''; // Second Parent Age
		$sPhrSecParentContact = ''; // Second Parent Contact No
		$sPhrSecParentEmail	  = ''; // Second Parent Email
		$sPhrSecParentImage   = ''; //  Second Parent Image
		$sPhrGuardianName     = ''; // Guardian name
		$ePhrGuardianGender	  = ''; // Guardian Gender
		$sPhrGuardianBdate	  = ''; // Guardian B'day
		$sPhrGuardianAge	  = ''; // Guardian Age
		$sPhrGuardianContact  = ''; // Guardian Contact
		$sPhrGuardianEmail	  = ''; // Guardian Email
		$sPhrGuardianImage	  = ''; // Guardian Image
		*/
		$sPhrProfileCreatedOn = ''; // phr profile createdOn  
		

		$this->eIsPhrMember      = $record['ePhrType']; // storing phr type
		$this->sPhrChildPrivilege = $record['sChildPrivilege'];
		$this->sPhrName          = $record['sName']; 
		$this->iPhrPlan         = $record['iPlan']; //storing patient name
		$sPhrMemberRegNo     = $this->createDynamicPhrMemberRegNo();
		$sPhrCreatedOn           = date('Y:m:d H:i:s');
		$this->ePhrGender        = $record['eGender'];
		$this->sPhrBdate         = $record['sBdate'];
		$iPhrAge                 = $record['iAge'];
		$this->sContact          = $record['sContact'];
		$this->sEmail            = $record['sEmail'];
		$sPhrAddress             = $record['sAddress'];
		$sPhrArea                = $record['sArea'];
		$this->iState            = $record['iState'];
		$this->iCity             = $record['iCity'];
		$this->iCountry          = $record['iCountry'];
		/*
		$this->sUserImage        = $record['sUserImage'];
		$sUserImage				 = $record['sUserImage'];
		$sPhrSecParentName	     = $record['sSecParentName'];
		$ePhrSecParentGender	 = $record['eSecParentGender'];
		$sPhrSecParentBdate	     = $record['sSecParentBdate'];
		$iPhrSecParentAge        = $record['iSecParentAge'];
		$sPhrSecParentContact	 = $record['sSecParentContact'];
		$sPhrSecParentEmail	     = $record['sSecParentEmail'];
		$sPhrSecParentImage      = $record['sSecParentImage'];
		$sPhrGuardianName        = $record['sGuardianName'];
		$ePhrGuardianGender      = $record['eGuardianGender'];
		$sPhrGuardianBdate       = $record['sGuardianBdate'];
		$iPhrGuardianAge         = $record['iGuardianAge'];
		$sPhrGuardianContact     = $record['sGuardianContact'];
		$sPhrGuardianEmail       = $record['sGuardianEmail'];
		$sPhrGuardianImage       = $record['sGuardianImage'];
		*/
		$sPhrProfileCreatedOn    = date('Y:m:d H:i:s');
		$this->sPhrUsername      = $this->sEmail;
		$this->sPhrPassword      = md5($this->randomPassword()); //Function call to generate random password

		$sPhrUserTable = $this->sPhrTable;
		$sPhrProfileTable = $this->sPhrProfileTable;
		$sPhrMemberTable = $this->sPhrMemberTable;

		$sInsertQuery1 = "INSERT INTO `{$sPhrUserTable}` (`phr_user_name`,`phr_user_pwd`,`phr_user_member`,`phr_user_member_child`,`phr_user_status`,`phr_user_created`)
						VALUES ('{$this->sPhrUsername}','{$this->sPhrPassword}','{$this->eIsPhrMember}','{$this->sPhrChildPrivilege}',1,'{$sPhrCreatedOn}')";
        
		$conn = $DBMan->getConnInstance(); // Requiring connection insatnce
        $result = $conn->query($sInsertQuery1);
        //var_dump($result);
        //var_dump($sPhrUserTable);
        $this->iPhrId = $conn->insert_id; // Storing last inserted  user id 


      	$this->sUserImage    = $this->renameUserImage($sUserImage,$this->iPhrId);
      	$sPhrSecParentImage  = $this->renameUserImage($sPhrSecParentImage, $this->iPhrId);
      	
      	$sPhrGuardianImage   = $this->renameUserImage($sPhrGuardianImage, $this->iPhrId);


      	/* Change the below query with extracted $profileTable array */
	    $sQuery1  = "INSERT INTO `{$sPhrProfileTable}`(`phr_user_id`,`phr_member_registration_no`,`phr_mem_name_signinparent`,`phr_mem_gender_signinparent`,`phr_mem_dob_signinparent`,`phr_mem_age_signinparent`,`phr_mem_mob_signinparent`,`phr_mem_email_signinparent`,`phr_mem_address_signinparent`,`phr_mem_area_signinparent`,`phr_mem_state_id_signinparent`,`phr_mem_city_id_signinparent`,`phr_mem_country_id_signinparent`,`phr_mem_image_signinparent`,`phr_2ndparent_name`,`phr_2ndparent_gender`,`phr_2ndparent_dob`,`phr_2ndparent_age`,`phr_2ndparent_mob`,`phr_2ndparent_email`,`phr_2ndparent_image`,`phr_guardian_name`,`phr_guardian_gender`,`phr_guardian_dob`,`phr_guardian_age`,`phr_guardian_mob`,
	        		`phr_guardian_email`,`phr_guardian_image`,`phr_member_createdatetime`,`phr_2ndparent_state_id`,`phr_2ndparent_city_id`,`phr_guardian_state_id`,`phr_guardian_city_id`) 
	                 VALUES ('{$this->iPhrId}','{$sPhrMemberRegNo}','{$this->sPhrName}','{$this->ePhrGender}','{$this->sPhrBdate}','{$iPhrAge}','{$this->sContact}','{$this->sEmail}','{$sPhrAddress}','{$sPhrArea}','{$this->iState}','{$this->iCity}','{$this->iCountry}','{$sUserImage}','{$sPhrSecParentName}','{$ePhrSecParentGender}','{$sPhrSecParentBdate}','{$iPhrSecParentAge}','{$sPhrSecParentContact}','{$sPhrSecParentEmail}','{$sPhrSecParentImage}','{$sPhrGuardianName}','{$ePhrGuardianGender}','{$sPhrGuardianBdate}','{$iPhrGuardianAge}','{$sPhrGuardianContact}','{$sPhrGuardianEmail}','{$sPhrGuardianImage}','{$sPhrProfileCreatedOn}','{$this->iState}','{$this->iCity}','{$this->iState}','{$this->iCity}')";
	    
	    $result1 = $conn->query($sQuery1);
		//var_dump($result);
		//var_dump($sPhrProfileTable);

	    $dTakenDate=date('Y-m-d');
	   // $dExpDate=;date('Y-m-d');
	    $dExpDate=date('Y-m-d',strtotime($dTakenDate. " + 1 year"));	    
	    
	    $sQuery2  = "INSERT INTO `{$sPhrMemberTable}`(`phr_user_id`,`phr_member_plan`,`phr_member_taken`,`phr_member_expire`) 
	                 VALUES ('{$this->iPhrId}','{$this->iPhrPlan}','{$dTakenDate}','{$dExpDate}')";
	    
	    $result2 = $conn->query($sQuery2);
	 
	    if($result == true && $result1 == true && $result2 == true ){
	      
	        return TRUE;
	    }
	    else{
	    	echo $sQuery1; 
	        return FALSE;   
	    }
	}	//EOF


 /* Function to add PHR Patient Users
   	* @param array $record
	* return TRUE/FALSE
	*/

	public function addPhrUserBeforePayment($record = []){

        $DBMan = new DBConnManager(); // Initialzing DBConnManager() Class;
		$conn  = $DBMan->getConnInstance(); // Requiring connection insatnce

		
		$eIsPhrMember 		  = '';
		$sPhrChildPrivilege 		  = '';
		$iPhrPlan		  = '';
		$sPhrName 		  = '';
		$ePhrGender 		  = '';
		$sPhrBdate 		  = '';
		$sContact 		  = '';
		$sEmail 		  = '';
		$iUID 		  = '';
		$iState 		  = '';
		$iCity 		  = '';
		$iCountry 		  = '';
        $sPhrAddress          = ''; //phr Address
        $sPhrArea             = ''; //phr area
        
        $sPhrCreatedOn        = ''; // phr created date time
		
		$eIsPhrMember      = $record['ePhrType']; // storing phr type
		$iPhrPlan        = $record['iPlan'];
		$sPhrChildPrivilege = $record['sChildPrivilege'];
		$sPhrName          = $record['sName']; //storing patient name		
		$ePhrGender        = $record['eGender'];
		$sPhrBdate         = $record['sBdate'];
		$sContact          = $record['sContact'];
		$sEmail            = $record['sEmail'];
		if(empty($record['iUID'])||($record['iUID']==null)){
			$iUID= 0;
		}else{
			$iUID= $record['iUID'];
		}	
		$sPhrAddress             = $record['sAddress'];
		$sPhrArea                = $record['sArea'];
		$iPincode                = $record['iPincode'];
		$iState            = $record['iState'];
		$iCity             = $record['iCity'];
		$iCountry          = $record['iCountry'];
		
		$sPhrUserTableBeforePayment = $this->sPhrProfileTableBPayment;
		
		

	     $sQuery  = "INSERT INTO `{$sPhrUserTableBeforePayment}`(`phrType`,`childPrivilege`,`phrPlan`,`phrName`,`phrBdate`,`phrGender`,`phrContact`,`phrEmail`,`phrUID`,`phrAddress`,`phrArea`,`pincode`,`country`,`state`,`city`) 
	                 VALUES ('{$eIsPhrMember}','{$sPhrChildPrivilege}','{$iPhrPlan}','{$sPhrName}','{$sPhrBdate}','{$ePhrGender}','{$sContact}','{$sEmail}','{$iUID}','{$sPhrAddress}','{$sPhrArea}','{$iPincode}','{$iCountry}','{$iState}','{$iCity}')";
	      
	      	//var_dump($sQuery);exit();
	    $result = $conn->query($sQuery);

	     $this->iPhrId = $conn->insert_id; // Storing last inserted  user id 
		  
		  

	    if($result){
	        return $this->iPhrId;
	    }
	    else{
	    	
	        return FALSE;   
	    }
	}	//EOF

/*
Function to retrieve the patient detail before payment
*/
function getDataBeforePayment($sIdBeforePayment){
	 $DBMan = new DBConnManager(); // Initialzing DBConnManager() Class;
	$conn  = $DBMan->getConnInstance(); // Requiring connection insatnce
	if(!mysqli_connect_errno()){
	$sQuery="select * from phr_member_info_before_payment where phr_member_payment_id = {$sIdBeforePayment}";
	//var_dump($sQuery);
	$i=0;
	$aData=array();
		if ($result =$conn->query($sQuery)) {
		    while($row=$result->fetch_array())
		    {
			    $aData[$i] =$row ;    
			    $i++;
			  }
		$result->close();
		}else{
			$aData="null";
		}
		//$DBMan->destroyConnInstance();
		//db_disconnect($bConnect);
		$DBMan->destroyConnInstance($conn);
		//var_dump($aData);
		return ($aData);
	}else{
		
		return null;
	}

}


/*
Function return the State List.
*/
function getStates(){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT * FROM  tpn_state";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $aState   = $result->fetch_all();
	    return $aState;
	}else{
		return null;
	}
}

/*
Function return the Country List.
*/
function getCountry(){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT * FROM  tpn_country";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $aCountry   = $result->fetch_all();
	    return $aCountry;
	}else{
		return null;
	}
}

function getCity(){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT * FROM  tpn_city";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $aCountry   = $result->fetch_all();
	    return $aCountry;
	}else{
		return null;
	}	
}

/*
Function return the State Name By Id.
*/
function getStateName($id){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT `tpn_state_name` FROM  tpn_state where `tpn_state_id`= {$id}";    
    $result  = $conn->query($sQuery);
    if($result==true){
	    $sState   = $result->fetch_all();
	    return $sState;
	}else{
		return null;
	}
}

/*
Function return the Country List.
*/
function getCountryName($id){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT `tpn_country_name` FROM  tpn_country where `tpn_country_id`= {$id}";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $aCountry   = $result->fetch_all();
	    return $aCountry;
	}else{
		return null;
	}
}

function getCityName($id){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT `tpn_city_name` FROM  tpn_city where `tpn_city_id`= {$id}";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $sCity   = $result->fetch_all();
	    return $sCity;
	}else{
		return null;
	}	
}

function getAllCityName(){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT * FROM  tpn_city";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $sCity   = $result->fetch_all();
	    return $sCity;
	}else{
		return null;
	}	
}

function getAreaName($id){
	$bConnect=db_connect('pediatric');
	/*
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    */
    $sQuery = "SELECT `area` FROM  location where `lid`= {$id}";
    $result  = $bConnect->query($sQuery);
    if($result==true){
	    $sArea   = $result->fetch_all();
	    return $sArea;
	}else{
		return null;
	}	
}

function getPlanAmount($idPlan){
	/*$bConnect=db_connect('tpn');*/
	
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
   
    $sQuery = "SELECT `tpn_membership_rates` FROM  tpn_memberships where `tpn_membership_id`= {$idPlan}";
   	
    $result  = $conn->query($sQuery);
    if($result==true){
	    $sPlan   = $result->fetch_all();
	    return $sPlan;
	}else{
		return null;
	}	
}

/*
Function return the Doctor Specialization Data.
*/
function getSpecializationName(){
	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
    $conn   =  $DBMan->getConnInstance(); // Requiring connection instance
    $sQuery = "SELECT * FROM  `tpn_ped_specializations`";
    $result  = $conn->query($sQuery);
    if($result==true){
	    $aSpecialization = $result->fetch_all();
	    return $aSpecialization;
	}else{
		return null;
	}
}



}


class DBConnManager{
	public function getConnInstance(){
		$bConnect=db_connect('tpn');
		return $bConnect;
	}
	public function destroyConnInstance($bConnect){
		$bConnect->close();
	}
}

?>