<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();

if ($securimage->check($_POST['captcha_code']) == false) {
  $_SESSION['capchaCode']=0;  
//echo "invalid capcha";
  header("Location: patient-plan-sign-up.php");
  exit();
}else{
  $_SESSION['capchaCode']=1; 
}

require_once 'function.php';
$addPHR = isset($_POST['phrSubmit'])   ? $_POST['phrSubmit'] : ''; // checking add type

$phrObj = new custom_PHR();
$temp = []; // declaring array variable

$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
$conn=db_connect('tpn');
$conn   =  $DBMan->getConnInstance(); // Requiring connection instance

$sUsername = $_POST['phrEmail'];

$sQuery = "SELECT `phr_mem_email_signinparent` FROM `tpn_phr_member_info` WHERE `phr_mem_email_signinparent` ='{$sUsername}'";

$result = $conn->query($sQuery);

$row = $result->fetch_array();
//var_dump($row);
if($sUsername === $row['phr_mem_email_signinparent']){
	$sMsg = array();
	$sMsg[] = "E11";
	$sError = "E11";
	redirectWithAlert("patient-plan-sign-up.php", $sError);
}
else{
	if(isset($addPHR) && !empty($addPHR)){ // creating temp array for data processing
		$temp['ePhrType']          = inputPostWeb($_POST['phrType']);
		$temp['sChildPrivilege']   = inputPostWeb($_POST['childPrivilege']);
		$temp['iPlan']   = inputPostWeb($_POST['phrPlan']);
		$temp['sName']             = inputPostWeb($_POST['phrName']);
		$temp['sBdate']            = inputPostWeb($_POST['phrBdate']); 
		$temp['eGender']           = inputPostWeb(isset($_POST['phrGender']) ? $_POST['phrGender']: '');
		$temp['sContact']          = inputPostWeb($_POST['phrContact']);
		$temp['sEmail']            = inputPostWeb($_POST['phrEmail']);
		$temp['iUID']              = inputPostWeb($_POST['phrUID']);
		$temp['sAddress']          = inputPostWeb($_POST['phrAddress']);
		$temp['sArea']			   = inputPostWeb($_POST['phrArea']);	
		$temp['iPincode']          = inputPostWeb($_POST['pincode']);
		$temp['iCountry']          = inputPostWeb($_POST['country']);
		$temp['iState']            = inputPostWeb($_POST['state']);
		$temp['iCity']             = inputPostWeb($_POST['city']);
}


	$addPhrResult = $phrObj->addPhrUserBeforePayment($temp); //calling function to add user
	
	if(!$addPhrResult){
		
		$sMsg = "E4";
		redirectWithAlert("phr-patient-detail-payment.php", $sMsg);
	}		 
	else {

		$iID = $addPhrResult;		
		redirectWithAlert("phr-patient-detail-payment.php", $iID);
		
	}

	//var_dump($sMsg);
	//exit();
}

   	

?>