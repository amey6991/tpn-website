<html>
	<head>
		<title>Pediatric - FAQs</title>		
	<?php include('header.php'); ?> 
	<script type="text/javascript">		 
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
<script>
   $(function() {
    $( "#accordion1" ).accordion({heightStyle: "content",collapsible: true});
  });
</script>
<script>
   $(function() {
    $( "#accordion2" ).accordion({heightStyle: "content",collapsible: true});
  });
</script>
<script>
   $(function() {
    $( "#accordion3" ).accordion({heightStyle: "content",collapsible: true});
  });
</script>

<style>
.classCustomBackground{
	background:linear-gradient(rgba(0, 68, 128, 0.74), #004480) !important;
	color: #fff !important;
}
</style>

	
	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	FAQ's
			              </div>		             
			              	
			              	<div id="tabs">
							  <ul>
							    <li><a href="#tabs-1">PARENTS</a></li>
							    <li><a href="#tabs-2">DOCTOR</a></li>
							    <li><a href="#tabs-3">PHARMACIST</a></li>
							  </ul>
							  <div id="tabs-1">
							    <div id="accordion1" class="accordion">
								  <h3 class="classCustomBackground">Que.1) How to sign up for Membership Plan?</h3>
								  <div>
								    <p>
								    	Goto ► <a href="http://thepediatricnetwork.com/patient-sign-up.php">Patient Sign Up</a> link select your plan and fill some few details.
								    	Once Signup Complete you will get an email from "ThePediatricNetwork" which will give you the login access to the "ThePediatricNetwork".
								    </p>
								  </div>
								  <h3 class="classCustomBackground">Que.2) Which plan patient can select?</h3>
								  <div>
								    <p>
								    	Plean are based on your requirement, so you will get all clear idea once you goto this link : Goto ► http://thepediatricnetwork.com/patient-sign-up.php
								    	For additional information you can also contact to our support team at support@thepediatricnetwork.com.
								    </p>
								  </div>
								  <h3 class="classCustomBackground">Que.3) Why to sign up in "ThePediatricNetwork"?</h3>
								  <div>
								   	<p>
								    	You are facilited with :
								    	<ul>
											<li>Taking an Appointment / Chatting with our portal doctor at anytime</li>
											<li>Finding a Pediatrician close to you</li>
											<li>Authorizing a Pediatrician to see your child's reports</li>
											<li>Uploading reports and growth milestones at one single location.</li>
											<li>Access to the complete history of your child's health journey</li>
											<li>Access to our specially created hand outs and education material.</li>
											<li>Access to our pharmacy network for discounts, home delivery and paperless prescriptions.</li>
										</ul>
								    </p>								   
								  </div>
							  	</div>
							  </div>
							  <div id="tabs-2">
							    <div id="accordion2" class="accordion">
								  <h3 class="classCustomBackground">Que.1) How to sign up for ThePediatricNetwork?</h3>
								  <div>
								     <p>
								    	Goto ► <a href="http://thepediatricnetwork.com/doctor-sign-up.php">Doctor Sign Up</a> link, fill some few details.
								    	Your detail will be send for the verification to the "ThePediatricNetwork", Once its verified you will recived an email from the "ThePediatricNetwork" with your login access.
								    </p>
								  </div>
								  <h3 class="classCustomBackground">Que.2) Why to link with "ThePediatricNetwork"</h3>
								  <div>
								    <p>
								    	You are facilited with :
								    	<ul>
											<li>Appointment Management</li>
											<li>See Patient History</li>
											<li>Storing Patient Medical Records</li>
											<li>Uploading reports and growth milestones at one single location.</li>
											<li>Access to the complete history of your patients health journey</li>
											<li>Managing Billing and Daily Register</li>
											<li>Access to our pharmacy network</li>
										</ul>
								    </p>	
								  </div>								  
								</div>
							  </div>
							  <div id="tabs-3">
							    <div id="accordion3" class="accordion">
								  <h3 class="classCustomBackground">Que.1) How to sign up for ThePediatricNetwork?</h3>
								  <div>
								     <p>
								    	Goto ► <a href="http://thepediatricnetwork.com/pharmacist-sign-up.php">Pharmacist Sign Up</a> link, fill some few details.
								    	Your detail will be send for the verification to the "ThePediatricNetwork", Once its verified you will recived an email from the "ThePediatricNetwork" with your login access.
								    </p>
								  </div>
								</div>
							  </div>
							</div>			              	
						</div>						
					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




