<html>
	<head>
		<title>Pediatric - Refund Policy</title>		
	<?php include('header.php'); ?> 

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Refund Policy
			              </div>
			              	<div class="classAboutUsText">	

			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">For Members enrolled for Membership</span><br/><br/>
			              			<ul class="classPLeaderText classJustifyText">
			              				<li>Members cannot directly request for cancellation of membership</li>
										<li>Cancellation requests to be made by the member only and cannot be made by anyone else on behalf of the member.</li>
										<li>Cancellation requests will be accepted only by submitting Membership Cancellation form at Clinic or through the Member Portal.</li>
										<li>All refunds shall be made in name of the payee of the membership (payee may or may not be different from the member).</li>
										<li>Refunds will be provided in form of Cheque only.</li>
										<li>As per the Indian Medical Council (Professional, Etiquette & Ethics) Regulations 2002, ThePediatricNetwork is required to retain the medical records of the member for a minimum period of 5 years from the date of cancellation of the ThePediatricNetwork membership.</li>
										<li>ThePediatricNetwork Health Solutions Private Limited reserves the right to make changes in it Refund or Cancellation Policy without any prior notice.</li>
				              		</ul>	
				              	</p><br/><br/>
				              		              	
							</div>
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




