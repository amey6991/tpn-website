<html>
	<head>
		<title>Pediatric - Patient Sign Up Complete</title>		
	<?php include('header.php'); ?> 


	</head>
<?php


$aFields =array();
$sFields=null;
if(isset($_GET['msg'])){ // Pass id for retrieving data before payment.
	$sIdBeforePayment=$_GET['id'];
	$sMsg=$_GET['msg'];
	$aFields=showDataBeforePayment($sIdBeforePayment);	
	if(isset($aFields[0]['phrName'])){
		$sName=$aFields[0]['phrName'];
		$sEmail=$aFields[0]['phrEmail'];
	}else{
		$sName=null;
		$sEmail=null;
	}

	//var_dump($sIdBeforePayment);
	if($sMsg=='S4'){
		$sFields='<strong>Thank You!</strong> </br>For signing up with the Pediatric Network.
		</br>Your Username and Password with basic instructions have been emailed to the official email id ( '.$sEmail.' ) provided. We hope you have a fruitful and happy year with us.';
		
	}else{			
		$sFields='<strong>Thank You!</strong> </br>For signing up with the Pediatric Network.<br/>Your Username and Password with basic instructions will be email to the official email id ( '.$sEmail.' ) provided, Once your payment confirmed.<br/>We hope you have a fruitful and happy year with us.';
	}

	
}else{
	$aFields=empty($aFields);
	$sEmail=null;
	$sFields='<strong>Thank You!</strong> </br>For signing up with the Pediatric Network.<br/>Your Username and Password with basic instructions will be email to the official email id ( '.$sEmail.' ) provided, Once your payment confirmed.<br/>We hope you have a fruitful and happy year with us.';
}


?>
	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          <!-- 
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Welcome
			              </div>	
			              <div class="classDivTextWrapper">			              		
			              		<p class="classPLeaderText classPMsgWrapper" style="color:#222 !important;">
				              		<?php
				              			echo $sFields;
				              		?>
				              	</p>
				              	
							</div>	

							<div class="classDivTextWrapper" style="width:80%;">			              		
			              		<h3 style="color:#F80;">The features you can access once you login in are:</h3>

		              			<ul class="classUlFeatureText">
									<li>Taking an Appointment / Chatting with our portal doctor at anytime</li>
									<li>Finding a Pediatrician close to you</li>
									<li>Authorizing a Pediatrician to see your child's reports</li>
									<li>Uploading reports and growth milestones at one single location.</li>
									<li>Access to the complete history of your child's health journey</li>
									<li>Access to our specially created hand outs and education material.</li>
									<li>Access to our pharmacy network for discounts, home delivery and paperless prescriptions.</li>
								</ul>
								
		              		</div>	             

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




