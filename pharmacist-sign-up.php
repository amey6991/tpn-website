<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();
?>
<html>
	<head>
		<title>Pediatric - Pharmacist Sign Up</title>		
	<?php include('header.php'); ?> 
<script type="text/javascript" src="./js/patient-signup.js"></script>

<script type="text/javascript">
//Funtion to hide Element
function hideDiv(elementId){	
	var id="#"+elementId;	
	  $(id).hide();	
}
</script>

	</head>

	<body>

<?php 
	if(isset($_SESSION['capchaCode'])){	
	$iInvalidCapcha=$_SESSION['capchaCode'];
	}else{  
		$iInvalidCapcha=1;	
	}
	$aLocation=array();
//	$aPincode=array();
	$aLocation=get_location();
	//$aCity=getCity();
?>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           <!--
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			        <div class="classDivTabContainCustom classTopMargin30"  id="" >
		            	<div class="classErrorMsg" id="idErrorMsg" onClick="hideDiv('idErrorMsg');">
		            		<?php 
		            			echo $iInvalidCapcha==0?"Please Enter Correct Security Code":'';
		            			$_SESSION['capchaCode']=1;
		            		?>
		            	</div>
			            <div class="classDivHeading">
			              	Pharmacist Sign Up
			            </div>	

			            <div class="classDivInnerPageBodyContainer">
			              	<div style="width:100%;">
					              <form id="pharmacy-signup" action="add-pharmacist-signup.php" method="POST">
		            				<div class="classP48Float">
						                <p class="">

						                <label for="name">Name of Pharmacy <span class="classRed">*</span><span id="idPharmaNameMsg" class="classValidationMsg"></span></label>
						                <input id="idPharmaName" type="text" name="pharmaName" value="" class="classFormTextBox"/>
						                </p>
						                
						                <p class="">
						                <label for="contact-no">Contact No <span class="classRed">*</span><span id="idPharmaContactMsg" class="classValidationMsg"></span></label>
						                <input id="idPharmaContact" type="text" name="pharmaContact" value="" class="classFormTextBox"/>
						                </p>

						                <p class="">
						                <label for="email">Email Id <span class="classRed">*</span><span id="idPharmaEmailMsg" class="classValidationMsg"></span></label>
						                <input id="idPharmaEmail" type="text" name="pharmaEmail" value="" class="classFormTextBox"/>
						                </p>

						                <div >
							                <p>
							                <label for="pharmaAddress">Address <span class="classRed">*</span><span id="idPharmaAddressMsg" class="classValidationMsg"></span></label>
							                <textarea id="idPharmaAddress" name="pharmaAddress" class="classTextAreaW100perH120px classFormTextBox"></textarea>
							                </p>
						            	</div>

						            	 <p class="classP48Float">
							                <label for="pharmaArea">Area <span class="classRed">*</span><span id="idPharmaAreaMsg" class="classValidationMsg"></span></label>
							                	<input type="text" id="idPharmaArea" name="pharmaArea" class="classFormTextBox" />
							                	<!-- <select id="idPharmaArea" name="pharmaArea" class="classFormTextBox">
							                		<option value="select">Select</option>
							                		<?php							                				                					
							                			// foreach ($aLocation as $key){
							                			// 	echo "<option value={$key[0]}>{$key[2]}</option>";
							                			// }
							                		 ?>						                		
							                	</select> -->
							                </p>
							                
							                <p class="classP48FloatLMargin">
							                	<label for="pharmaArea2">Area Catering To</label>
							                	<input type="text" id="idPharmaArea2" name="pharmaArea2" class="classFormTextBox">
							                	<!-- <select id="idPharmaArea2" name="pharmaArea2" class="classFormTextBox">
							                		<option value="select">Select</option>
							                		<?php	
							                				                					
							                			// foreach ($aLocation as $key){
							                			// 	echo "<option value={$key[0]}>{$key[2]}</option>";
							                			// }
							                		 ?>						                		
							                	</select>	 -->						                		
							                </p>

								            <p class="">
							                	<label for="pharmaArea3">Another Area Catering To</label>
							                		<input id="idPharmaArea3" type="text" name="pharmaArea3" value=" " class="classFormTextBox"/>						                		
							                </p>
								         
						            </div>    
						            <div class="classP48FloatLMargin">
						                
						            	<p>
						                <label for="pharmaName1">Name of Contact Person <span class="classRed">*</span><span id="idPharmaName1Msg" class="classValidationMsg"></span></label>
						                <input id="idPharmaName1" type="text" name="pharmaName1" value="" class="classFormTextBox"/>
						                </p>

						                <p>
						                <label for="pharmaContact1">Mobile<span class="classRed">*</span><span id="idPharmaContact1Msg" class="classValidationMsg"></span></label>
						                <input id="idPharmaContact1" type="text" name="pharmaContact1" value="" class="classFormTextBox"/>
						                </p>

						                <p>
						                <label for="pharmaDesignation1">Designation<span class="classRed">*</span><span id="idPharmaDesignation1Msg" class="classValidationMsg"></span></label>
						                <input id="idPharmaDesignation1" type="text" name="pharmaDesignation1" value="" class="classFormTextBox"/>
						                </p>

						                <p>
						                <label for="pharmaEmail1">Email Id<span class="classRed">*</span><span id="idPharmaEmail1Msg" class="classValidationMsg"></span></label>
						                <input id="idPharmaEmail1" type="text" name="pharmaEmail1" value="" class="classFormTextBox"/>
						                </p>
						            	
						            	<p>
											<label for="captcha_code">Secure code<span class="classRed">*</span><span id="idPharmaCapchaMsg" class="classValidationMsg"></span></label>
											<input type="text" name="captcha_code" class="classFormTextBox" size="10" id="idPharmaCapcha" maxlength="6" />
										</p>
										
						            	<p>
						            		<img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image"  style="border:1px solid #aaa;"/>
											<a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><img style="width:25px; margin:20px;" src="securimage/images/refresh.png" /></a>
										</p>

						            </div>						            
						            <div class="classP48">
						                						                            
						                <p class="classP48">
						                <button id="idPharmaSubmit" type="submit" class="classGoButton"> Submit</button>
						                </p>
						            </div>

						            </form>
						            
								<div id="required">
								<p>* Required Fields</p></div>

	    						</div>
						
							</div>


						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




