
<?php
/*
Map script, which get latitude and longitude from address and city and then
fetch maps.
*/
?>
<script type="">
function initialize(latitude, longitude, t, i) {
  //var res = GetLocation();
 // alert(i);
  var myLatlng = new google.maps.LatLng(latitude,longitude);
  var mapOptions = {
    zoom: 10,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'+i), mapOptions);
 // var map2 = new google.maps.Map(document.getElementById('map-canvas2'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: t
  });
 
 
}

//google.maps.event.addDomListener(window, 'load', GetLocation);
</script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
    

<script type="text/javascript">
 function GetLocation(i) {
            var geocoder = new google.maps.Geocoder();
           // var address = 'Chaprasi pura camp amravati';            
            //var title = address;
            
            var area = document.getElementById("address"+i).value;
            var city = document.getElementById("area"+i).value;
            //alert(city);
            var address = area+','+city;
            var t=address;
            //alert(address);
              geocoder.geocode({ 'address': address }, function (results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      var latitude = results[0].geometry.location.lat();
                      var longitude = results[0].geometry.location.lng();
                      var res=latitude+','+longitude;
                      
                     // alert("Latitude: " + latitude + "\nLongitude: " + longitude);
                    initialize(latitude, longitude, t, i);  

                      //return res;
                  } else {
                                            
                      //alert("Request failed");
                      //alert("hello");
                      return 'none';
                  }

                
            });
            
        };

</script>


  
<script type="text/javascript">
function displayMap(){
  var n = document.getElementById("idTotalRow").value;
  var i=1; 
  
  while(i<n){
  GetLocation(i);
  i++;
}
  
}
</script>  

<?php

/*
End : Map script, which get latitude and longitude from address and city and then
fetch maps.
*/

/*
Start : Custom Map script, fetch the map by direct providing latitude and longitutde.
*/
?>

<script type="text/javascript">
function showMap(){
  var n = document.getElementById("idTotalRow").value;
  var i=1; 
  while(i<n){
  getMap(i);  
  i++;
}
  
}
</script>  
  
<script type="">
function getMap(i) {
  //var res = GetLocation();
 // alert(i);
  var latitude = document.getElementById("idLatitude"+i).value;
  var longitude = document.getElementById("idLongitude"+i).value;
  
  if(latitude==''||longitude==''){
    GetLocation(i);
  }else{
      var area = document.getElementById("address"+i).value;
      var city = document.getElementById("area"+i).value;
      //alert(city);
      var address = area+','+city;
      var t=address;
      var myLatlng = new google.maps.LatLng(latitude,longitude);
      var mapOptions = {
        zoom: 10,
        center: myLatlng
      }
      var map = new google.maps.Map(document.getElementById('map-canvas'+i), mapOptions);
     // var map2 = new google.maps.Map(document.getElementById('map-canvas2'), mapOptions);

      var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: t
      });
    }
 
}

//google.maps.event.addDomListener(window000, 'load', GetLocation);
</script>

<?php
/*
End : Custom Map script, fetch the map by direct providing latitude and longitutde.
*/
?>