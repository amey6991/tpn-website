<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();
?>
<html>
	<head>
		<title>Pediatric - Doctor Sign Up</title>		
	<?php include('header.php'); ?> 
 <script type="text/javascript" src="./js/patient-signup.js"></script>
 

<script>
$(document).ready(function($) {	
  $(function() {
    $( "#dob" ).datepicker({
      changeMonth: true,
      changeYear: true,      
    	minDate: "-100Y",
        maxDate: "-24Y",
        dateFormat: "dd/mm/yy",
       	yearRange: "c-100:c+24"         
    });
  });
});
  </script>


<script>
     
    function get_pincode_zone() {
        var searchBy = document.getElementById("area").value;
        //alert(searchBy);
        var request = $.ajax({
            url: "ajaxFunction.php",
            type: "POST",    
            data: {callTo: "pincodezone", searchBy:searchBy},        
            dataType: "html"
        });
 
        request.done(function(msg) {
            $("#idDisplayZonePin").html(msg);          
        });
 
        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });
    }
								
</script>

<script type="text/javascript">
/*
Functio to check the unique email address of doctor.
*/
function checkDoctor(){
    	
    	var searchBy = document.getElementById("idPedEmailid").value;
    	//alert(searchBy);
    	var request = $.ajax({
            url: "ajaxFunction.php",
            type: "POST",    
            data: {callTo: "checkDoctor", searchBy:searchBy},        
            dataType: "html"
        });
 
        rs=request.done(function(msg) {
        	//alert(msg);
        	if(msg==1){
        		var result=validateEmail("idPedEmailid");
        		//alert(result);
        		if(result==1){
            		document.getElementById("idPedEmailidMsg").innerHTML='<span class="classGreen">Available</span>';          
            	}else{
            		if(searchBy==''){
	        			document.getElementById("idPedEmailid").value='';  
	       				document.getElementById("idPedEmailidMsg").innerHTML='<span class="classRed">Please Enter Email</span>';
	        		}else{
	        			document.getElementById("idPedEmailid").value='';  
	            		document.getElementById("idPedEmailidMsg").innerHTML='<span class="classRed">Please Enter Valid Email</span>';          
	            	}
            	}
        	}else{
        		if(searchBy==''){
        			document.getElementById("idPedEmailid").value='';  
        			document.getElementById("idPedEmailidMsg").innerHTML='<span class="classRed">Please Enter Email</span>';
        		}else{
        			document.getElementById("idPedEmailid").value='';  
        			document.getElementById("idPedEmailidMsg").innerHTML='<span class="classRed">Email Already Exist</span>';          		
        		}
        	}
        }); 		
        request.fail(function(jqXHR, textStatus) {
            alert( "User Already Existed" + textStatus );
        });
    }

</script>

<script type="text/javascript">
//Funtion to hide Element
function hideDiv(elementId){	
	var id="#"+elementId;	
	  $(id).hide();	
}

</script>

	</head>

	<body>

<?php 
$PostBack= array();

if(isset($_SESSION['capchaCode'])){	
	$iInvalidCapcha=$_SESSION['capchaCode'];	
	$PostBack=$_SESSION['postBack'];	
}else{  
	$iInvalidCapcha=1;	
	unset($PostBack);
	unset($_SESSION['postBack']);
	$PostBack=null;
}

$aLocation=array();
$aPincode=array();
$aLocation=get_location();
$aCity=getCity();
$aState=displayState();
$aCountry=displayCountry();
$aSpecialization=getDoctorSpecialization()
//$aPincode=get_pincode();
?>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">			          

			          <div class="classDivTabContainCustom classTopMargin30"  id="" >
			            <div class="classErrorMsg" id="idErrorMsg" onClick="hideDiv('idErrorMsg');">
		            		<?php 
		            			echo $iInvalidCapcha==0?"Please Enter Correct Security Code":'';
		            			$_SESSION['capchaCode']=1;
		            			unset($_SESSION['capchaCode']);		            			
		            		?>
		            	</div>

			            <div class="classDivHeading">
			              	Doctor Sign Up
			            </div>		             

			            <div class="classDivInnerPageBodyContainer">
			              	<div style="width:100%;">
					              <form id="pharmacy-signup" action="postDoctorAPI.php" method="POST">
		            				
		            				<input id="idIpAddress" type="hidden" name="ip_address" value="<?php echo gethostbyaddr($_SERVER['REMOTE_ADDR']); ?>"/>
		            				<div class="classDivFormSubHeading">Personal Information</div>
		            				<p class="classP48FloatWithClear">
						                <label for="doctor-name">First Name <span class="classRed">*</span> <span id="idPedFirstNameMsg" class="classValidationMsg"></span></label>
						                <input type="text" name="pedFirstName" id="idPedFirstName" class="classFormTextBox" value="<?php echo isset($PostBack['pedFirstName'])?$PostBack['pedFirstName']:''; ?>" />
						                
						            </p>

						            <p class="classP48FloatLMargin">
						                <label for="doctor-name">Last Name <span class="classRed">*</span> <span id="idPedLastNameMsg" class="classValidationMsg"></span></label>
						                <input type="text" name="pedLastName" id="idPedLastName" class="classFormTextBox" value="<?php echo isset($PostBack['pedLastName'])?$PostBack['pedLastName']:''; ?>"/>
						                
						            </p>

						            <p class="classP48FloatWithClear">
						               <label for="np-code">Specialization <span class="classRed">*</span> <span id="idPedSpecializtnMsg" class="classValidationMsg"></span></label>
						                	<select id="idPedSpecializtn" name="pedSpecializtn" class="classFormTextBox">
						                		<option value="select">Select</option>
						                		<?php			
						                		 $iCheckValue= isset($PostBack['pedSpecializtn'])?$PostBack['pedSpecializtn']:'';
						                			foreach ($aSpecialization as $key){
						                				if($iCheckValue==$key[0]){
						                					echo "<option value={$key[0]} selected>{$key[1]}</option>";
						                				}else{
						                					echo "<option value={$key[0]}>{$key[1]}</option>";	
						                				}
						                				
						                			}
						                		 ?>							                		
						                	</select>
						            </p>

						           
						            <p class="classP48FloatLMargin">
						                <label for="mci-code">MCI Code<span class="classRed">*</span> <span id="idPedMCICodeMsg" class="classValidationMsg"></span></label>
						                <input type="text" name="pedMCICode" id="idPedMCICode"  class="classFormTextBox" value="<?php echo isset($PostBack['pedMCICode'])?$PostBack['pedMCICode']:''; ?>"/>
						            </p>						            

						            <p class="classP48FloatWithClear">
						                <label for="gender">Gender<span class="classRed">*</span> <span id="idPedGenderMsg" class="classValidationMsg"></span></label>
						                	<?php  $iCheckValue= isset($PostBack['pedGender'])?$PostBack['pedGender']:''; ?>
						                <select name="pedGender" id="idPedGender" class="classFormTextBox">
						                	<option value="select">Select</option>						                	
						                	<option value="Male" <?php echo $iCheckValue=='Male'?'Selected':''; ?>>Male</option>
						                	<option value="Female" <?php echo $iCheckValue=='Female'?'Selected':''; ?>>Female</option>
						                </select>

						            </p>

						            <p class="classP48FloatLMargin">
						                <label for="dob">Date Of Birth<span class="classRed">*</span> <span id="idPedDOBMsg" class="classValidationMsg"></span></label>
						                <input id="dob" type="text" name="pedDOB" class="classFormTextBox" value="<?php echo isset($PostBack['pedDOB'])?$PostBack['pedDOB']:''; ?>"/>
						            </p>
						            
						            <p class="classP48FloatWithClear">
						                <label for="mobile">Mobile<span class="classRed">*</span> <span id="idPedMobileNoMsg" class="classValidationMsg"></span></label>
						                <input type="text" name="pedMobileNo" id="idPedMobileNo" class="classFormTextBox" value="<?php echo isset($PostBack['pedMobileNo'])?$PostBack['pedMobileNo']:''; ?>"/>
						            </p>

					                <p class="classP48FloatLMargin">
						                <label for="emai-sparent">Email Id<span class="classRed">*</span> <span id="idPedEmailidMsg" class="classValidationMsg"></span></label>
						                <input type="text" name="pedEmailid" id="idPedEmailid" class="classFormTextBox" value="<?php echo isset($PostBack['pedEmailid'])?$PostBack['pedEmailid']:''; ?>" onBlur="checkDoctor();"/>
					                </p>
						            
						            <div style="clear:both;"></div>						            
						            <h1></h1>
						             <p class="classP48FloatWithClear">
						                <label for="address">Address <span class="classRed">*</span> <span id="idPedAddressMsg" class="classValidationMsg"></span></label>
						                <textarea name="pedAddress" id="idPedAddress" class="classTextAreaW100perH120px classFormTextBox"><?php echo isset($PostBack['pedAddress'])?$PostBack['pedAddress']:''; ?></textarea>
						             </p>

						            <span class="classP48FloatLMargin">
										<p class="classP48FloatWithClear">
						               	 <label for="city">City <span class="classRed">*</span> <span id="idPedCityMsg" class="classValidationMsg"></span></label>
						                	<select id="city" name="pedCity" class="classFormTextBox">
						                		<option value="select">Select</option>
						                		<?php							                				                					
						                				 $iCheckValue= isset($PostBack['pedCity'])?$PostBack['pedCity']:'';
						                			foreach ($aCity as $key){						                									                			
						                			
						                				if($iCheckValue==$key[0]){
						                					echo "<option value={$key[0]} selected>{$key[2]}</option>";
						                				}else{
						                					echo "<option value={$key[0]}>{$key[2]}</option>";	
						                				}
						                				
						                			}
						                		 ?>	
						                									                		
						                	</select>
						                </p>

									    <p class="classP48FloatLMargin">
						               	 <label for="area">Area <span class="classRed">*</span> <span id="idPedAreaMsg" class="classValidationMsg"></span></label>
						                	<input type="text" name="pedArea" class="classFormTextBox" value="<?php echo isset($PostBack['pedArea'])?$PostBack['pedArea']:''; ?>" id="area" />
						                	<!--<select id="area" name="pedArea" class="classFormTextBox" value="<?php// echo isset($PostBack['pedFirstName'])?$PostBack['pedFirstName']:''; ?>" onChange="get_pincode_zone('area');">
						                		<option value="select">Select</option>
						                		<?php	
						                				                					
						                			//foreach ($aLocation as $key){
						                			//	echo "<option value={$key[0]}>{$key[2]}</option>";
						                			//}
						                		 ?>						                		
						                	</select>	-->					                	
						                </p>
						            </span>
						            
						            <span id="idDisplayZonePin">    
							            <span class="classP48FloatWithClear">
							                <p class="classP48FloatWithClear" id="idDisplayZone">
							               	 <label for="zone">Zone <span class="classRed"></span> <span id="idPedZoneMsg" class="classValidationMsg"></span></label>
							                	<input id="zone" type="text" name="zone" class="classFormTextBox" value="<?php echo isset($PostBack['zone'])?$PostBack['zone']:''; ?>"/> 
							                	<!--<select id="zone" name="pedZone" class="classFormTextBox">
							                		<option value=" ">Select</option>
							                		<?php		/*						                				                					
							                			foreach ($aLocation as $key){
							                				echo "<option value={$key[3]}>{$key[3]}</option>";
							                			} */
							                		 ?>					                		
							                	</select>-->
							                </p>

							                <p class="classP48FloatLMargin" id="idDisplayPincode">
							                <label for="pin-code">Pin Code<span class="classRed">*</span> <span id="idPincodeMsg" class="classValidationMsg"></span></label>
							                	<input id="pin-code" type="text" name="pincode"  value="<?php echo isset($PostBack['pincode'])?$PostBack['pincode']:''; ?>" class="classFormTextBox"/> 							                	
							                	<!--<select id="pin-code" name="pincode" class="classFormTextBox">						                		
							                		<option value="select">Select</option>
							                		<?php								                				                					
							                			//foreach ($aPincode as $key){
							                			//	echo "<option value={$key[1]}>{$key[1]}</option>";
							                			//}
							                		 ?>							                		
							                	</select>-->
							                </p>
							            </span>
						            </span>

						            <span class="classP48FloatLMargin">
						                <p class="classP48FloatWithClear">
							                <label for="state">State</label>
							                <select id="state" name="pedState" class="classFormTextBox">
						                		<option value="" selected="selected">Select</option>
						                		<?php

						                			 $iCheckValue= isset($PostBack['pedState'])?$PostBack['pedState']:'';
						                			foreach ($aState as $key){
						                				if($iCheckValue==$key[0]){
						                					echo "<option value={$key[0]} selected>{$key[1]}</option>";
						                				}else{
						                					echo "<option value={$key[0]}>{$key[1]}</option>";	
						                				}
						                				
						                			}
						                		?>						                			
						                	</select>
						           		</p>

						                <p class="classP48FloatLMargin">
						                <label for="country">Country</label>
						                	<select id="country" name="pedCountry" class="classFormTextBox">
						                		<option value="1" selected="selected">India</option>
						                		<?php		

						                			// $iCheckValue= isset($PostBack['pedCountry'])?$PostBack['pedCountry']:'';
						                			// foreach ($aCountry as $key){
						                			// 	if($iCheckValue==$key[0]){
						                			// 		echo "<option value={$key[0]} selected>{$key[1]}</option>";
						                			// 	}else{
						                			// 		echo "<option value={$key[0]}>{$key[1]}</option>";	
						                			// 	}						                				
						                			// }
						                		 ?>							                		
						                	</select>
						                </p>	
						            </span>  

						            	<p class="classP48FloatWithClear">
											<label for="captcha_code">Secure code<span class="classRed">*</span><span id="idPedCapchaMsg" class="classValidationMsg"></span></label>
											<input type="text" name="captcha_code" value='' class="classFormTextBox" size="10" id="idPedCapcha" maxlength="6" />
										</p>
						            	<p class="classP48FloatWithClear">
						            		<img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image"  style="border:1px solid #aaa;"/>
											<a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false"><img style="width:25px; margin:20px;" src="securimage/images/refresh.png" /></a>
											
										</p>
										
						                <p class="classP48FloatWithClear">
						                <button id="idPedSubmit" type="submit" class="classGoButton"> Submit</button>
						                </p>

						                
						            </form>
						            
								<div id="required" class="classP48FloatWithClear">
								<p>* Required Fields</p></div>

	    						</div>
						
							</div>
							
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




