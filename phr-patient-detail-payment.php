
<html>
	<head>
		<title>Pediatric</title>		
	<?php include('header.php'); ?> 

<!-- Validation script start-->
<script type="text/javascript" src="./js/patient-signup.js"></script>
<!-- Date Picker script start-->
<script>
$(document).ready(function($) {
  $(function() {
    $( "#idPhrPayDate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      minDate: "1"
    });
  });

});
</script>
<!-- Auto complete css -->
<style type="text/css">
  .custom-combobox {
    position: relative;
    display: inline-block;
    width: 100%;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }

  .custom-combobox .custom-combobox-input {
        background: none repeat scroll 0px 0px #FFF;
    border: 1px solid #C9C9C9;
    color: #545658;
    padding: 8px;
    font-size: 16px;
    border-radius: 2px;
    width: 100%;
  }

</style>

<?php
//var_dump($viewPHR);
$aFields =array();

if(isset($_GET['id'])){ // Pass id for retrieving data before payment.
	$sIdBeforePayment=$_GET['id'];
	$aFields=showDataBeforePayment($sIdBeforePayment);
	$sCity=getCityById($aFields[0]['city']);	
	$sCountry=getCountryById($aFields[0]['country']);
	$sState=getStateById($aFields[0]['state']);

}else{
	$aFields=empty($aFields);
}
// var_dump($aFields);
?>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Patient Sign Up Detail and Payment 
			              </div>		             

			               <div class="classDivInnerPageBodyContainer">
			              	<div style="width:100%;">
					              
		            				<p class="classP48">
		            					<?php
		            						$sPlan=null;
		            						if($aFields[0]['phrPlan']=='3'){
		            							$sPlan="Bronze";
		            						}
		            						if($aFields[0]['phrPlan']=='2'){
			            						$sPlan="Silver";
			            					}
			            					/*
											Gold Plan replaced with Basic Plan 
			            					*/
			            					if($aFields[0]['phrPlan']=='1'){
	            								$sPlan="Basic";
			            					}
			            					// if($aFields[0]['phrPlan']=='1'){
	            							// 	$sPlan="Gold";
			            					// }
			            					if($aFields[0]['phrPlan']=='4'){
	            								$sPlan="Test";
			            					}		
		            					?>
						                <span class="classSpanDetailHolder">Membership Plan :</span><span class="classSpanDataField"> <?php echo $sPlan; ?></span>
						            </p>

						            

						            <div class="classP48Float">
						            	<div class="classDivFormSubHeading">Parents Detail</div>
						                <p>
						                	<span class="classSpanDetailHolder">Name of Signing Parent :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrName']; ?></span>
			                            </p>

			                            <p>
			                            	<span class="classSpanDetailHolder">Contact :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrContact']; ?></span>
			                        	</p>

			                            <p>
			                            	<span class="classSpanDetailHolder">Email Id :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrEmail']; ?></span>
			                        	</p>

			                        	<p>
			                            	<span class="classSpanDetailHolder">UID :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrUID']; ?></span>
			                        	</p>

			                            <p>
			                            <span class="classSpanDetailHolder">Date Of Birth :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrBdate']; ?></span>
			                        	</p>

			                        	<p>
			                            	<span class="classSpanDetailHolder">Gender :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrGender']; ?></span>
			                        	</p>
									    

						            </div>						            
						              
						            <div class="classP48Float" style="margin-left:35px;">
						           		<div class="classDivFormSubHeading">Address Detail </div>				            
						                <div class="classP48Float" style="width:100% !important;" >
						                <p>
						                	<span class="classSpanDetailHolder">Address :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrAddress']; ?></span>
						            	</p>
						            	</div>

						            	<div class="classP48Float">
						                <p>
						               	 <span class="classSpanDetailHolder">City :</span><span class="classSpanDataField"> <?php echo $sCity[0][0]; ?></span>
						                </p>
						            	</div>
									 
										<div class="classP48FloatLMargin">
										<p>
						               	 <span class="classSpanDetailHolder">Area :</span><span class="classSpanDataField"> <?php echo $aFields[0]['phrArea']; ?></span>
						                </p>
						            	</div>

										<div class="classP48Float">
						            	<p>
						                <span class="classSpanDetailHolder">State :</span><span class="classSpanDataField"> <?php echo $sState[0][0]; ?></span>
						                </p>
						            	</div>

						            	<div class="classP48FloatLMargin">
						                <p>
						                <span class="classSpanDetailHolder">Country :</span><span class="classSpanDataField"> <?php echo $sCountry[0][0]; ?></span>
						                </p>
						            	</div>

										<div class="classP48Float">    
						            
						                <p>
						               	 <span class="classSpanDetailHolder">Pincode :</span><span class="classSpanDataField"> <?php echo $aFields[0]['pincode']; ?></span>
						                </p>
						              
						            	</div>
						            </div>

						            	<div class="classDivFormSubHeading">Payment Mode</div>
						                
						                <div class="classW12 classFL classPMHolder">				                            						                
										<?php

											$idT=null;
						                	$idT=generateUniqueCode($sIdBeforePayment);
						                	
						                		/*
												If condition is use to skip Payment Option
												Test Plan: need not require any payment.
												From 29-09-2015 onwords "Cash Payment Option added, hence online payment skip"
						                		*/
						                		$sCashPaymentRedirect='postUserAPI.php';
						                		if(isset($_GET)){
						                			if($sPlan=="Test"){
						                				$sRedirect='postUserAPI.php?freePlanId='.$idT;
						                			}else{
						                				$sRedirect='payment/checkout.php?id='.$idT;	
						                			}
						                			
						                		}else{
						                			$sRedirect='phr-patient-detail-payment.php?id='.$sIdBeforePayment;
						                		}
						                	?>
						                	<div id="tabs" class="classPaymentModeWrapper">
											  <ul>
											    <li><a href="#tabs-1">Cash Payment</a></li>
											    <li><a href="#tabs-2">Online Payment</a></li>
											  </ul>											  
											  <div id="tabs-1">
											    <div class="classCashPaymentForm ui-tab-inner-div">
											    	<form class="classCPForm" name="cpform" id="idCPForm" action="<?php echo $sCashPaymentRedirect; ?>" method="POST">
											    		<input type="hidden" value="<?php echo $sIdBeforePayment; ?>" name="idBeforePayment" id="idBeforePayment"/>
													    <div class="classW12 classFL classXBMargin">	
													    	<div class="classW6 classFL">
													    		<div class="classW11 classFL classPedHolder">
										                            <label for="idPhrPedName">Select  Pediatrician Name <span class="classRed">*</span><span id="idPhrPedNameMsg" class="classValidationMsg"></span></label>
										                           	<select name="phrPedName" class="classFormTextBox" id="idPhrPedName">
										                           		<option value=''>--</option>
										                           	<?php
										                           		$aPediatricians=get_pediatric_list();
										                           		// var_dump($aPediatricians);
										                           		foreach ($aPediatricians as $key => $value) {
										                           			// var_dump($value['emr_peduser_id']);
										                           			echo "<option value='{$value['emr_peduser_id']}'>{$value['ped_doc_name']}</option>";
										                           		}
										                           	?>
										                            </select>
								                            	</div>
								                            </div>
								                            <div class="classW6 classFL">
													    		<div class="classW11 classFL">
													    			<label for="idPhrCash">Cash <span class="classRed">*</span><span id="idPhrCashMsg" class="classValidationMsg"></span></label>
										                            <input type="text" name="phrCash" class="classFormTextBox" id="idPhrCash" value="250"/>
													    		</div>
													    	</div>
													    </div>
													    <div class="classW12 classFL classXBMargin">
													    	<div class="classW6 classFL">
													    		<div class="classW11 classFL">
													    			<label for="idPhrNote">Note <span id="idPhrNoteMsg" class="classValidationMsg"></span></label>
										                            <textarea name="phrNote" class="classFormTextBox" id="idPhrNote" style="min-height:100px;"></textarea>
													    		</div>
													    	</div>
													    	<div class="classW6 classFL">
													    		<div class="classW11 classFL classXBMargin">
															    	<div class="classW6 classFL">
														    			<label for="idPhrPayDate">Date Collected<span id="idPhrPayDateMsg" class="classValidationMsg"></span></label>
											                            <input type="text" name="phrPayDate" class="classFormTextBox" id="idPhrPayDate" value="<?php echo date("d-m-Y"); ?>"/>
														    		</div>
															    	<div class="classW6 classFL">
														    			<label for="idPhrPayTime">Time Collected<span id="idPhrContactMsg" class="classValidationMsg"></span></label>
											                            <input type="text" name="phrPayTime" class="classFormTextBox" id="idPhrPayTime" value="<?php echo date('h:i a', time()); ?>"/>											                            
															    	</div>
															    </div>
															    <div class="classW11 classFL classTAR">											    		
												    				<!-- <a href="<?php echo $sRedirect; ?>"><button type="button" name="phrSubmit" id="idPhrSubmit" value="phrSubmit" class="classGoButton"> Pay Online ►</button></a> -->
												    				<input type="submit" name="phrCashSubmit" id="idPhrCashSubmit" value="Submit" class="classGoButton">
													    		</div>
														    </div>
														</div>
												    </form>
											  	</div>	
											</div>
											<div id="tabs-2">
											    <div class="ui-tab-inner-div">											    												    	
											    	<a href="<?php echo $sRedirect; ?>"><button type="button" class="classGoButton"> Pay Now ►</button></a>
											    </div>
											</div>
			    						</div>
						           </div> 
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
				<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
				<script src="//code.jquery.com/jquery-1.10.2.js"></script>
				<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  				<!-- Tab script -->
				 <script>
					  $(function() {
					    $( "#tabs" ).tabs({
					      collapsible: true
					    });
					  });					  
				  </script>
				  <!-- Auto Complete script -->
				  <script>
					  (function( $ ) {
					    $.widget( "custom.combobox", {
					      _create: function() {
					        this.wrapper = $( "<span>" )
					          .addClass( "custom-combobox" )
					          .insertAfter( this.element );
					 
					        this.element.hide();
					        this._createAutocomplete();
					        this._createShowAllButton();
					      },
					 
					      _createAutocomplete: function() {
					        var selected = this.element.children( ":selected" ),
					          value = selected.val() ? selected.text() : "";
					 
					        this.input = $( "<input>" )
					          .appendTo( this.wrapper )
					          .val( value )
					          .attr( "title", "" )
					          .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					          .autocomplete({
					            delay: 0,
					            minLength: 0,
					            source: $.proxy( this, "_source" )
					          })
					          .tooltip({
					            tooltipClass: "ui-state-highlight"
					          });
					 
					        this._on( this.input, {
					          autocompleteselect: function( event, ui ) {
					            ui.item.option.selected = true;
					            this._trigger( "select", event, {
					              item: ui.item.option
					            });
					          },
					 
					          autocompletechange: "_removeIfInvalid"
					        });
					      },
					 
					      _createShowAllButton: function() {
					        var input = this.input,
					          wasOpen = false;
					 
					        $( "<a>" )
					          .attr( "tabIndex", -1 )
					          .attr( "title", "Show All Items" )
					          .tooltip()
					          .appendTo( this.wrapper )
					          .button({
					            icons: {
					              primary: "ui-icon-triangle-1-s"
					            },
					            text: false
					          })
					          .removeClass( "ui-corner-all" )
					          .addClass( "custom-combobox-toggle ui-corner-right" )
					          .mousedown(function() {
					            wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					          })
					          .click(function() {
					            input.focus();
					 
					            // Close if already visible
					            if ( wasOpen ) {
					              return;
					            }
					 
					            // Pass empty string as value to search for, displaying all results
					            input.autocomplete( "search", "" );
					          });
					      },
					 
					      _source: function( request, response ) {
					        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
					        response( this.element.children( "option" ).map(function() {
					          var text = $( this ).text();
					          if ( this.value && ( !request.term || matcher.test(text) ) )
					            return {
					              label: text,
					              value: text,
					              option: this
					            };
					        }) );
					      },
					 
					      _removeIfInvalid: function( event, ui ) {
					 
					        // Selected an item, nothing to do
					        if ( ui.item ) {
					          return;
					        }
					 
					        // Search for a match (case-insensitive)
					        var value = this.input.val(),
					          valueLowerCase = value.toLowerCase(),
					          valid = false;
					        this.element.children( "option" ).each(function() {
					          if ( $( this ).text().toLowerCase() === valueLowerCase ) {
					            this.selected = valid = true;
					            return false;
					          }
					        });
					 
					        // Found a match, nothing to do
					        if ( valid ) {
					          return;
					        }
					 
					        // Remove invalid value
					        this.input
					          .val( "" )
					          .attr( "title", value + " didn't match any registered pediatric name" )
					          .tooltip( "open" );
					        this.element.val( "" );
					        this._delay(function() {
					          this.input.tooltip( "close" ).attr( "title", "" );
					        }, 2500 );
					        this.input.autocomplete( "instance" ).term = "";
					      },
					 
					      _destroy: function() {
					        this.wrapper.remove();
					        this.element.show();
					      }
					    });
					  })( jQuery );
 
					  $(function() {
					    $( "#idPhrPedName" ).combobox();
					    $( "#toggle" ).click(function() {
					      $( "#idPhrPedName" ).toggle();
					    });
					  });
					</script>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




