<meta charset="utf-8">

<meta name="keywords" content="The Pediatric Network">
<meta name="description" content="The Pediatric Network">

<link type="text/css" href="css/style.css" rel="stylesheet"></link><!--Main CSS-->

<!--Scroll Top Plugin Start-->
  <link rel="shortcut icon" type="image/x-icon" href="images/pediatric_fevicon.ico">
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/sequence.jquery-min.js"></script>    
<script type="text/javascript" src="js/jquery-scrollToTop.js"></script>
<link rel="stylesheet" href="css/scrollToTop.css"><!--CSS for Scrolling page to top.-->
<link rel="stylesheet" href="css/easing.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">  
  <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!---->  

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  
  
<!-- Google Analytics Tracking Code Start -->  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50435263-1', 'thepediatricnetwork.com');
  ga('send', 'pageview');

</script>
<!-- Google Analytics Tracking Code End -->  
  

<!---->

<script type="text/javascript">

$(document).ready(function($) {
$('body').scrollToTop({
speed: 1500,
easing: 'easings', // CSS easings
distance: 200,
text: 'Scroll To Top',
animation: 'fade', // fade, slide, none
animationSpeed: 100, 
skin: 'cycle', // default, cycle, square, text or triangle
namespace: 'scrollToTop'
});
});
</script><!--Scroll Top Plugin End-->



	<!--<link rel="stylesheet" href="stylesheets/style1.css">-->

	
	<!--[if lt IE 9]><link rel="stylesheet" type="text/css" media="screen" href="stylesheets/sequencejs-theme.sliding-horizontal-parallax-ie.css" /><![endif]-->

		
	<!-- JS
  ================================================== -->	
	
	

	<!--<script type="text/javascript" src="js/jquery.flexslider-min.js"></script> -->
	
	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

   
<script type="text/javascript">
$(document).ready(function($) {
function deselect() {
    $(".pop").slideFadeToggle(function() { 
        $("#idLogin").removeClass("selected");
    });    
}

$(function() {
    $("#idLogin").live('click', function() {
        if($(this).hasClass("selected")) {
            deselect();               
        } else {
            $(this).addClass("selected");
            $(".pop").slideFadeToggle(function() { 
                $("#idUsername").focus();
            });
        }
        return false;
    });

    $(".close").live('click', function() {
        deselect();
        return false;
    });
});

$.fn.slideFadeToggle = function(easing, callback) {
    return this.animate({ opacity: 'toggle', height: 'toggle' }, "fast", easing, callback);
};
});
</script><!-- Login Pannel-->

<!--Script for Maps-->


<!--Script for Maps End -->

<?php include('function.php'); ?>