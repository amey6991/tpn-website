<!--Main Footer Div Start-->
			<div class="classDivMainFooter">
				<div class="classDivMainFooter1">
					<div class="classDivFooterPannelHead">
						<div class="classSpanFooterPannelHead" style="min-width:40%!important;margin-left: 40px;">Navigation</div>	
						<div class="classSpanFooterPannelHead" style="margin-left: 15px;">Benefits</div>	
						<div class="classSpanFooterPannelHead">Contact</div>	
					</div>
					<div class="classDivFooterPannel" style="">
						<span class="classSpanFooterItem"><a href="index.php">Home</a></span>
						<span class="classSpanFooterItem"><a href="about.php">About</a></span>
						<span class="classSpanFooterItem"><a href="forums.php">Forums</a></span>
						<span class="classSpanFooterItem"><a href="faqs.php">FAQS</a></span>
						<span class="classSpanFooterItem"><a href="testimonials.php">Testimonials</a></span>
						<span class="classSpanFooterItem"><a href="contact.php">Contact</a></span>
					</div>
					
					<div class="classDivFooterPannel">
						
						<span class="classSpanFooterItem"><a href="partner-listing.php">Partner Listing</a></span>
						<span class="classSpanFooterItem"><a href="leadership-team.php">Leadership Team</a></span>
						<span class="classSpanFooterItem"><a href="advisory-board.php">Advisory Board</a></span>
						<span class="classSpanFooterItem"><a href="become-a-partner.php">Become a Partner</a></span>
						<span class="classSpanFooterItem"><a href="patient-sign-up.php">Membership Plans</a></span>
					</div>
					<div class="classDivFooterPannel">
						<span class="classSpanFooterItem classDivBenefitsInFooter" style="margin-bottom: 25px;"><a href="img/brochure/parent_brochure.pdf" target="_blank"><img class="classImgBenefitsFooter" src="./images/patient-benefits.png"/>Member Patient</a></span>
						<span class="classSpanFooterItem classDivBenefitsInFooter" style="margin-bottom: 25px;"><a href="img/brochure/pharma-bro.pdf" target="_blank"><img  class="classImgBenefitsFooter" src="./images/pharmacist-benefits.png"/>Pharmacist</a></span>
						<span class="classSpanFooterItem classDivBenefitsInFooter" style="margin-bottom: 25px;"><a href="doctor-benefits.php"><img  class="classImgBenefitsFooter" src="./images/doctor-benefit.png"/>Doctor</a></span>						
					
						<!--
						<span class="classSpanFooterItem"><a href="#">Online CME Center</a></span>
						<span class="classSpanFooterItem"><a href="#">Clinical Vignettes</a></span>
						<span class="classSpanFooterItem"><a href="#">Drugs A-Z</a></span>
						<span class="classSpanFooterItem"><a href="#">Diseases & Conditions</a></span>
						<span class="classSpanFooterItem"><a href="#">Grand Rounds</a></span>
						<span class="classSpanFooterItem"><a href="#">Podcasts</a></span>
					-->
					</div>
					<div class="classDivFooterPannel classDivFooterPannel2">
						<span class="classSpanFooterItem">
							<div class="classFooterIcons" id="idDivAddInFooter">
								Indian Pediatric Network Pvt Ltd <br/>
D/404-405, Twin Arcade, <br/>
Opp. Blossom Society, <br/>
Military Road, Marol, <br/>
Andheri East, Mumbai 400059
							</div>
								
							<div class="classFooterIcons" id="idDivPhoneInFooter">
								9819620035, 02240054428 <!--29253707,<br/> 29253708-->
							</div>
						</span>
						
					</div>					
				</div>
				<div class="classDivMainFooter2">
					<span class="classSpanTermAndCondition classSpanLeftTermAndCondition">
						<a href="privacy-policy.php" target="_blank">Privacy Policy</a> | <a href="terms-and-condition.php" target="_blank">Terms & Conditions</a> | <a href="refund-policy.php" target="_blank">Refund Policy</a>
					</span>
					<span class="classSpanTermAndCondition classSpanRightTermAndCondition">
						Powered by : <a href="http://www.plus91.in" target="_blank">Plus91 Technologies Pvt. Ltd.</a>
					</span>

				</div>

			</div>