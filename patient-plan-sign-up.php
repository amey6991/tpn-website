<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();
?>

<html>
	<head>
		<title>Pediatric - Patient Plan Sign Up</title>		
	<?php include('header.php'); ?> 
<script type="text/javascript" src="./js/patient-signup.js"></script>
<script>
$(document).ready(function($) {
  $(function() {
    $( "#idPhrBdatenm" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy',
      minDate: "-100Y",
        maxDate: "-17Y",
        dateFormat: "dd/mm/yy",
       	yearRange: "c-100:c+17"  
    });
  });

});
</script>

<script type="text/javascript">
//Funtion to hide Element
function hideDiv(elementId){	
	var id="#"+elementId;	
	  $(id).hide();	
}

</script>
<script>
    function get_pincode() {
        var searchBy = document.getElementById("idArea").value;
        
        //document.getElementById("idPhrArea").value=searchBy;
        //alert(searchBy);
        if(searchBy=="select"){
        	alert("Select Area");
        	//noAction(searchBy);
        	
        }
        var request = $.ajax({
            url: "ajaxFunction.php",
            type: "POST",    
            data: {callTo: "pincode", searchBy:searchBy},        
            dataType: "html"
        });
 
        request.done(function(msg) {
            $("#idDisplayPin").html(msg);          
        });
 
        request.fail(function(jqXHR, textStatus) {
            alert( "Request failed: " + textStatus );
        });
    }

    
</script>
<script type="text/javascript">
function checkUser(){
    	
    	var searchBy = document.getElementById("idPhrEmail").value;
    	//alert(searchBy);
    	var request = $.ajax({
            url: "ajaxFunction.php",
            type: "POST",    
            data: {callTo: "checkUser", searchBy:searchBy},        
            dataType: "html"
        });
 
        rs=request.done(function(msg) {
        	//alert(msg);
        	if(msg==1){
        		var result=validateEmail("idPhrEmail");
        		//alert(result);
        		if(result==1){
            		document.getElementById("idPhrEmailMsg").innerHTML='<span class="classGreen">Available</span>';          
            	}else{
            		if(searchBy==''){
            			document.getElementById("idPhrEmail").value='';   
            			document.getElementById("idPhrEmailMsg").innerHTML='<span class="classRed">Please Enter Email</span>';          
            		}else{
            			document.getElementById("idPhrEmail").value='';   
            			document.getElementById("idPhrEmailMsg").innerHTML='<span class="classRed">Please Enter Valid Email</span>';          
            		}

            	}
        	}else{
        		if(searchBy==''){

        			document.getElementById("idPhrEmail").value='';   
        			document.getElementById("idPhrEmailMsg").innerHTML='<span class="classRed">Please Enter Email</span>';       	        	
        		}else{
        			document.getElementById("idPhrEmail").value='';          	
        			document.getElementById("idPhrEmailMsg").innerHTML='<span class="classRed">Email Already Exist</span>';          		
        		}

        	
        	}
        }); 		
        request.fail(function(jqXHR, textStatus) {
            alert( "User Already Existed" + textStatus );
        });
    }

</script>


	</head>

	<body>

<?php 
$aLocation=array();
$aPincode=array();
$aLocation=get_location();
$aCity=getCity();
$aState=displayState();
$aCountry=displayCountry();
//$aPincode=get_pincode();
?>		
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					

					<div id="example" class="classDivTabContainerCustom">
			           
<?php

/*
In first time page request take plan from the URL.
In second time page request in the same session then take plan from session variable
*/

$iInvalidCapcha=null;
$plan=null;
$planName=null;
if(isset($_GET['plan'])){
$plan=$_GET['plan'];
$_SESSION['patientPlan']=$plan;
}else{  
	$plan=null;	
}
// Check for the Patient plan.
if(isset($_SESSION['patientPlan'])){
	$plan=$_SESSION['patientPlan'];	
}else{  
	$plan=null;
}
// Check for the incorrect Captcha code, If incorrect show message else not.
if(isset($_SESSION['capchaCode'])){
	$iInvalidCapcha=$_SESSION['capchaCode'];
}else{  
	$iInvalidCapcha=1;
}
$sCaptionName='';

// if($plan=='Gold'){
// 	$plan=1;
// 	$planName='Gold';
// 	$childPrivilege='multiple';
// $sCaptionName='multiple';
// }

/*
Gold Plan rename as Basic on: 29-9-2015
*/
if($plan=='Basic'){
	$plan=1;
	$planName='Basic';
	$childPrivilege='multiple';
$sCaptionName='multiple';
}elseif($plan=='Silver'){
	$plan=2;
	$planName='Silver';
	$childPrivilege='single';
$sCaptionName='Silver';
}if($plan=='Bronze'){
	$plan=3;
	$planName='Bronze';
	$childPrivilege='single';
$sCaptionName='Bronze';

}if($plan=='Test'){
	$plan=4;
	$planName='Test';
	$sCaptionName="Health Check-up";
	$childPrivilege='single';
}

 ?>
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >
			            	<div class="classErrorMsg" id="idErrorMsg" onClick="hideDiv('idErrorMsg');">
			            		<?php 
			            			echo $iInvalidCapcha==0?"Please Enter Correct Security Code":'';
			            			$_SESSION['capchaCode']=1;
			            		?>
		            		</div>
			              	<div class="classDivHeading" >
			              		Patient Sign Up <span class="classSpanMembershipPlan">[ Membership Plan ► <?php echo '<b>'.$sCaptionName.'</b>'; ?> ] </span>
			              	</div>
				              	
			                <div class="classDivInnerPageBodyContainer">
			              	   <div style="width:100%;margin-top:-30px;">
							<!--Note for developer : For making fields mandatory remove last 2 char (i.e. nm) from the ID  -->
					              <form name="phrAddFrm"  id="idPhrAddFrm" method="post" enctype="multipart/form-data" action="add-patient-detail-before-payment.php"  role="form">
		            				   	<input type="hidden" type="text" name="childPrivilege" value="<?php echo $childPrivilege; ?>" />
						                <input type="hidden" type="text" name="phrPlan" value="<?php echo $plan; ?>" />						                
						                <input type="hidden" type="text" name="phrType" value="yes" />
						          
						            <div class="classP48Float">
						           		 <div class="classDivFormSubHeading">Parents Detail</div>
						                <p class="class95Percent">
			                            <label for="name-sparent">Name of Signing Parent <span class="classRed">*</span><span id="idPhrNameMsg" class="classValidationMsg"></span></label>
			                            <input type="text" name="phrName" class="classFormTextBox" id="idPhrName" />
			                            </p>

			                            <p class="class95Percent">
			                            <label for="mobile-sparent">Contact <span class="classRed">*</span><span id="idPhrContactMsg" class="classValidationMsg"></span></label>
			                            <input type="text" name="phrContact" class="classFormTextBox" id="idPhrContact" />
			                            </p>

			                            <p class="class95Percent">
			                            <label for="emai-sparent">Email Id <span class="classRed">*</span> <span id="idPhrEmailMsg" class="classValidationMsg"></span></label>
			                            <input type="text" name="phrEmail" onBlur="checkUser();" class="classFormTextBox" id="idPhrEmail" />
			                            </p>

			                            <p class="class95Percent">
			                            <label for="emai-sparent">UID (Unique Identification)  <span id="idPhrUIDMsg" class="classValidationMsg"></span></label>
			                            <input type="text" name="phrUID" class="classFormTextBox" id="idPhrUIDnm" />
			                            </p>

			                            <p class="class95Percent">
			                            <label for="dob-sparent">Date Of Birth <span id="idPhrBdateMsg" class="classValidationMsg"></span></label>
			                            <input type="text" name="phrBdate" id="idPhrBdatenm" class="timepicker classFormTextBox" readonly="true" placeholder="DD-MM-YYYY" />
			                            </p>

			                            <p class="class95Percent">
			                            <label for="gender-sparent">Gender <span class="classRed">*</span> <span id="idPhrGenderMsg" class="classValidationMsg"></span></label>
			                            <input type="radio" name="phrGender" id="idPhrMGendernm" value="Male" />Male
			                                <input type="radio" name="phrGender" id="idPhrFGender" value="Female" />Female                                
			                            </p>								                
						                

						            </div>

						            <div class="classP48Float" style="margin-left:35px;">
						           		<div class="classDivFormSubHeading">Address Detail </div>				            
						                
						                <p class="class95Percent">
						                <label for="address">Address  <span class="classRed">*</span><span id="idPhrAddressMsg" class="classValidationMsg"></span></label>
						                <textarea name="phrAddress" id="idPhrAddress" class="classTextAreaW100perH120px classFormTextBox"></textarea>
						                </p>
						            	
						                <p class="class95Percent">
							            	<div class="classP48Float">
							                
							               	 <label for="city ">City <span id="idCityMsg" class="classValidationMsg"></span></label>
							                	<select id="idCitynm" name="city" class="classFormTextBox">
							                		<option value="select">Select</option>
							                		<?php							                				                					
							                			foreach ($aCity as $key){
							                				echo "<option value={$key[0]}>{$key[2]}</option>";
							                			}
							                		 ?>							                								                					                		
							                	</select>
							                
							            	</div>
						            	</p>
									
										<p class="class95Percent">
											<div class="classP48FloatLMargin">										
							               	 <label for="area">Area <span id="idAreaMsg" class="classValidationMsg"></span></label>
							                	<input id="idAreanm" name="phrArea" class="classFormTextBox" >
							               	 <!--
							                	<select id="idArea" name="phrArea" class="classFormTextBox" onchange="get_pincode();">
							                		<option value="select" >Select</option>
							                		<?php	                					
							                	//		foreach ($aLocation as $key){
							                	//			echo "<option value={$key[0]}>{$key[2]}</option>";
							                		//	}
							                		 ?>							                		
							                	</select>
							                -->
							                	<!--<input type="hidden" id="idPhrArea" name="phrArea" value="" />-->
							                </div>
						            	</p>

										

										<p class="classP48Float">
							            	<div class="classP48Float">
								               	 <label for="pincode">Pincode <span class="classRed">*</span><span id="idPincodeMsg" class="classValidationMsg"></span></label>
								                	<input type="text" id="pin-code" name="pincode" class="classFormTextBox" >
								                	<!--
								                	<select id="pin-code" name="pincode" class="classFormTextBox">
								                		<option value="select">Select</option>
								                		<?php							                				                					
								                		//		foreach ($aPincode as $key){
								                		//		echo "<option value={$key[1]}>{$key[1]}</option>";
								                		//	}
								                		 ?>							                		
								                	</select>
								                -->
								            </div>
								        </p>

								        <p class="class95Percent">
							            	<div class="classP48FloatLMargin">						            	
							                <label for="state">State <span class="classRed">*</span><span id="idStateMsg" class="classValidationMsg"></span></label>
							                	
							                	<select id="idState" name="state" class="classFormTextBox">
							                		<option value="select" selected="selected">Select</option>
							                		<?php							                				                					
							                			foreach ($aState as $key){
							                				echo "<option value={$key[0]}>{$key[1]}</option>";
							                			}
							                		 ?>							                		
							                	</select>						                
							            	</div>
						            	</p>

						            	<div class="classP48Float">
						                <p>
						                <label for="country">Country <span id="idCountryMsg" class="classValidationMsg"> </span></label>
						                	<select id="idCountrynm" name="country" class="classFormTextBox">
						                		<?php							                				                					
						                			foreach ($aCountry as $key){
						                				echo "<option value={$key[0]}>{$key[1]}</option>";
						                			}
						                		 ?>							                		
						                	</select>
						                </p>
						            	</div>
						           
						            </div>
						            <div class="classP48">							            
										<p class="class95Percent">
											<label for="captcha_code">Secure code<span class="classRed">*</span><span id="idPhrCapchaMsg" class="classValidationMsg"></span></label>
											<input type="text" name="captcha_code" class="classFormTextBox" size="10" id="idPhrCapcha" maxlength="6" />
										</p>
										<p class="class95Percent">
						            		<img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image"  style="border:1px solid #aaa;"/>
											<a href="#" onclick="document.getElementById('captcha').src = '/securimage/securimage_show.php?' + Math.random(); return false"><img style="width:25px; margin:20px;" src="securimage/images/refresh.png" /></a>										
										</p>
						            </div>
						            <div class="classP48Float">				                            						            
						            	
						                <p>
						                <button type="submit" name="phrSubmit" id="idPhrSubmit" value="phrSubmit" class="classGoButton"> Submit</button>
						                <div id="required">
											<p><span class="classRed">*</span> Required Fields</p>
										</div>

			    					</div>
			    					
						               
						                
						            </form>
						        </div> 
								
						
							</div>

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




