<div id="slider_holder" class="sixteen columns" style="border-bottom: 1px solid #004480;padding-bottom:20px;margin-bottom:20px;">
                <div id="sequence" class="classTestimonialSequence">
                    <div class="prev" ><span></span></div>
                    <div class="next" ><span></span></div>
    
                    <ul style="list-style:none!important;">
                        <!-- Layer 1 -->
                        <li>
                            <div class="info animate-in classTInfo">
                                <h4 class="classTTextInText">Dr Sagar Chaudhari</h4>                              
                                <p class="classTTextInText">                                    
                                   "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.                                   
                                </p>   
                                <span><a class="link class10Padding" href="#">Watch Video ► </a></span>                        
                            </div>
                            <img class="slider_bgr classCTImg" src="images/testimonial/Dr_Ashutosh.png" />

                        </li>                       
                        
                        <!-- Layer 2 -->
                        
                        <li>
                            <div class="info animate-in classTInfo">
                                <h4 class="classTTextInText">Dr Deshmukh</h4>                              
                                <p class="classTTextInText">                                    
                                    "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. parameter with ease.
                                </p> 
                                <span class="classSpanSignInButtonWatch"><a class="link class10Padding" href="#"> Video ► </a></span>                        
                            </div>
                            <img class="slider_bgr classCTImg" src="images/testimonial/Dr_Kulkarni.png"/>
                            <!--<img class="slider_img" src="images/2_2.png"/> -->
                        </li>
                        
                       
                        <li>
                            <div class="info  animate-in classTTextInText classTInfo">
                                <h4 class="classTTextInText">Dr Sheetal Pande</h4>                              
                                <p class="classTTextInText">                                    
                                    "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.
                                </p>       
                                <span class="classSpanSignInButtonWatch"><a class="link class10Padding" href="#">Watch Video ► </a></span>                        
                            </div>
                            <img class="slider_bgr classCTImg" src="images/testimonial/Dr_swati.png"/>
                            
                        </li>
                        
                                    
                    </ul>
                </div>
            </div>
            <!-- Sequence Slider::END-->
            




<script type="text/javascript"> 


            var preloader = ($.browser.msie) ? false : true ;
                
            $(document).ready(function(){
                var options = {
                    autoPlay: true,
                    autoPlayDelay: 10000,
                    nextButton: true,
                    prevButton: true,
                    preloader: preloader,
                    animateStartingFrameIn: true,
                    transitionThreshold: 500,                    
                    direction:"none",
                    fallback: {
                        theme: "easing",
                        speed: 0
                    }
                };
                
                var sequence = $("#sequence").sequence(options).data("sequence");
    
                sequence.afterLoaded = function(){
                    //$(".info").css('display','block'); // To display Next and Previous Buttons
                    $(".info").css('display','block');
                    $("#sequence").hover(
                            function() {
                                $(".prev, .next").stop().animate({opacity:0.7},300);                
                            },
                            function() {        
                                $(".prev, .next").stop().animate({opacity:0},300);
                            }
                    );
                    
                    $(".prev, .next").hover(
                            function() {
                                $(this).stop().animate({opacity:1},200);                
                            },
                            function() {        
                                $(this).stop().animate({opacity:0.7},200);
                            }       
                    );
                }
            })

        </script>