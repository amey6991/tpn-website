<?php

/* Loading configuration for app */
require_once 'config/config.php';

//Laoding Database constants for the App
require_once 'config/config.db.php';

//Loading Database Class 
require_once 'classes/class.DBConnManager.php';

// Loading PHR class file
require_once 'classes/class.PhrUser.php';

// Loading Emr class file
require_once 'classes/class.EmrUser.php';

// Loading common functions
require_once 'commonFunctions.php';


$pedIpAddress   = $_POST['ip_address'];
$pedFirstName 	= isset($_POST['pedFirstName'])?inputPost($_POST['pedFirstName']):'';
$pedLastName	= isset($_POST['pedLastName'])?inputPost($_POST['pedLastName']):'';
$pedName        = $pedFirstName.' '.$pedLastName;
$pedSpecializtn = isset($_POST['pedSpecializtn'])?inputPost($_POST['pedSpecializtn']):'';
$pedStatus      = 1;
$pedMCICode     = isset($_POST['pedMCICode'])?inputPost($_POST['pedMCICode']):'';
$pedGender      = isset($_POST['pedGender'])?$_POST['pedGender']:'';
$pedDOB         = isset($_POST['pedDOB'])?date('Y-m-d',strtotime($_POST['pedDOB'])):'';
$pedAddress     = isset($_POST['pedAddress'])?inputPost($_POST['pedAddress']):'';
$pedCity		= isset($_POST['pedCity'])?$_POST['pedCity']:'';
$pedArea        = isset($_POST['pedArea'])?inputPost($_POST['pedArea']):'';
$pedZone        = isset($_POST['pedZone'])?$_POST['pedZone']:'';
$pedCountry     = isset($_POST['pedCountry'])?$_POST['pedCountry']:'';
$pedState       = isset($_POST['pedState'])?$_POST['pedState']:'';
$pedPincode     = isset($_POST['pedPincode'])?inputPost($_POST['pedPincode']):'';
$pedEmailid     = isset($_POST['pedEmailid'])?inputPost($_POST['pedEmailid']):'';
$pedMobileNo    = isset($_POST['pedMobileNo'])?inputPost($_POST['pedMobileNo']):'';
$pedCreatdDate  = date('Y-m-d');
$pedApproved    = 0;

$sMasterTbl = 'tpn_user_signup_request';

$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class;
$conn   = $DBMan->getConnInstance(); // Requiring connection insatnce

$sQuery = "INSERT INTO `{$sMasterTbl}`(`signup_req_date`,`signup_ip`,`signup_status`,`emr_name`,
	                    `emr_specialization`,`emr_mci_code`,`emr_gender`,`emr_date_of_birth`,
	                    `emr_address`,`emr_area`,`emr_country_id`,`emr_state_id`,`emr_city_id`,`emr_pincode`,`emr_contact_no`,`emr_email`,`signup_approved`)
           VALUES('{$pedCreatdDate}','{$pedIpAddress}',{$pedStatus},'{$pedName}',
           	      {$pedSpecializtn},'{$pedMCICode}','{$pedGender}','{$pedDOB}',
           	      '{$pedAddress}','{$pedArea}','{$pedCountry}','{$pedState}','{$pedCity}','{$pedPincode}','{$pedMobileNo}','{$pedEmailid}','{$pedApproved}')";
$insertReq = $conn->query($sQuery);

if($addPhrResult){
	$result['status'] = 1;
	$result['msg']    = 'Registration Succesfull';
	echo json_encode($result);
}else{
	$result['status'] = 0;
	$result['msg']    = 'Error Occured';
	echo json_encode($result);
}


?>