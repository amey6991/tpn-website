<?php

include('function.php');

//check_user("laxmikant@lamikant.comfdf");
if(isset($_POST)){
	
	$sCallTo = $_POST['callTo']; 
	$sSearchBy= $_POST['searchBy'];

	if($sCallTo=='pincodezone'){
		echo get_pincode_zone_display($sSearchBy);
	}else if($sCallTo=='pincode'){		
		echo get_pincode_display($sSearchBy);
	}else if($sCallTo=='checkUser'){		
		echo check_user($sSearchBy);
		
	}else if($sCallTo=='checkDoctor'){		
		echo check_doctor($sSearchBy);
		
	}

}else{
	$sCallTo = null; 
	$sSearchBy= null; 
}

/*
Function called through Ajax. 
Function for displaying the zone and pincode.
*/
function get_pincode_zone_display($sSearchBy = 'select'){ 		
	$aPincode=array();
	if($sSearchBy=='select'){
		$aPincode=null;
		$sLocationId=null;	
		$sLocation=null;
		$sLocationId=null;
		$sLocationZone=null;
	}else{
		$aPincode=get_pincode($sSearchBy);
		$sLocationId=null;	
		$sLocation=$aPincode[0];
		

		$sLocationId=$sLocation[0];
		$sLocationZone=$sLocation[2];
	}
	

	$sPincodeText=preg_replace('/\s/', '', $sLocation[1]);

	$aSymbol=array(",","(",")",".","|",":"," ","-");
	$sPincodeText=str_replace($aSymbol, $aSymbol[0], $sPincodeText);	
	$aPincodeArray=explode($aSymbol[0], $sPincodeText);
	

	echo '<span class="classP48FloatWithClear"><p class="classP48Float" id="idDisplayZone">';
	echo '<label for="zone">Zone </label>
			<input id="zone" type="text" name="zone" value="'.$sLocationZone.'" class="classFormTextBox"/>'; 
    echo '</p>';
	
	echo '<p class="classP48FloatLMargin" id="idDisplayPincode">
						<label for="pin-code">Pincode<span class="classRed">*</span> <span id="idPincodeMsg" class="classValidationMsg"></span></label>
				    	<select id="pin-code" name="pincode" class="classFormTextBox">
				    	<option value="select">Select</option>';
				    			foreach ($aPincodeArray as $key){
				        			echo "<option value={$key}>{$key}</option>";
				        		}
	echo '</select></p></span>';				        		

    
}


/*
Function called through Ajax. 
Function for displaying the pincode.
*/
function get_pincode_display($sSearchBy){ 		
	$aPincode=array();
	if($sSearchBy=='select'){
		$aPincode=null;
		$sLocationId=null;	
		$sLocation=null;
		$sLocationId=null;
		$sLocationZone=null;
	}else{
		$aPincode=get_pincode($sSearchBy);
		$sLocationId=null;	
		$sLocation=$aPincode[0];		
		$sLocationId=$sLocation[0];
		$sLocationZone=$sLocation[2];
	}
	$sPincodeText=preg_replace('/\s/', '', $sLocation[1]);

	$aSymbol=array(",","(",")",".","|",":"," ","-");
	$sPincodeText=str_replace($aSymbol, $aSymbol[0], $sPincodeText);	
	$aPincodeArray=explode($aSymbol[0], $sPincodeText);


	echo '<p class="" id="idDisplayPincode">
			<label for="pin-code">Pincode<span class="classRed">*</span> <span id="idPincodeMsg" class="classValidationMsg"></span></label>
	    	<select id="pin-code" name="pincode" class="classFormTextBox">
	    	<option value="select">Select</option>';
	    			foreach ($aPincodeArray as $key){
	        			echo "<option value={$key}>{$key}</option>";
				        		}
	echo '</select></p>';				        		

    
}
  

/*
Function called through Ajax. 
Function for Checking the username availability for the user.
Function restrict user to create the duplicate signup.
*/
function check_user($sSearchBy){

	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
	//$conn=db_connect('tpn');
	$conn   =  $DBMan->getConnInstance(); // Requiring connection instance

	$sUsername = $sSearchBy;

	$sQuery = "SELECT `phr_mem_email_signinparent` FROM `tpn_phr_member_info` WHERE `phr_mem_email_signinparent` = '$sUsername'";
	
	$result = $conn->query($sQuery);

	$row = $result->fetch_array();
	
	if($sUsername == $row['phr_mem_email_signinparent']){
		return 0;
	}
	else{

		return 1;
	}  
}

/*
Function called through Ajax. 
Function for Checking availability of email address for the doctor.
This function restrict to create a duplicate signup
*/
function check_doctor($sSearchBy){

	$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
	//$conn=db_connect('tpn');
	$conn   =  $DBMan->getConnInstance(); // Requiring connection instance

	$sUsername = $sSearchBy;

	$sQuery = "SELECT `ped_doc_email` FROM `tpn_emr_ped` WHERE `ped_doc_email` = '$sUsername'";
	//var_dump($sQuery);
	$result = $conn->query($sQuery);
	$row = $result->fetch_assoc();
	
	if($sUsername == $row['ped_doc_email']){
		return 0;
	}
	else{

		return 1;
	}  
}
?>
