<html>
	<head>
		<title>Pediatric - Payment Fail</title>		
	<?php include('header.php'); ?> 


	</head>
<?php


$aFields =array();
$sFields=null;
if(isset($_GET['msg'])){ // Pass id for retrieving data before payment.
	$sIdBeforePayment=$_GET['paymentid'];
	$sMsg=$_GET['msg'];
	$sIdBeforePayment=decriptUniqueCode($sIdBeforePayment); // Decript the ID
	$sMsg=decriptUniqueCode($sMsg); // Decript the Message

	$aFields=showDataBeforePayment($sIdBeforePayment);	
	
}else{
	$aFields=empty($aFields);
	$sIdBeforePayment='';
}

?>
	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          <!-- 
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Sorry!
			              </div>	
			              <div class="classDivTextWrapper">			              		
			              		<p class="classPLeaderText classPMsgWrapper" style="color:#222 !important;">			              			
				              		Sorry you could not complete the sign up process. Please <a href='phr-patient-detail-payment.php?id=<?php echo $sIdBeforePayment; ?>' >try again</a> or contact support at support@thepediatricnetwork.com

				               	</p>				              	
							</div>	 

							<div class="classDivTextWrapper" style="width:80%;">			              		
			              		<h3 style="color:#F80;">The features you can access once you login in are:</h3>

		              			<ul class="classUlFeatureText">
									<li>Taking an Appointment / Chatting with our portal doctor at anytime</li>
									<li>Finding a Pediatrician close to you</li>
									<li>Authorizing a Pediatrician to see your child's reports</li>
									<li>Uploading reports and growth milestones at one single location.</li>
									<li>Access to the complete history of your child's health journey</li>
									<li>Access to our specially created hand outs and education material.</li>
									<li>Access to our pharmacy network for discounts, home delivery and paperless prescriptions.</li>
								</ul>
								
		              		</div>	            

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




