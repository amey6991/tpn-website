<html>
	<head>
		<title>Pediatric - Contact Us</title>		
	<?php include('header.php'); ?> 
<style>
#idActiveMenu6{
	color: #004480 !important;
  border-bottom-color:#004480 !important;
}
#idActiveMenu6 a{
color: #004480 !important;
  border-bottom-color:#004480 !important;
}
</style><!--Style for the active Link-->

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Contact
			              </div>		             
						 <div class="classDivContactTextWrapper">
			              	
							<div class="classFooterIcons">
								<span class="classDivHomeIcon classContactIcon classAddressMargin"></span>
								<span class="classDivContactText ">
									Indian Pediatric Network Pvt Ltd <br/>
D/404-405, Twin Arcade, <br/>
Opp. Blossom Society, <br/>
Military Road, Marol, <br/>
Andheri East, Mumbai 400059
									
								</span>
								
							</div>
								
							<div class="classFooterIcons">
								<span class="classDivPhoneIcon classContactIcon"></span>
								<span class="classDivContactText">	
									02240054428
								</span>								
								<span class="classDivPhoneIcon classContactIcon"></span>
								<span class="classDivContactText">	
									9819620035 
								</span>

								<!--<span class="classDivPhoneIcon classContactIcon"></span>
								<span class="classDivContactText">	
									29253707
								</span>
								
								<span class="classDivPhoneIcon classContactIcon"></span>
								<span class="classDivContactText">	
									29253708
								</span>-->
							</div>
							
			              </div>  
			              <div class="classDivContactTextWrapper">
			              	<iframe src="https://www.google.com/maps/d/embed?mid=zz5nise28tow.k9lscG-S-eko" width="500" height="380"></iframe>			              	
			              </div>						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




