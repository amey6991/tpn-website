<!DOCTYPE html>
<html>
	<head> 
		<title>Pediatric - Home</title>		
	<?php include('header.php'); ?> 

<style>
#idActiveMenu1{
	color: #004480 !important;
  border-bottom-color:#004480 !important;
}
#idActiveMenu1 a{
color: #004480 !important;
  border-bottom-color:#004480 !important;
}
</style><!--Style for the active Link-->
<script type="text/javascript">

function validate_type(){

	if( document.search_form.stype.value == "all" )
	   {
    	 alert( "Please Select For - Hospital | Clinic | Pharmacist" );
     	return false;
	   }
   	return( true );
}
</script>
<link rel="stylesheet" href="css/lightbox.css">

	<script src="js/lightbox.min.js"></script>

	<script>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-2196019-1']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader">
				
				<div class="classDivMainHeader1">
					
					<?php include('body-header.php'); ?>

				<div class="classMainSliderDiv">
			  		<!--<img class="classBannerImg" src="images/req/banner3.png" width="100%;position:relative;"/>
					<div class="classDivInfo">
								<p class="">
									<img class="classImgStartQuote" src="images/req/start.png"/>
									<span class="classSpanStartQuoteText"><strong>From your baby's first cry till puberty, </br>
									From trivia to anxiety, </br>
									Every challenge, big or small, </br>
									Every squeal, step and fall, </br>
									The right advice, the right call. </br>
									Holding your baby's hand always. </br>
									The Pediatric Network. 24X7. </strong></span>
									<img class="classImgEndQuote"src="images/req/end.png"/>
								</p>	
								<p style="width:950px;">
									<span class="classSpanStartQuoteText"><strong>The Pediatric Network</strong></span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leverage technology to deliver integrated, reliable and cost-effective health services to a child and peace of minds to the parents.
								</p>														
							</div>-->

			  		<?php include('addSlider.php'); ?>
			  		<div>

				  		<div class="classDivDirectoryOnBanner">				  			
				  			<form name="search_form" method="POST" action="pediatric_search_result.php"  onsubmit="return validate_type()">
				  				<span class="classFormFieldSpan classSpanMDText">
				  					<strong>DIRECTORY</strong>
				  				</span>
				  				<span class="classFormFieldSpan">
					  				<select class="form-control classFormSelectBox" style="width:135px!important;" name="stype">
					  					<option value="all">FOR</option>
					  					<option value="Pediatrician">Pediatrician</option>
					  					<option value="Pharmacist">Pharmacist</option>				  					
					  				</select>					  			
				  				</span>
				  				<span class="classFormFieldSpan">
				  				
									<input type="text" class="form-control classFormFieldsCustom" name="location"  style="width: 400px;margin-right: -10px;" placeholder="SEARCH BY: LOCATION, NAME ( Mumbai, Dr. Shah )"/>
								
								</span>
				  				<span class="classFormFieldSpan">
				  					<input type="submit" value="SEARCH" class="classSearchButton"/>
				  				</span>	
				  			</form>
				  		</div>
			  			<!--<img src="images/1.png" alt="" height="350px" width="100%"/>    -->
					</div>
				</div>	  

			</div>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			          
			            <div class="classDivTabContainCustomHome" id="Tab1Data">

			              <div class="classDivHeading">
			              	PARENTS
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>Online pediatric backup to your child 24x7</li>
				              			<li>Electronic health records</li>
				              			<li>Disease and growth surveillance</li>
				              			<li>Anticipatory guidance</li>
				              			<li>Online connectivity to your pediatrician</li>
				              			<li>E-prescription to your local pharmacy</li>
				              			<li>Discounts from our network pharmacy</li>
				              			<li>Less doctor visits & more savings</li>
				              		</ul>
				              	</div>				              	
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">
			              			<img class="classImgPDP" src="./images/parents.png"/>
			              		</div>	
			              	</div>
			              	<div class="classButtonWrapper" style="margin-top: -100px;">
			              		<form name="ParentsForm" method="POST" action="http://app.thepediatricnetwork.com/doPhrLogin.php">
				              		<div class="classDivForInputBox">				              		
					              		
						           			<input type="text" class="classSpanUPButton classUPTextbox" name="uname" placeholder="USERNAME">
						           			<input type="password" class="classSpanUPButton classUPTextbox" name="password" placeholder="PASSWORD">
						             
					              	</div>
					              	<div class="classDivForButtons" style="clear:both;"></div>

					              	<div class="classDivForButtons">
					              		<input type="submit" name="login" class="classSpanSignInButton" value="SIGN IN">
						              	<span class="classSpanOr">OR</span>
							            <span class="classSpanCreateUser"><a href="patient-sign-up.php">CREATE NEW ACCOUNT</a></span>						              	
						            </div> 
					              	<div class="classDivForButtons">
					              		<span class="classSpanDownloadBrochure">
					              			<a class="" href="img/brochure/patient_brochure_1.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
					              				CLICK TO VIEW BROCHURE					              			</a>
					              			<a class="example-image-link" href="img/brochure/patient_brochure_2.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."></a>
											<a class="example-image-link" href="img/brochure/patient_brochure_3.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."></a>
											<a class="example-image-link" href="img/brochure/patient_brochure_4.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."></a>
		              			
					              		</span>
				              		</div>
			              		</form>
			              	</div>

			              </div>
<div class="classDividerDiv"></div>
						</div>

			            <div class="classDivTabContainCustomHome" id="Tab2Data" >

			              <div class="classDivHeading">
			              	DOCTOR
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>Conversion of your clinic to paperless clinic.</li>
				              			<li>Increased patient traffic</li>
				              			<li>Low-cost access to best-in-class patient	management systems</li>
				              			<li>Microsite for your profile</li>
				              			<li>Electronic health records and standardized	medical solutions for your patients</li>
				              			<li>Knowledge updates and your link to newer developments</li>
				              			<li>Partial outsourcing of semi-emergency calls</li>
				              			<li>Online scheduling of appointments</li>
				              			<li>Increased networking opportunities with other pediatricians</li>
				              			<li>Better integration with the pharmacies</li>
				              		</ul>
				              	</div>				              	
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">

			              			<img class="classImgPDP" src="./images/doctor.png"/>
			              		</div>

			              	</div>
			              	<div class="classButtonWrapper">
			              		<form name="DoctorForm" method="POST" action="http://app.thepediatricnetwork.com/doLogin.php">
					              	<div class="classDivForInputBox">
					           			<input type="text" class="classSpanUPButton classUPTextbox" name="uname" placeholder="USERNAME">
					           			<input type="password" class="classSpanUPButton classUPTextbox"  name="password" placeholder="PASSWORD">				              		
					              	</div>
					              	<div class="classDivForButtons" style="clear:both;"></div>

					              	<div class="classDivForButtons">
					              		
					              		<input type="submit" name="login" class="classSpanSignInButton" value="SIGN IN" >
				              			
				              			<span class="classSpanOr">OR</span>
					              		<span class="classSpanCreateUser"><a href="doctor-sign-up.php">CREATE NEW ACCOUNT</a></span>			         
						            </div> 
					              	<div class="classDivForButtons">
					              		<span class="classSpanDownloadBrochure">
					              			
					              				CLICK TO VIEW BROCHURE
					              			</a>
					              		</span>					              		
					              	</div>
				            	</form>			            

			              </div>

						</div>
					<div class="classDividerDiv"></div>
					</div>


			            <div class="classDivTabContainCustomHome"  id="Tab3Data" >

			              <div class="classDivHeading">
			              	PHARMACIST
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>More prescriptions</li>
				              			<li>Loyal clientele</li>
				              			<li>More discounts from companies</li>
				              			<li>Online presence</li>
				              			<li>Low cost advertising</li>
				              			<li>Latest drug information & trends</li>
				              			<li>Monthly business reviews & performance optimization</li>
				              			<li>AT NO ADDITIONAL COST</li>
				              		</ul>
				              	</div>			              		
				              	
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">
			              			<img class="classImgPDP" src="./images/Pharmacist.png"/>
			              		</div>		              					              		

			              	</div>
			              	<div class="classButtonWrapper" style="margin-top: -100px;">
			              		<form name="PharmacistForm" method="POST" action="http://app.thepediatricnetwork.com/doPharmacyLogin.php">
			              			<div class="classDivForInputBox">
			              				<input type="text" class="classSpanUPButton classUPTextbox" name="uname" placeholder="USERNAME">
				           				<input type="password" class="classSpanUPButton classUPTextbox"  name="password" placeholder="PASSWORD">
				              		</div>
				              		<div class="classDivForButtons" style="clear:both;"></div>

					              	<div class="classDivForButtons">
					              		<input type="submit" name="login" class="classSpanSignInButton" value="SIGN IN"/>				              		
				              						         
				              			<span class="classSpanOr">OR</span>
					              		<span class="classSpanCreateUser"><a href="pharmacist-sign-up.php">CREATE NEW ACCOUNT</a></span>
				            		</div>

				            	<div class="classDivForButtons">
			              			<span class="classSpanDownloadBrochure">
			              				<a class="" href="img/brochure/pharma_brochure_1.jpg" data-lightbox="example-pharma-set" data-title="Click the right half of the image to move forward.">
					              				CLICK TO VIEW BROCHURE					              			</a>
					              			<a class="example-image-link" href="img/brochure/pharma_brochure_2.jpg" data-lightbox="example-pharma-set" data-title="Click anywhere outside the image or the X to the right to close."></a>
		              			
			              			</span>			              		</div>
				            </form>

			              </div>

						</div>
<div class="classDividerDiv"></div>
				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




