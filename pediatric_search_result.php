<html>
	<head>
		<title>Pediatric - Search Result</title>		
	<?php include('header.php'); 
 
?>

<style>

/*
#idActiveMenu1{
	color: #004480 !important;
  border-bottom-color:#004480 !important;
}
#idActiveMenu1 a{
color: #004480 !important;
  border-bottom-color:#004480 !important;
}*/
</style><!--Style for the active Link-->
<link href="jss/style.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="js/jquery.pages.js"></script>
<!--Paging Script-->
<script type="text/javascript">
$(document).ready(function() {
	$("div.holder").jPages({
	    containerID : "content",
	    perPage: 10
  	});
});
</script>
<script type="text/javascript">

function validate_type(){

	if( document.search_form.stype.value == "all" )
	   {
    	 alert( "Please Select For - Hospital | Clinic | Pharmacist" );
     	return false;
	   }
   	return( true );
}
</script>
<!--Script to display Map-->
<?php include('maps_script.php');?>




	</head>

  <!--<body onload="displayMap()">-->
  	<body onload="showMap()">
  
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
					
					<?php include('body-header.php'); ?>



			  		<?php //include('addSlider.php'); ?>
			  		<div>

				  		<div class="classDivDirectoryWithoutBanner">				  			
				  			<form name="search_form" id="idSearchForm" method="POST" action="pediatric_search_result.php" onsubmit="return validate_type()">
				  				<span class="classFormFieldSpan classSpanMDText">
				  					DIRECTORY
				  				</span>
				  				<span class="classFormFieldSpan">
					  				<select class="form-control classFormSelectBox" name="stype" style="width:135px!important;">
					  					<option value="all">FOR</option>
					  					<option value="Pediatrician">Pediatrician</option>
					  					<option value="Pharmacist">Pharmacist</option>				  					
					  				</select>				  				

					  			
				  				</span>
				  				<span class="classFormFieldSpan">
				  					
									<input type="text" class="form-control classFormFieldsCustom" name="location"  style="width: 400px;margin-right: -10px;" placeholder="SEARCH BY: LOCATION, NAME ( Mumbai, Dr. Shah )"/>
								
								</span>
				  				<span class="classFormFieldSpan">
				  					<input type="submit" value="SEARCH" class="classSearchButton"/>
				  				</span>	
				  			</form>
				  		</div>
			  			<!--<img src="images/1.png" alt="" height="350px" width="100%"/>    -->
					</div>
					  

			</div>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           <!--
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom" id="Tab1Data" >
			            	
			            	<div id="idSearchResultOutput">

								
				            		<?php echo display_search_result(null,null); ?>
				            	
					        </div> <!-- Search Output Here -->

						</div>          
			            
					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>


			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




