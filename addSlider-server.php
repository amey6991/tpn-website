<div id="slider_holder" class="sixteen columns">
				<div id="sequence">
					<div class="prev" ><span></span></div>
					<div class="next" ><span></span></div>
	
					<ul style="list-style:none!important;">
						<!-- Layer 1 -->
						<li>
							<div class="info">
								<p class="">
									<img class="classImgStartQuote" src="images/req/start.png"/>
									<span class="classSpanStartQuoteText"><strong>From your baby's first cry till puberty, </br>
									From trivia to anxiety, </br>
									Every challenge, big or small, </br>
									Every squeal, step and fall, </br>
									The right advice, the right call. </br>
									Holding your baby's hand always. </br>
									The Pediatric Network. 24X7. </strong></span>
									<img class="classImgEndQuote"src="images/req/end.png"/>
								</p>	
								<p style="width:950px;">
									<span class="classSpanStartQuoteText"><strong>The Pediatric Network</strong></span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leaverage technology to deliver integrated, reliable and cost-effective health services to a child, and peace of minds to the parents.
								</p>														
							</div>
							<img class="slider_bgr" src="images/req/banner3.png"/>
						</li>						
						
						<!-- Layer 2 -->
						<!--
						<li>
							<div class="info">
								<p class="">
									<img class="classImgStartQuote" src="images/req/start.png"/>
									<span class="classSpanStartQuoteText">From your baby's first cry till puberty, </br>
									From trivia to anxiety, </br>
									Every challenge, big or small, </br>
									Every squeal, step and fall, </br>
									The right advice, the right call. </br>
									Holding your baby's hand always. </br>
									The Pediatric Network. 24X7. </span></br>
									<img class="classImgEndQuote"src="images/req/end.png"/>
								</p>	
								<p style="width:1000px;">
									<span class="classSpanStartQuoteText">The Pediatric Network</span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leaverage technology to deliver integrated, reliable and cost-effective health services to a child, and peace of minds to the parents.
								</p>
						-->
								<!--
								<h2>Pediatric</h2>								
								<p>
									In order to carry out various functions you are using important body parts like bones, muscles, and joint now more about different problema associated with it.
								</p>
								<h3><a class="link" href="#">Solutions &raquo;</a></h3>
							-->
						<!--
							</div>
							<img class="slider_bgr" src="images/req/banner1.png"/>
							<!--<img class="slider_img" src="images/2_2.png"/>
						</li>
						-->

						<!--
						<li>
							<div class="info animate-in">
								<h2>Pediatric</h2>								
								<p>
									In order to carry out various functions you are using important body parts like bones, muscles, and joint now more about different problema associated with it.
								</p>
								<h3><a class="link" href="#">Solutions &raquo;</a></h3>
							</div>
							<img class="slider_bgr animate-in" src="images/req/banner2.png"/>
							<img class="slider_img animate-in" src="images/1_1.png"/>-->
						<!--</li>
						 Layer 3 -->		
									
					</ul>
				</div>
			</div>
			<!-- Sequence Slider::END-->

			<script type="text/javascript">	
			var preloader = ($.browser.msie) ? false : true ;
				
			$(document).ready(function(){
				var options = {
					autoPlay: true,
					autoPlayDelay: 10000,
					nextButton: false,
					prevButton: false,
					preloader: preloader,
					animateStartingFrameIn: false,
					transitionThreshold: 500,
					fallback: {
			        	theme: "slide",
			        	speed: 5000
			        }
				};
				
				var sequence = $("#sequence").sequence(options).data("sequence");
	
				sequence.afterLoaded = function(){
					//$(".info").css('display','block'); // To display Next and Previous Buttons
					$(".next").css('display','none');
					$(".prev").css('display','none');
					$(".info").css('display','block');
					$("#sequence").hover(
					        function() {
					        	$(".prev, .next").stop().animate({opacity:0.7},300);	            
					        },
					        function() {        
					        	$(".prev, .next").stop().animate({opacity:0},300);
					        }
					);
					
					$(".prev, .next").hover(
					        function() {
					        	$(this).stop().animate({opacity:1},200);	            
					        },
					        function() {        
					        	$(this).stop().animate({opacity:0.7},200);
					        }		
					);
				}
			})
		</script>

	