<html>
	<head>
		<title>Pediatric - Privacy Policy</title>		
	<?php include('header.php'); ?> 

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Privacy Policy
			              </div>
			              	<div class="classAboutUsText">	

			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Privacy Policy</span><br/><br/>
			              			This Privacy Policy sets forth the information gathering, use, and dissemination practices of ThePediatricNetwork Health Solutions Private Limited (“ThePediatricNetwork”) in connection with the web site located at www.thepediatricnetwork.com (the “Site”). This Privacy Policy addresses ThePediatricNetwork’s practices regarding information collected only directly through or from the Site – it does not address or govern any information gathering, use, or dissemination practices related to information collected other than directly through or from the Site, including, without limitation, from or via telephone, postal mail, personal delivery, or other offline means or media. ThePediatricNetwork can be contacted by phone at (+91) 022 29253707 or at support@thepediatricnetwork.com or through the “Contact Us” section of the web site at www.thepediatricnetwork.com
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Collection of Personal Information from Site Visitors</span><br/><br/>
				              		ThePediatricNetwork may collect or track >1) the email addresses of visitors that communicate with ThePediatricNetwork via e-mail, and 2) information knowingly provided by the visitor in online forms, registration forms, surveys, e-mail, and other online avenues (including, without limitation, demographic and personal profile data). ThePediatricNetwork also may collect or track 3) the home server domain names, e-mail addresses, type of client computer, files downloaded, search engine used, operating system, and type of web browser of visitors to the Site, and 4) aggregate and user-specific information regarding which pages Site visitors access.<br/><br/>
				              		ThePediatricNetwork may place Internet “cookies” on visitors’ hard drives. Internet cookies save data about individual visitors, such as the visitor’s name, password, user-name, screen preferences, and the pages of a site viewed by the visitor. When the visitor revisits the Site, ThePediatricNetwork may recognize the visitor by the Internet cookie and customize the visitor’s experience accordingly. Visitors may decline Internet cookies, if any, by using the appropriate feature of their web client software, if available. <br/><br/>
				              		ThePediatricNetwork also may use web bugs to gather, store, and track certain information related to your visit to and activity on the Site. A web “bug” is a file object, which can be a graphic image such as a transparent one-pixel-by-one-pixel graphics file, which is placed on a web page or in an email message to monitor user activity. A web bug can gather, for example, the IP address of your computer, the time the web page was viewed, and the type of browser used.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Use of Personal Data Collected</span><br/><br/>
			              			Personal data collected by ThePediatricNetwork may be used by ThePediatricNetwork for many reasons, for example, for editorial and feedback purposes, for marketing and promotional purposes, for a statistical analysis of users’ behaviour, for product development, for content improvement, for fulfilment of a requested transaction or record keeping, or to customize the content and layout of the Site. Aggregate visitor data may be used for internal purposes, and individually identifying information, such as names, postal and email addresses, phone numbers, and other personal information which visitors voluntarily provide to ThePediatricNetwork may be added to ThePediatricNetwork’s databases and used for future calls, mailings, and other communications regarding Site updates, new products and services, and upcoming events.<br/><br/>
			              			ThePediatricNetwork also may use Site visitor data to contact the Site visitors regarding account status and changes to the subscriber agreement, Privacy Policy, and any other policies or agreements relevant to Site visitors.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Disclosure of Personal Data to Third Parties</span><br/><br/>
			              			Aggregate visitor data may be provided to marketing firms and other organizations providing marketing and/or marketing support services to ThePediatricNetwork.<br/><br/>
			              			Identifiable or non-identifiable data may be disclosed or distributed 1) to another party with which ThePediatricNetwork enters or reasonably may enter into a corporate transaction, such as, for example, a merger, consolidation, acquisition, or asset purchase, or 2) to a third party (a) pursuant to a subpoena, court order, or other form of legal process, (b) in response to a request by or on behalf of any local, state, or other government / regulatory agency, department, or body, whether or not pursuant to a subpoena, court order, or other form of legal process, or (c) if determined by ThePediatricNetwork in its sole judgment that such disclosure or distribution is appropriate to protect the life, health, or property of ThePediatricNetwork or any other person or entity. Identifiable and non-identifiable visitor information and data which visitors provide to ThePediatricNetwork also may be provided to third parties that provide products, information, or services which ThePediatricNetwork believes that visitors may be interested in directly or indirectly purchasing or obtaining.<br/><br/>
			              			While ThePediatricNetwork may undertake efforts to see that any third party to or with which ThePediatricNetwork shares, rents, sells, or otherwise discloses personal data is under a contractual obligation to use the personal data solely for the purposes for which the data was disclosed, ThePediatricNetwork is not responsible for their conduct, actions, omissions, or information handling or dissemination practices and ThePediatricNetwork will not be liable therefore.<br/><br/>
			              			This Site may contain links to other web sites. ThePediatricNetwork is not responsible for the privacy practices of such web sites, advertisers, or third parties, or for the content of such sites or advertisements. It is possible that these links, themselves, may be used by third parties or others to collect personal or other information about Site visitors. It is solely the visitors’ obligation to review and understand the privacy practices and policies of these other web sites and of these advertisers and third parties.<br/><br/>

				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Security Measures</span><br/><br/>
			              			ThePediatricNetwork has implemented numerous security features designed to prevent the unauthorized release of or access to personal information. Please be advised, however, that the confidentiality of any communication or material transmitted to or from ThePediatricNetwork via this Site or email cannot be guaranteed. Accordingly, ThePediatricNetwork is not responsible for the security of information transmitted via the Internet. Instead of communicating with ThePediatricNetwork via email or the Internet, visitors can contact ThePediatricNetwork by phone number set forth at the beginning of this Statement.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Opt-Out Right</span><br/><br/>
			              			Except as necessary for ThePediatricNetwork to provide the services, information, or products requested by a visitor, the visitor may opt out of having his or her personally identifiable information, which has been voluntarily provided to ThePediatricNetwork through an online form, registration form, survey, or other similar mechanism, prospectively retained by ThePediatricNetwork, used by ThePediatricNetwork for secondary purposes, or disclosed by ThePediatricNetwork to third parties by contacting ThePediatricNetwork via telephone at the number set out above. Once ThePediatricNetwork has implemented the visitor’s request, ThePediatricNetwork will prospectively cease retaining, using for secondary purposes, or disclosing to third parties the personally identifiable information identified by the visitor in his or her request, to the extent directed in the request. (This does not apply to collection of the type of web browser of the visitor to the Site or to information provided or collected that does not meet the above description.)
			              		</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Right to Change Privacy Policy</span><br/><br/>
			              			ThePediatricNetwork may change this Privacy Policy at any time. Notice of any new or revised Privacy Policy, as well as the location of the new or revised statement, will be posted on the Site for at least 60 days after the change. It is the obligation of users visiting the Site before the change to learn of changes to the Privacy Policy since their last visit. Any change to this Privacy Policy shall be effective as to any visitor who has visited the Site before the change was made.
			              		</p><br/><br/>		              	
							</div>
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




