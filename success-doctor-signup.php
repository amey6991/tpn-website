<html>
	<head>
		<title>Pediatric - Doctor Sign Up Complete</title>		
	<?php include('header.php'); ?> 

	</head>
<?php


$sFields =null;

if(isset($_GET['msg'])){ // Pass id for retrieving data before payment.
	
	$sMsg=$_GET['msg'];
	//var_dump($sIdBeforePayment);
	if($sMsg=='E4'){
		$sFields="Sorry you could not complete the sign up process. Please <a href='doctor-sign-up.php' >try again</a> or contact support at support@thepediatricnetwork.com";

	}else{		
		$sFields='<strong>Thank You! </strong> <br/>For signing up with the Pediatric Network. <br/>Your Username and Password with basic instructions have been emailed to the official email id provided. <br/>We hope you have a fruitful and happy year with us.';
	}

	
}else{
	$aFields=empty($aFields);
	
	$sFields="Sorry you could not complete the sign up process. Please <a href='doctor-sign-up.php' >try again</a> or contact support at support@thepediatricnetwork.com";
}

?>
	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			        

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >
			              <div class="classDivHeading">
			              	Welcome
			              </div>	
			              <div class="classDivTextWrapper">			              		
			              		<p class="classPLeaderText classPMsgWrapper" style="color:#222 !important;">
				              		<?php
				              			echo $sFields;
				              		?>
				              	</p>
				              	
							</div>							
							

							<div class="classDivTextWrapper" style="width:80%;">			              		
			              		<h3 style="color:#F80;">The features you can access once you login in are:</h3>

		              			<ul class="classUlFeatureText">
									<li>Appointment Management</li>
									<li>See Patient History</li>
									<li>Storing Patient Medical Records</li>
									<li>Uploading reports and growth milestones at one single location.</li>
									<li>Access to the complete history of your patients health journey</li>
									<li>Managing Billing and Daily Register</li>
									<li>Access to our pharmacy network</li>
								</ul>
								
		              		</div>	
		              		
							</div>							             

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




