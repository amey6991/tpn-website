<html>
	<head>
		<title>Pediatric - Terms & Condition</title>		
	<?php include('header.php'); ?> 

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Terms and Condition
			              </div>
			              	<div class="classAboutUsText">	

			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">ThePediatricNetwork Terms of Use</span><br/><br/>
			              			This Privacy Policy sets forth the information gathering, use, and dissemination practices of ThePediatricNetwork Health Solutions Private Limited (“ThePediatricNetwork”) in connection with the web site located at www.thepediatricnetwork.com (the “Site”). This Privacy Policy addresses ThePediatricNetwork’s practices regarding information collected only directly through or from the Site – it does not address or govern any information gathering, use, or dissemination practices related to information collected other than directly through or from the Site, including, without limitation, from or via telephone, postal mail, personal delivery, or other offline means or media. ThePediatricNetwork can be contacted by phone at (+91) 022 29253707 or at support@thepediatricnetwork.com or through the “Contact Us” section of the web site at www.thepediatricnetwork.com
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">1. Information Disclaimer</span><br/><br/>
				              		The information on this Site is provided by Us. While We attempt to keep the information as accurate as possible, We disclaim any implied warranty or representation about its accuracy or completeness, or appropriateness for a particular purpose. You assume full responsibility for using the information at this Site, and You understand and agree that We are neither responsible nor liable for any claim, loss or damage resulting from its use. The mention of specific products or services at this Site does not constitute or imply a recommendation or endorsement by Us, unless it is explicitly stated. Information on the Site may be changed or updated without notice. The views and opinions expressed in the media, articles or comments on this Site by users and/or Our staff are those of the speakers or authors and do not necessarily reflect Our views and opinions.<br/><br/>	
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">2. Limited Warranty</span><br/><br/>
			              			THIS SITE, INCLUDING ANY CONTENT OR INFORMATION CONTAINED HEREIN OR ANY SITE-RELATED SERVICE, IS PROVIDED “AS IS” WITH NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NONINFRINGEMENT. YOU ASSUME TOTAL RESPONSIBILITY AND RISK FOR YOUR USE OF THIS SITE, SITE-RELATED SERVICES, AND ANY HYPERLINKED WEBSITES. Some jurisdictions do not allow the exclusion of implied warranties, so the above exclusion may not apply to You.<br/><br/>
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">3. Links to Third Party Sites</span><br/><br/>
			              			Some hyperlinks in the Site may take You outside of the Site or link to articles and videos (“Linked Sites”). The Linked Sites are not under Our control and We are not responsible for the content of any such Linked Sites, any hyperlinks contained in a Linked Site, or any changes or updates to such Linked Sites. We provide these hyperlinks to Linked Sites to You only as a convenience and the inclusion of any hyperlink or content in that Linked Site does not imply endorsement by Us of the Linked Site or the contents contained therein.<br/><br/>
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">4. Intellectual Property Rights</span><br/><br/>
			              			All right, title, and interest in and to the intellectual property rights embodied in the Site and any documentation downloaded or printed from the Site is owned or licensed by Us.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">5. Liability for the Actions of Users of the Website</span><br/><br/>
			              			You must not use Our Site in any way which is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity. We accept no responsibility for any loss or harm incurred.<br/><br/>
			              		</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">6. Limiting Liability for Viruses, Damage, and Availability</span><br/><br/>
			              			We do not warrant that functions available on this Site will be uninterrupted or error free, that defects will be corrected, or that the server that makes it available is free of viruses or bugs. You acknowledge that it is Your responsibility to implement sufficient procedures and virus checks (including anti-virus and other security checks) to satisfy Your particular requirements for the accuracy of data input and output.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">7. Legal Jurisdiction</span><br/><br/>
			              			You consent to resolve any dispute within the jurisdiction of Mumbai, India, that you may have with us, our affiliates, our partners, or the Site.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">8. Limitation of Liability</span><br/><br/>
			              			TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL WE OR CONTRIBUTORS TO THE SITE BE LIABLE FOR ANY SPECIAL, INCIDENTAL, AND DIRECT OR CONSEQUENTIAL DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR OTHER PECUNIARY LOSS ARISING OUT OF THE USE OF OR INABILITY TO USE THE SITE ARISING AS A RESULT OF OUR PROVISION OR FAILURE TO PROVIDE INFORMATION VIA THE SITE EVEN IN THE EVENT THAT WE HAVE BEEN ADVISED OF SUCH DAMAGES.
			              		</p><br/><br/>		              	
							</div>
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




