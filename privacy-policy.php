<html>
	<head>
		<title>Pediatric</title>		
	<?php include('header.php'); ?> 

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Privacy Policy
			              </div>
			              	<div class="classAboutUsText">	

			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Collection and Use of Personal Information</span><br/><br/>
			              			Personal information is data that can be used to identify or contact a single person.<br/>
You may be asked to provide your personal information anytime you are in contact with ThePediatricNetwork or ThePediatricNetwork affiliated company. ThePediatricNetwork and its affiliates may share this personal information with each other and use it consistent with this Privacy Policy. They may also combine it with other information to provide and improve our products, services, content, and advertising. You are not required to provide the personal information that we have requested, but, if you chose not to do so, in many cases we will not be able to provide you with our products or services or respond to any queries you may have.<br/>
Here are some examples of the types of personal information ThePediatricNetwork may collect and how we may use it.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">What personal information we collect</span><br/><br/>
			              				- When you create an ThePediatricNetwork ID, apply for credit, purchase a product or service, contact us or participate in an online survey, we may collect a variety of information, including your name, mailing address, phone number, email address, contact preferences, and credit card information.<br/>
	- We may ask for a government issued ID in limited circumstances including when setting up an account, for the purpose of extending commercial credit, managing information, or as required by law.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">How we use your personal information</span><br/><br/>
			              			- The personal information we collect allows us to keep you posted on ThePediatricNetwork’s latest announcements, updates, and upcoming events.<br/>
- We also use personal information to help us create, develop, operate, deliver, and improve our products, services, content and advertising, and for loss prevention and anti-fraud purposes.<br/>
- We may use your personal information, including date of birth, to verify identity, assist with identification of users, and to determine appropriate services. For example, we may use date of birth to determine the age of ThePediatricNetwork ID account holders.<br/>
- From time to time, we may use your personal information to send important notices, such as communications about purchases and changes to our terms, conditions, and policies. Because this information is important to your interaction with ThePediatricNetwork, you may not opt out of receiving these communications.<br/>
- We may also use personal information for internal purposes such as auditing, data analysis, and research to improve ThePediatricNetwork’s products, services, and customer communications.<br/>
- If you enter into a sweepstake, contest, or similar promotion we may use the information you provide to administer those programs.<br/>
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Collection and Use of Non-Personal Information</span><br/><br/>
			              			We also collect data in a form that does not, on its own, permit direct association with any specific individual. We may collect, use, transfer, and disclose non-personal information for any purpose. The following are some examples of non-personal information that we collect and how we may use it:<br/>
- We may collect information such as occupation, language, zip code, area code, unique device identifier, referrer URL, location, and the time zone where an ThePediatricNetwork product is used so that we can better understand customer behavior and improve our products, services, and advertising.<br/>
- We may collect information regarding customer activities on our website. This information is aggregated and used to help us provide more useful information to our customers and to understand which parts of our website, products, and services are of most interest. Aggregated data is considered non-personal information for the purposes of this Privacy Policy.<br/>
- We may collect and store details of how you use our services, including search queries. This information may be used to improve the relevancy of results provided by our services. Except in limited instances to ensure quality of our services over the Internet, such information will not be associated with your IP address.<br/>
If we do combine non-personal information with personal information the combined information will be treated as personal information for as long as it remains combined.
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Cookies and Other Technologies</span><br/><br/>
			              			ThePediatricNetwork’s websites, online services, interactive applications, email messages, and advertisements may use “cookies” and other technologies such as pixel tags and web beacons. These technologies help us better understand user behavior, tell us which parts of our websites people have visited, and facilitate and measure the effectiveness of advertisements and web searches. We treat information collected by cookies and other technologies as non-personal information. However, to the extent that Internet Protocol (IP) addresses or similar identifiers are considered personal information by local law, we also treat these identifiers as personal information. Similarly, to the extent that non-personal information is combined with personal information, we treat the combined information as personal information for the purposes of this Privacy Policy.<br/>
			              			ThePediatricNetwork and our partners also use cookies and other technologies to remember personal information when you use our website. Our goal in these cases is to make your experience with ThePediatricNetwork more convenient and personal. For example, knowing your first name lets us welcome you the next time you visit the ThePediatricNetwork. As is true of most internet services, we gather some information automatically and store it in log files. This information includes Internet Protocol (IP) addresses, browser type and language, Internet service provider (ISP), referring and exit websites and applications, operating system, date/time stamp, and clickstream data.<br/>
			              			We use this information to understand and analyze trends, to administer the site, to learn about user behavior on the site, to improve our product and services, and to gather demographic information about our user base as a whole. ThePediatricNetwork may use this information in our marketing and advertising services.<br/>
			              			In some of our email messages, we use a “click-through URL” linked to content on the ThePediatricNetwork website. When customers click one of these URLs, they pass through a separate web server before arriving at the destination page on our website. We track this click-through data to help us determine interest in particular topics and measure the effectiveness of our customer communications. If you prefer not to be tracked in this way, you should not click text or graphic links in the email messages.<br/>
			              			Pixel tags enable us to send email messages in a format customers can read, and they tell us whether mail has been opened. We may use this information to reduce or eliminate messages sent to customers.<br/>
				              	</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Disclosure to Third Parties</span><br/><br/>
			              			ThePediatricNetwork shares personal information with companies who provide services such as information processing, extending credit, fulfilling customer orders, delivering products to you, managing and enhancing customer data, providing customer service, assessing your interest in our products and services, and conducting customer research or satisfaction surveys. These companies are obligated to protect your information and may be located wherever ThePediatricNetwork operates.
			              		</p><br/><br/>
				              	<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Others</span><br/><br/>
			              			It may be necessary − by law, legal process, litigation, and/or requests from public and governmental authorities − for ThePediatricNetwork to disclose your personal information. We may also disclose information about you if we determine that for purposes of national security, law enforcement, or other issues of public importance, disclosure is necessary or appropriate.<br/>
			              			We may also disclose information about you if we determine that disclosure is reasonably necessary to enforce our terms and conditions or protect our operations or users. Additionally, in the event of a reorganization, merger, or sale we may transfer any and all personal information we collect to the relevant third party.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Protection of Personal Information</span><br/><br/>			              			
			              			ThePediatricNetwork takes the security of your personal information very seriously. When you post on ThePediatricNetwork forum, chat room, or social networking service, the personal information and content you share is visible to other users and can be read, collected, or used by them. You are responsible for the personal information you choose to share or submit in these instances. For example, if you list your name and email address in a forum posting, that information is public. Please take care when using these features.
			              		</p>
			              		<br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Integrity and Retention of Personal Information</span><br/><br/>			              			
			              			ThePediatricNetwork makes it easy for you to keep your personal information accurate, complete, and up to date. We will retain your personal information for the period necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Location-Based Services</span><br/><br/>			              			
			              			To provide location-based services, ThePediatricNetwork and our partners and licensees may collect, use, and share precise location data, including the real-time geographic location of your ThePediatricNetwork computer or device. Where available, location-based services may use GPS, Bluetooth, and your IP Address, along with crowd-sourced Wi-Fi hotspot and cell tower locations, and other technologies to determine your devices’ approximate location. Unless you provide consent, this location data is collected anonymously in a form that does not personally identify you and is used by ThePediatricNetwork and our partners and licensees to provide and improve location-based products and services. For example, your device may share its geographic location with application providers when you opt in to their location services.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Our Companywide Commitment to Your Privacy</span><br/><br/>			              			
			              			To make sure your personal information is secure, we communicate our privacy and security guidelines to ThePediatricNetwork employees and strictly enforce privacy safeguards within the company.
			              		</p><br/><br/>		              	
			              		<p class="classPLeaderText classJustifyText">
			              			<span class="classBlueColorSubHeading">Privacy Questions</span><br/><br/>			              			
			              			If you have any questions or concerns about ThePediatricNetwork’s Privacy Policy or data processing or if you would like to make a complaint about a possible breach of local privacy laws, please contact us. All such communications are examined and replies issued where appropriate as soon as possible. If you are unsatisfied with the reply received, you may refer your complaint to the relevant regulator in your jurisdiction. If you ask us, we will endeavor to provide you with information about relevant complaint avenues which may be applicable to your circumstances.<br/>
			              			ThePediatricNetwork may update its Privacy Policy from time to time. When we change the policy in a material way, a notice will be posted on our website along with the updated Privacy Policy.<br/>
ThePediatricNetwork.
			              		</p><br/><br/>	             	
							</div>
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




