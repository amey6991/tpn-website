<html>
	<head>
		<title>Pediatric</title>		
	<?php include('header.php'); ?> 

<style>
#idActiveMenu21{
	color: #004480 !important;
}
#idActiveMenu21 a{
color: #004480 !important;
}
</style><!--Style for the active Link-->
	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin" >

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           <!-- 
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			           
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Leadership Team
			              </div>	

			              <div class="classDivInnerPageOfABContainer">			              	
			              	

			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/atul.jpg" />				              		
				              	</div><!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">				              		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Atul Laddad </span><br/><span class="classSpanLeaderDesig">Advisor</span></p>
				              		<p class="classPLeaderText"><b>Atul </b>has 10+ years of experience in advising, building and deploying solutions to enable companies develop and broaden their partner networks. <br/>

											In his most recent role, Atul led development of <a href="http://www.fuelnownet.com" target="_blank" >www.fuelnownet.com</a>, a platform for a network of refined-products carriers to assist a client broaden its delivery footprint across the United States. Prior, Atul worked with Deloitte, a top-tier management consulting firm where he helped several Fortune 500 companies optimize their supply chains. <br/>

											Atul holds an MBA from the University of Michigan and a Masters in Industrial Engineering from Penn State University. 
				              		</p>
				              	</div><!-- Left div contain complete text-->
			              	</div><!-- Div for one individual leader -->

			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/AdityaPatkar.jpg" />				              		
				              	</div><!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Aditya Patkar</span><br/><span class="classSpanLeaderDesig">Marketing & Sales Director, Plus 91 Technologies</span></p>
				              		<p class="classPLeaderText">
				              			<b>Aditya </b> is a co-founder of Plus 91, an IT company engaged in designing medical software products and the IT backbone for this project.
				              		</p>
				              		<p class="classPLeaderText">
				              		 	He has done his Masters from NMIMS, Mumbai and is an electronics engineer.
				              		</p>
				              	</div><!-- Left div contain complete text-->
			              	</div><!-- Div for one individual leader -->

			              	

			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/virendra.jpg"/>				              		
				              	</div><!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Nilesh Parekh</span><br/><span class="classSpanLeaderDesig">Director</span></p>
				              		<p class="classPLeaderText">
				              			<strong>Nilesh </strong>is a financial advisor and portfolio manager with  LPK Securities in Mumbai and has 11 years of experience in India's equity markets and investments.At TPN, Nilesh has been instrumental in developing and expanding the pharmacy network. He has a Bachelor of Commerce from Mumbai university. 
				              		</p>
				              	</div><!-- Left div contain complete text-->
			              	</div><!-- Div for one individual leader -->

			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/nrip.jpg" />				              		
				              	</div><!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Nrip Nihalani</span><br/><span class="classSpanLeaderDesig">Managing Director, Plus 91 Technologies</span></p>
				              		<p class="classPLeaderText">
				              			<b>Nrip </b> is a Health Care Technology Evangelist.
				              		</p>
				              		<p class="classPLeaderText">
				              		 	An advocate of using technology to improve Health care, he is a speaker and educator on various topics including Signal Processing, Mobility, Information Therapy, Internet and Mobile use in Healthcare, Electronic Medical Records, Big Data, Healthcare Data Mining and Patient Record Digitization.
				              		</p>
				              	</div><!-- Left div contain complete text-->
			              	</div><!-- Div for one individual leader -->

			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leader-icon.png" />				              		
				              	</div> <!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Faridh</span><br/><span class="classSpanLeaderDesig">Director</span></p>
				              		<p class="classPLeaderText">A post graduate in Management with 20 years of experience in Strategy, Branding, Marketing and Sales. An innovative strategic thinker, Faridh's last assignment was Spear heading Strategy, Marketing and Sales functions of Kohinoor Hospital, Mumbai. Prior to that he worked as Manager Sales and Marketing in top health care brands like Fortis and Asian Heart Institute. He is also the founder of Firsteye, a Strategic Creative Solution consultancy.  Currently playing a Business Consultant role with the world's largest health care user portal "Medisurge India Ltd" and "Topdoctorsonline.com".
				              		</p>
				              		<p class="classPLeaderText">				              			 
				              		 </p> 
				             	</div> <!-- Left div contain complete text -->
			              	</div> <!-- Div for one individual leader


			              	<!-- <div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/nilesh.jpg" />				              		
				              	</div> --><!-- Left div contain image and designation-->
				              	<!-- <div class="classDivLeaderContianer" id="idDivLeaderText">		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Virendra Vilankar</span><br/><span class="classSpanLeaderDesig">Creative Director, Lowe & Partners.</span></p>
				              		<p class="classPLeaderText"><b>Virendra</b> is the creative person behind this project and has an experience of over 10 years in managing advertising for some leading national and international brands.
				              		</p>
				              		<p class="classPLeaderText">
				              			 His advertising skills has won him many prestigious awards.
				              		 </p> -->
				            <!-- </div> --><!-- Left div contain complete text-->
			              	<!-- </div> --><!-- Div for one individual leader -->

			              	
			              </div><!-- Div for all individual leader -->	             

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




