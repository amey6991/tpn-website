<?php
require_once 'function.php';
$aCashPaymentDetail = array();
if (isset($_POST)) {  
  $aCashPaymentDetail['phr_member_payment_id']=$_POST['idBeforePayment'];
  $aCashPaymentDetail['emr_peduser_id']=$_POST['phrPedName'];
  $aCashPaymentDetail['phr_cash_payment_cash']=$_POST['phrCash'];
  $aCashPaymentDetail['phr_cash_payment_note']=$_POST['phrNote'];
  $aCashPaymentDetail['phr_cash_payment_date']=$_POST['phrPayDate'];
  $aCashPaymentDetail['phr_cash_payment_time']=$_POST['phrPayTime'];
  $sPaymentMode = "Cash";
}else{
  $aCashPaymentDetail['phr_member_payment_id']='';
  $aCashPaymentDetail['emr_peduser_id']='';
  $aCashPaymentDetail['phr_cash_payment_cash']='';
  $aCashPaymentDetail['phr_cash_payment_note']='';
  $aCashPaymentDetail['phr_cash_payment_date']='';
  $aCashPaymentDetail['phr_cash_payment_time']='';
  $sPaymentMode = "online";
}

// var_dump($aCashPaymentDetail);
// var_dump(get_ped_name_byid($aCashPaymentDetail['emr_peduser_id']));
// exit();


if (isset($_GET['freePlanId'])||$sPaymentMode=='Cash') {
   if ($sPaymentMode=='online') {
      $sIdBeforePayment=decriptUniqueCode($_GET['freePlanId']);    
   }else{
      $sIdBeforePayment= $aCashPaymentDetail['phr_member_payment_id'];
   }
 
 $returnFields = getData($sIdBeforePayment); 
 $sMsg='S4';
// var_dump($returnFields);
}else{
$sIdBeforePayment = isset($_GET['paymentid']) ? $_GET['paymentid'] : '';
$sMsg = isset($_GET['msg']) ? $_GET['msg'] : '';

$sIdBeforePayment=decriptUniqueCode($sIdBeforePayment); // Decript the ID
$sMsg=decriptUniqueCode($sMsg); // Decript the Message
 $returnFields = getData($sIdBeforePayment);
}

function getData($sIdBeforePayment){
$phrObj = new custom_PHR();
$temp = []; // declaring array variable

$DBMan  = new DBConnManager(); // Initialzing DBConnManager() Class; 
$conn=db_connect('tpn');
$conn   =  $DBMan->getConnInstance(); // Requiring connection instance
$aFields =array();
//$sIdBeforePayment=64;
$aFields=$phrObj->getDataBeforePayment($sIdBeforePayment);
return $aFields;
}


$aPostData = array(
"sChildPrivilege"=>$returnFields[0]["childPrivilege"],
"sName"=>$returnFields[0]["phrName"],  
"phrPlan"=>$returnFields[0]["phrPlan"],
"phrType"=>$returnFields[0]["phrType"],
"sBdate"=>$returnFields[0]["phrBdate"],
"eGender"=>$returnFields[0]["phrGender"],
"iAge"=>$returnFields[0]["phrName"],
"sContact"=>$returnFields[0]["phrContact"],
"sEmail"=>$returnFields[0]["phrEmail"],
"iUID"=>$returnFields[0]["phrUID"],
"sAddress"=>$returnFields[0]["phrAddress"],
"sArea"=>$returnFields[0]["phrArea"],
"iState"=>$returnFields[0]["state"],
"iCity"=>$returnFields[0]["city"],
"iCountry"=>$returnFields[0]["country"]
);


/*
Check the payment status before completing the signUp
*/  
  if($sMsg=='S4'){
     
      //create cURL connection
       // $curl_connection = curl_init('http://10.10.10.7/tpn/registerUserAPI.php');
        $curl_connection = curl_init('http://app.thepediatricnetwork.com/registerUserAPI.php');

        //$headers = array("Content-Type:multipart/form-data"); // cURL headers for file uploading

        //set options
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl_connection, CURLOPT_HEADER, true);
        
        //set data to be posted
        curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $aPostData);

        //perform our request
        $result = curl_exec($curl_connection);

        //close the connection
        curl_close($curl_connection);      
      
        //if(!empty($result)){   
  }else{
      sendToAdmin_Phr_PendingPay($aPostData);
      header('Location:success-signup.php?msg=E4&id='.$sIdBeforePayment);
  }

  if($result!=FALSE){   
    
    /*
    if payment mode is cash then only email.
    */
    if ($sPaymentMode=="Cash") {
      $bResult=insert_cash_payment_detail($aCashPaymentDetail);  
    }else{
      $bResult=false;
    }
    
    /*
      send email to admin when user selected Cash Payment Mode
    */    
    if ($bResult==true) {
      sendToAdmin_Phr_CashPaymentReg($aPostData,$aCashPaymentDetail);
    }
    header('Location:success-signup.php?msg='.$sMsg.'&id='.$sIdBeforePayment);
  }else{    
    sendToAdmin_Phr_PendingReg($aPostData,$sPaymentMode);
    header('Location:success-signup.php?msg=E4&id='.$sIdBeforePayment);
  }
    

?>