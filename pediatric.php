<html>
	<head>
		<title>Pediatric</title>		
	<?php include('header.php'); ?> 

<style>
#idActiveMenu1{
	color: #004480 !important;
  border-bottom-color:#004480 !important;
}
#idActiveMenu1 a{
color: #004480 !important;
  border-bottom-color:#004480 !important;
}
</style><!--Style for the active Link-->

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader">
				
				<div class="classDivMainHeader1">
					
					<?php include('body-header.php'); ?>

				<div class="classMainSliderDiv">
			  		

			  		<?php include('addSlider.php'); ?>
			  		<div>

				  		<div class="classDivDirectoryOnBanner">				  			
				  			<form name="SearchForm" method="POST" action="pediatric_search_result.php" >
				  				<span class="classFormFieldSpan classSpanMDText">
				  					MEDICAL DIRECTORY
				  				</span>
				  				<span class="classFormFieldSpan">
					  				<select class="form-control classFormFieldsCustom" style="width:100px!important;">
					  					<option value="none">For</option>
					  					<option value="Hospital">Hospital</option>
					  					<option value="Clinic">Clinic</option>
					  					<option value="Lab">Pharmacist</option>				  					
					  				</select>
					  				

					  			<!--	<select class="form-control classFormFieldsCustom" style="width:210px!important;">
					  					<option>Type</option>
					  					<option>Hospital</option>
					  					<option>Click</option>
					  					<option>Lab</option>				  					
					  				</select>
					  			-->
				  				</span>
				  				<span class="classFormFieldSpan">
				  					<!--<input type="text" class="form-control classFormFieldsCustom" name="type" style="width:150px;" placeholder="Type"/>-->
									<input type="text" class="form-control classFormFieldsCustom" name="location"  style="width: 410px;margin-right: 10px;" placeholder="Search By: Location, Name ( Mumbai, Dr. Shah )"/>
								<!--<select class="form-control classFormFieldsCustom" width="100px">
										<option>Location</option>
										<option>India</option>
										<option>USA</option>
										<option>China</option>
										<option>Japan</option>
										<option>UK</option>
									</select>
								-->
								</span>
				  				<span class="classFormFieldSpan">
				  					<input type="submit" value="SEARCH" class="classSearchButton"/>
				  				</span>	
				  			</form>
				  		</div>
			  			<!--<img src="images/1.png" alt="" height="350px" width="100%"/>    -->
					</div>
				</div>	  

			</div>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			           <!-- 
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom" id="Tab1Data" style="border-bottom: 4px solid rgba(116, 201, 227, 0.4);">

			              <div class="classDivHeading">
			              	PARENTS
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>Online pediatric backup to your child 24x7</li>
				              			<li>Electronic health records</li>
				              			<li>Disease and growth surveillance</li>
				              			<li>Anticipatory guidance</li>
				              			<li>Online connectivity to your pediatrician</li>
				              			<li>E-prescription to your local pharmacy</li>
				              			<li>Discounts from our network pharmacy</li>
				              			<li>Less doctor visits & more savings</li>
				              		</ul>
				              	</div>
				              	<div class="classDivForButtons">				              		
				              		<form name="ParentsForm" >
					           			<input type="text" class="classSpanUPButton classUPTextbox" name="ParentsUsername" placeholder="USERNAME">
					           			<input type="password" class="classSpanUPButton classUPTextbox" name="ParentsPassword" placeholder="PASSWORD">
					              		<input type="submit" class="classSpanSignInButton" value="SIGN IN">
				              		</form>				         
				              	</div>
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">
			              			<img class="classImgPDP" src="./images/parents.JPG"/>
			              		</div>		              		

			              		<div class="classDivForImgBenefits">
			              			<span class="classSpanBenefitText">BENEFITS</span>
			              			<a href="member-patient-benfits.php" ><img class="classImgBenefits" src="./images/1.png"/></a>
			              			<a href="member-patient-benfits.php" ><img class="classImgBenefits" src="./images/2.png"/></a>
			              			<a href="member-patient-benfits.php" ><img class="classImgBenefits" src="./images/3.png"/></a>
			              			<a href="member-patient-benfits.php" ><img class="classImgBenefits" src="./images/4.png"/></a>
			              			<a href="member-patient-benfits.php" ><img class="classImgBenefits" src="./images/5.png"/></a>
			              		</div>

			              	</div>

			              </div>

						</div>

			            <div class="classDivTabContainCustom" id="Tab2Data"  style="border-bottom: 4px solid rgba(116, 201, 227, 0.4);">

			              <div class="classDivHeading">
			              	DOCTOR
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>Conversion of your clinic to paperless clinic.</li>
				              			<li>Increased patient traffic</li>
				              			<li>Low-cost access to best-in-class patient	management systems</li>
				              			<li>Microsite for your profile</li>
				              			<li>Electronic health records and standardized	medical solutions for your patients</li>
				              			<li>Knowledge updates and your link to newer developments</li>
				              			<li>Partial outsourcing of semi-emergency calls</li>
				              			<li>Online scheduling of appointments</li>
				              			<li>Increased networking opportunities with other pediatricians</li>
				              			<li>Better integration with the pharmacies</li>
				              		</ul>
				              	</div>
				              	<div class="classDivForButtons">				              		
				              		<form name="DoctorForm" >
					           			<input type="text" class="classSpanUPButton classUPTextbox" name="DoctorUsername" placeholder="USERNAME">
					           			<input type="password" class="classSpanUPButton classUPTextbox"  name="DoctorPassword" placeholder="PASSWORD">				              		
					              		<input type="submit" class="classSpanSignInButton" value="SIGN IN" >
				              		</form>				         
				              	</div>
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">
			              			<img class="classImgPDP" src="./images/doctor.jpg"/>
			              		</div>		              		

			              		<div class="classDivForImgBenefits">
			              			<span class="classSpanBenefitText">BENEFITS</span>
			              			<a href="doctor-benfits.php" ><img class="classImgBenefits" src="./images/1.png"/></a>
			              			<a href="doctor-benfits.php" ><img class="classImgBenefits" src="./images/2.png"/></a>
			              			<a href="doctor-benfits.php" ><img class="classImgBenefits" src="./images/3.png"/></a>
			              			<a href="doctor-benfits.php" ><img class="classImgBenefits" src="./images/4.png"/></a>
			              			<a href="doctor-benfits.php" ><img class="classImgBenefits" src="./images/5.png"/></a>
			              		</div>

			              	</div>

			              </div>

						</div>

			            <div class="classDivTabContainCustom"  id="Tab3Data" >

			              <div class="classDivHeading">
			              	PHARMACIST
			              </div>

			              <div class="classDivLRContainer">

			              	<div class="classDivLContainer">
			              		<div>
				              		<ul class="classUlData">
				              			<li>More prescriptions</li>
				              			<li>Loyal clientele</li>
				              			<li>More discounts from companies</li>
				              			<li>Online presence</li>
				              			<li>Low cost advertising</li>
				              			<li>Latest drug information & trends</li>
				              			<li>Monthly business reviews & performance optimization</li>
				              			<li>AT NO ADDITIONAL COST</li>
				              		</ul>
				              	</div>
			              		
				              	<div class="classDivForButtons">				              		
				              		<form name="PharmacistForm" >
					           			<input type="text" class="classSpanUPButton classUPTextbox" name="PharmacistUsername" placeholder="USERNAME">
					           			<input type="password" class="classSpanUPButton classUPTextbox"  name="PharmacistPassword" placeholder="PASSWORD">
					              		<input type="submit" class="classSpanSignInButton" value="SIGN IN">
				              		</form>				         
				              	</div>
			              	</div>

			              	<div class="classDivLContainer">

			              		<div class="classDivForImg">
			              			<img class="classImgPDP" src="./images/pharmacist.jpg"/>
			              		</div>		              		

			              		<div class="classDivForImgBenefits">
			              			<span class="classSpanBenefitText">BENEFITS</span>
			              			<a href="pharmacist-benfits.php" ><img class="classImgBenefits" src="./images/1.png"/></a>
			              			<a href="pharmacist-benfits.php" ><img class="classImgBenefits" src="./images/2.png"/></a>
			              			<a href="pharmacist-benfits.php" ><img class="classImgBenefits" src="./images/3.png"/></a>
			              			<a href="pharmacist-benfits.php" ><img class="classImgBenefits" src="./images/4.png"/></a>
			              			<a href="pharmacist-benfits.php" ><img class="classImgBenefits" src="./images/5.png"/></a>
			              		</div>

			              	</div>

			              </div>

						</div>
					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




