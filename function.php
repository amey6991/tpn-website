<?php
/*
List of all Functions required.
*/


/*
function to establish the Data base Connection
*/
/*include("PHPMailer/class.phpmailer.php");
include("PHPMailer/class.smtp.php");*/
require_once 'class.PHRUserWeb.php';

//require_once ("config.EmailTemplate.php");
include("email/class.Email.php");
include("email/config.php");
//require_once ("/email/config.EmailTemplate.php");
require_once "email/mailer/PHPMailer/class.phpmailer.php";
require_once "email/mailer/PHPMailer/class.smtp.php";

/*require_once "/PHPMailer/class.phpmailer.php";
require_once "/PHPMailer/class.smtp.php";*/

function db_connect($sDataBase){	

try {
     $mysqli = new mysqli("localhost", "root", null, "tpn") ;
} catch (Exception $e ) {
     echo "Service unavailable";
     echo "message: " . $e->message;   // not in live code obviously...
     exit;
}

/*switch ($sDataBase) {
	case 'pediatric':{
		$mysqli = new mysqli("localhost", "root", null, "pediatric");		
		break;	
	}
	case 'tpn':{
		$mysqli = new mysqli("localhost", "root", null, "tpn");	

		break;	
	}
	default:{
		$mysqli=null;		
		break;
	}*/
		
	$returnValue=$mysqli;
	return $returnValue;
}


/*
Function to close database connection.
*/
function db_disconnect($bConnect){	// DB Connection Close
	$bConnect->close();
}

/*
Function to retrive location form db. 
*/
function get_location(){	
	$bConnect=db_connect('pediatric');
	
	if(!mysqli_connect_errno()){
	$sQuery="select * from location";
	$aLocation=array();
		if ($result =$bConnect->query($sQuery)) {
		    while($row=$result->fetch_array())
		    {
			    $aLocation[] =$row ;    
			    
			  }
		$result->close();
		}else{
			$aLocation="null";
		}
		
		db_disconnect($bConnect);
		return ($aLocation);
	}else{
		
		return("No Location Available");
	}
	

}

function checkIsset($sCheck){
	if($sCheck==null){
		return '';
	}else{
		return $sCheck;
	}
}

/*
Function to get pincode base on the location id form db. 
*/
function get_pincode($sSearchBy){		

	$bConnect=db_connect('pediatric');	
	if(!mysqli_connect_errno()){
	$sQuery="select lid, pincode, zone from location  where lid = {$sSearchBy}";	
	$aPincode=array();
		if ($result =$bConnect->query($sQuery)) {
		    while($row=$result->fetch_array())
		    {
			    $aPincode[] =$row ;    
			    
			  }
		$result->close();
		}else{
			$aPincode="null";
		}		
		db_disconnect($bConnect);
		return ($aPincode);
	}else{
		
		return("No Pincode Available");
	}
}



/*
Function to get the search result on the basis of 
type(Hospital, clinic and pharmacist) and  text(name, city,etc).
*/
function get_search_result($sType,$aText){		

	$aData=array();	
	$aData=do_search($sType,$aText);		
	if(is_null($aData)) {		
		$aData=null;		
	}
	return $aData;
}


/*
Function to retrive search result from the database on the basis of 
type(Hospital, clinic and pharmacist) and  text(name, city,etc).
*/
function do_search($sType,$aText){	
	
	$bConnect=db_connect('tpn');

	/* For the Pharmacist Search*/
	$sQSubName=null;
	$sQSubAdd=null;
	$sQSubArea=null;
	$sQSubEmail=null;

	if(!mysqli_connect_errno()){
	
		/*This Switch case direct the search result for;
		 Hopital, Clinic and Pharmacist.
			Note: Clinic and Hopital Table not available, Right now.

		*/
		switch ($sType) {
			case 'Pediatrician':{

				/* For the Doctor Search*/


				foreach ($aText as $key) {
					$sTemp="`tpn_emr_ped`.`ped_doc_name` LIKE '%{$key}%' OR ";
					$sQSubName=$sQSubName.$sTemp;	
					$sTemp="`tpn_clinics`.`tpn_clinic_address` LIKE '%{$key}%' OR ";
					$sQSubAdd=$sQSubAdd.$sTemp;
					$sTemp="`tpn_clinics`.`tpn_clinic_name` LIKE '%{$key}%' OR ";
					$sQSubClinic=$sQSubAdd.$sTemp;
					$sTemp="`tpn_clinics`.`tpn_clinic_area` LIKE '%{$key}%' OR ";
					$sQSubArea=$sQSubArea.$sTemp;
					$sTemp="`tpn_emr_ped`.`ped_doc_email` LIKE '%{$key}%' OR ";
					$sQSubEmail=$sQSubEmail.$sTemp;
				}
				
				/* To Remove OR after last loop*/
				$sQSubName=substr($sQSubName, 0, -3);
				$sQSubAdd=substr($sQSubAdd, 0, -3);
				$sQSubClinic=substr($sQSubClinic, 0, -3);
				$sQSubArea=substr($sQSubArea, 0, -3);
				$sQSubEmail=substr($sQSubEmail, 0, -3);

				$sGetDataFor="`tpn_emr_ped`.`ped_doc_name`, `tpn_clinics`.`tpn_clinic_address`,`tpn_clinics`.`tpn_clinic_pincode`, `tpn_clinics`.`tpn_clinic_area`, `tpn_emr_ped`.`ped_doc_mobile`, `tpn_emr_ped`.`ped_doc_email`, `tpn_clinics`.`tpn_clinic_name`, `tpn_clinics`.`tpn_clinic_coordinates`";
				if($aText[0]=='all'){
				$sQuery = "SELECT {$sGetDataFor}
							FROM tpn_ped_clinic
							left join tpn_clinics
							on `tpn_ped_clinic`.`tpn_clinic_id`=`tpn_clinics`.`tpn_clinic_id`
							left join tpn_emr_ped
							on `tpn_ped_clinic`.`emr_user_id`=`tpn_emr_ped`.`emr_usr_id`";
				/*$sQuery = "SELECT {$sGetDataFor} FROM tpn_emr_ped GROUP BY emr_peduser_id";*/
				$iTotalRecord=0;
				}else{
				
				$sQuery = "SELECT {$sGetDataFor}
							FROM tpn_ped_clinic
							left join tpn_clinics
							on `tpn_ped_clinic`.`tpn_clinic_id`=`tpn_clinics`.`tpn_clinic_id`
							left join tpn_emr_ped
							on `tpn_ped_clinic`.`emr_user_id`=`tpn_emr_ped`.`emr_usr_id`
							WHERE {$sQSubName} OR
						      {$sQSubAdd} OR
						      {$sQSubArea} OR
						      {$sQSubClinic} OR
						      {$sQSubEmail} GROUP BY `tpn_emr_ped`.`emr_peduser_id` order by `tpn_emr_ped`.`emr_peduser_id` ";
				/*
				$sQuery = "SELECT {$sGetDataFor} FROM tpn_emr_ped 
						WHERE {$sQSubName} OR
						      {$sQSubAdd} OR
						      {$sQSubArea} OR
						      {$sQSubClinic} OR
						      {$sQSubEmail} GROUP BY emr_peduser_id order by emr_peduser_id ";
				*/
						      			
				}
				//print_r($sQuery);
				//var_dump($sQuery);
				break;
			}
			case 'Pharmacist':{

			foreach ($aText as $key) {
				$sTemp="tpn_pharma_name LIKE '%{$key}%' OR ";
				$sQSubName=$sQSubName.$sTemp;
				$sTemp="tpn_pharma_address LIKE '%{$key}%' OR ";
				$sQSubAdd=$sQSubAdd.$sTemp;
				$sTemp="tpn_pharma_area LIKE '%{$key}%' OR ";
				$sQSubArea=$sQSubArea.$sTemp;
				$sTemp="tpn_pharma_email LIKE '%{$key}%' OR ";
				$sQSubEmail=$sQSubEmail.$sTemp;
			}
				/* To Remove OR after last loop*/
				$sQSubName=substr($sQSubName, 0, -3);
				$sQSubAdd=substr($sQSubAdd, 0, -3);
				$sQSubArea=substr($sQSubArea, 0, -3);
				$sQSubEmail=substr($sQSubEmail, 0, -3);


				$sGetDataFor="tpn_pharma_name, tpn_pharma_address, tpn_pharma_pincode, tpn_pharma_area, tpn_pharma_contact, tpn_pharma_email, tpn_pharma_discounts, home_delivery, gift_hampers, tpn_pharmacy_coordinates";
				if($aText[0]=='all'){
				$sQuery = "SELECT {$sGetDataFor} FROM tpn_pharmacy GROUP BY tpn_pharmacy_id";
				$iTotalRecord=0;
				}else{			

				$sQuery = "SELECT {$sGetDataFor} FROM tpn_pharmacy 
						WHERE {$sQSubName} OR
						      {$sQSubAdd} OR
						      {$sQSubArea} OR
						      {$sQSubEmail} GROUP BY tpn_pharmacy_id order by tpn_pharma_name ";
						      			
				}
				//var_dump($sQuery);
				break;		
			}
			default:{
				$sQuery=1;
				$iTotalRecord=0;
				break;
			}
		}

		//var_dump($sQuery);
		$aData=array();
		$aData=null;

		if ($result =$bConnect->query($sQuery)) {			
			$iCounterForPagination=0;
		    while($row=$result->fetch_array())
		    {
		    	$aData[] =$row ; 
		    				    
			  }
		
		$result->close();
		}else{
			$aData=null;
		}
		//var_dump($aData);
		db_disconnect($bConnect);
		return ($aData);
	}else{
		
		return null;
	}
}



/*
Function to display search result on the user screen.
type(Hospital, clinic and pharmacist) and  text(name, city,etc).
*/
function display_search_result($sType,$sText){
	
 
	$aPostData=get_post_data();	// Recived Post data and return array
	
	if(is_null($aPostData)) {
		$sType="all";
		$sText="all";
	}else{
		$sType=$aPostData[0];
		$aSymbol=array(" ","(",")",".","|",":",",","-");
		$sText=str_replace($aSymbol, $aSymbol[0], $aPostData[1]);	
		$aText=explode($aSymbol[0], $sText);		
	}
	
	$aData=array();	
	$aData=get_search_result($sType,$aText);	
	//var_dump($aData);
	if($sType=="all" && $sText=="all"){
		$sSearchByText="all";
	}elseif($sType!="all" && $sText=="all"){
		$sSearchByText="All , ".$sType;
	}elseif($sType!="all" && $sText!="all"){
		$sSearchByText=$sType.", ".$sText;
	}else{
		$sSearchByText=null;
	}
	$sResultHead ='<div class="classDivHeading">
			Search Result For :<span style="font-weight:lighter;"> '.$sSearchByText.' </span><!-- Search Query-->
			</div>	
          	<div style="width:100%;" id="">
          	<div class="holder"></div>
				<ul id="content" class="classUlPagination" style="list-style: none !important;">';
    $sResultFooter ='</ul></div>';

    $sResultBody=null;        
	
	if(is_null($aData)) {		
		$sResultBody="No Data Available for this : {$sText}";
		$ii=0;
	}else{		

		$ii=1;
		$iBFlag=1;
	if($sType=='Pharmacist'){
		foreach ($aData as $key){
			
			
			$sResultBody = $sResultBody.'<li><span><div class="classSearchResultWrapper" id="">				              		
						              		<div style="float:left;width:80%;"><p class="classPLeaderDesig">
						              			<span class="classSpanLeaderName">'.$key[0].' </span><br/>
						              			<span class="classSpanLeaderDesig"></span>
						              		</p>
						              		<p class="classPLeaderText">													
						              			<span class="classSpanResultText" ><span class="classLightColor">Address : </span><span >'.preg_replace('/[^A-Za-z0-9\-]/', ' ', $key[1]).' -'.$key[2].'</span></span>					              			
						              			<input type="hidden" id="address'.$ii.'" value="'.preg_replace('/[^A-Za-z0-9\-]/', ' ', $key[1]).'" />
 						              			<span class="classSpanResultText" ><span class="classLightColor">Area : </span><span >'.$key[3].'</span></span>
						              			<input type="hidden" id="area'.$ii.'" value="'.$key[3].'" />
						              			<span class="classSpanResultText"><span class="classLightColor">Phone no.: </span><span>'.$key[4].'</span></span>
						              			<span class="classSpanResultText"><span class="classLightColor">Email : </span><span>'.$key[5].'</span></span>
						              			<span class="classSpanResultText"><span class="classLightColor">TPN Partner :</span><span>Yes</span></span>';						              			
						    $aSymbol=array(" ","(",")",".","|",":",",","-");
							//$sText=str_replace($aSymbol, $aSymbol[0], $aPostData[1]);	
							$alatLong=explode(',', $key[9]);  
							//var_dump($key[9]);
							if(isset($alatLong[0])==true){
								$dLatitude=$alatLong[0];
							}else{
								$dLatitude='';
							}
							if(isset($alatLong[1])==true){
								$dLongitude=$alatLong[1];    
							}else{
								$dLongitude='';
							}
							
							
							
						     $sResultBody = $sResultBody.'<div class="classDivBLContainer">';

											     if($key[6]!=null){
								                  $sResultBody = $sResultBody.'<span class="classSpanBL" ><p style="float:left;line-height: 0px;color:#004480;">'.$key[6].'%</p><img src="images/business/custom_discount.png" title="Discount Offered"  class="classImgBL"></span>';
								                }else{
								                  $sResultBody = $sResultBody.'<span class="classSpanBL" ><p style="float:left;line-height: 0px;color:#004480;"></p></span>';                }
								                if($key[7]==1){
								                  $sResultBody = $sResultBody.'<span class="classSpanBL" ><img src="images/business/custom_delivery.png" title="Home Delivery"  class="classImgBL"></span>';
								                }else{
								                   $sResultBody = $sResultBody.'<span class="classSpanBL" ></span>';
								                }
								                if($key[8]==1){
								                   $sResultBody = $sResultBody.'<span class="classSpanBL" ><img src="images/business/custom_gift.png" title="Gift Hamper"  class="classImgBL"></span>';
								                }else{
								                    $sResultBody = $sResultBody.'<span class="classSpanBL" ></span>';
								                }

						     $sResultBody = $sResultBody.'</div>
						              		</p></div>
						              		<input type="hidden" id="idLatitude'.$ii.'" name="latitude'.$ii.'" value="'.$dLatitude.'"/>
						              		<input type="hidden" id="idLongitude'.$ii.'" name="longitude'.$ii.'" value="'.$dLongitude.'"/>
						              		<div id="map-canvas'.$ii.'" class="classDivMap" style=""  onclick="getMap('.$ii.')">click to view map</div>
						              	</div></span></li>';	
			              
			
			$ii++;			     				              	
			$iBFlag++;
						       
		}// Foreach end.
	}

	if($sType=='Pediatrician'){
		foreach ($aData as $key){
			
			
			$sResultBody = $sResultBody.'<li><span><div class="classSearchResultWrapper" id="">				              		
						              		<div style="float:left;width:80%;"><p class="classPLeaderDesig">
						              			<span class="classSpanLeaderName"> Dr.'.$key['ped_doc_name'].' </span><br/>
						              			<span class="classSpanLeaderDesig"></span>
						              		</p>
						              		<p class="classPLeaderText">													
						              			<span class="classSpanResultText" ><span class="classLightColor">Address : </span><b>'.$key['tpn_clinic_name'].' </b> <span >'.preg_replace('/[^A-Za-z0-9\-]/', ' ', $key['tpn_clinic_address']).' - '.$key['tpn_clinic_pincode'].'</span></span>					              			
						              			<input type="hidden" id="address'.$ii.'" value="'.preg_replace('/[^A-Za-z0-9\-]/', ' ', $key['tpn_clinic_address']).' '.$key['tpn_clinic_pincode'].'" />
 						              			<span class="classSpanResultText" ><span class="classLightColor">Area : </span><span >'.$key['tpn_clinic_area'].'</span></span>
						              			<input type="hidden" id="area'.$ii.'" value="'.$key['tpn_clinic_area'].'" />
						              			<span class="classSpanResultText"><span class="classLightColor">Phone no.: </span><span>'.$key['ped_doc_mobile'].'</span></span>
						              			<span class="classSpanResultText"><span class="classLightColor">Email : </span><span>'.$key['ped_doc_email'].'</span></span>
						              			<span class="classSpanResultText"><span class="classLightColor">TPN Partner :</span><span>Yes</span></span>';						              			
						              			
						     $aSymbol=array(" ","(",")",".","|",":",",","-");
							//$sText=str_replace($aSymbol, $aSymbol[0], $aPostData[1]);	
							$alatLong=explode(',', $key['tpn_clinic_coordinates']);  
							//var_dump($key[9]);
							if(isset($alatLong[0])==true){
								$dLatitude=$alatLong[0];
							}else{
								$dLatitude='';
							}
							if(isset($alatLong[1])==true){
								$dLongitude=$alatLong[1];    
							}else{
								$dLongitude='';
							}
							

						     $sResultBody = $sResultBody.'</p></div>
						     				<input type="hidden" id="idLatitude'.$ii.'" name="latitude'.$ii.'" value="'.$dLatitude.'"/>
						              		<input type="hidden" id="idLongitude'.$ii.'" name="longitude'.$ii.'" value="'.$dLongitude.'"/>						              		
						              		<div id="map-canvas'.$ii.'" class="classDivMap" style=""  onclick="getMap('.$ii.')">click to view map</div>
						              	</div></span></li>';	
			              
			
			$ii++;			     				              	
			$iBFlag++;
						       
		}// Foreach end.
	}
		
	}
	
	$sResutl=$sResultHead.$sResultBody.$sResultFooter.'<input type="hidden" name="totalRow" id="idTotalRow" value='.$ii.'>' ;
	return 	$sResutl;
}


/*
Function to recieve post data and return it as array.
*/
function get_post_data(){	

	$aSearchForText=array();	
	if(isset($_POST)){
		
		if(isset($_POST['stype'])){
			$aSearchForText[0]=$_POST['stype'];
		}else{
			$aSearchForText[0]="all";
		}

		if(isset($_POST['location'])){
			$aSearchForText[1]=$_POST['location'];
			if($aSearchForText[1]==null){
				$aSearchForText[1]="all";	
			}
		}else{
			$aSearchForText[1]="all";
		}
	}else{
		$aSearchForText=null;
	}
	return $aSearchForText;
}



/* Following function are not in use right now, the only structure was design.*/
/*
Function prototype to get area form db. 
Rightnow function not in used.
*/
function get_area(){
	
}

/*
Function prototype to get Zone form db. 
Rightnow function not in used.
*/
function get_zone(){	
	$bConnect=db_connect();
	if($bConnect==True){
		
		//$aZone=
		
		return ($aZone);
	}else{
		
		return("No Zone Available");
	}
}


/*
Function prototype to get City form db. 
Rightnow function not in used.
*/
function get_city(){		
	$bConnect=db_connect();
	if($bConnect==True){		
		db_disconnect();
		return ($aCity);
	}else{

		
		return("No City Available");
	}
}

/*
Function For Taking Post and return post data via addslashes();
*/
function inputPostWeb($sPost){
	if($sPost==null){
		return $sPost=null;
	}else{
	$sPost=addslashes($sPost);
		return $sPost;	
	}
}

function calculateAge($date){
return floor((strtotime(date('d-m-Y')) - strtotime($date))/(60*60*24*365.2421896));
}

function redirectWithAlert($sPage, $sMsg){
$sRedirect=$sPage.'?id='.$sMsg;
header("Location: {$sRedirect}") ;
}

function displayState(){
	$phrObj = new custom_PHR();
	$aState = array();
	$aState= $phrObj->getStates();
	if($aState!=null){
		$idValue=$aState;	
	}else{
		$idValue=null;	
	}
	return $idValue;

}

function displayCountry(){
	$phrObj = new custom_PHR();
	$aCountry = array();
	$aCountry= $phrObj->getCountry();
	if($aCountry!=null){
		$idValue=$aCountry;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

function getCityById($id){
	$phrObj = new custom_PHR();
	$sCity = null;
	$sCity= $phrObj->getCityName($id);
	if($sCity!=null){
		$idValue=$sCity;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

function getCity(){
	$phrObj = new custom_PHR();
	$sCity = null;
	$sCity= $phrObj->getAllCityName();
	if($sCity!=null){
		$idValue=$sCity;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

function getAreaById($id){
	$phrObj = new custom_PHR();
	$sArea = null;
	$sArea= $phrObj->getAreaName($id);
	if($sArea!=null){
		$idValue=$sArea;	
	}else{
		$idValue=null;	
	}	
	return $idValue;	
}
function getStateById($id){
	$phrObj = new custom_PHR();
	$sState = null;
	$sState= $phrObj->getStateName($id);
	if($sState!=null){
		$idValue=$sState;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}
function getCountryById($id){
	$phrObj = new custom_PHR();
	$sCountry = null;
	$sCountry= $phrObj->getCountryName($id);
	if($sCountry!=null){
		$idValue=$sCountry;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

/*
Function to fetch the Doctor Specialization.
*/
function getDoctorSpecialization(){
	$phrObj = new custom_PHR();
	$aSpecialization = null;
	$aSpecialization= $phrObj->getSpecializationName();
	if($aSpecialization!=null){
		$idValue=$aSpecialization;	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

/*
Function to do call for DB to retrieve Data
*/
function showDataBeforePayment($sIdBeforePayment){
	$aData=array();	
	$phrObj = new custom_PHR();
	$aData=$phrObj->getDataBeforePayment($sIdBeforePayment);		
	if(is_null($aData)) {		
		$aData=empty($aData);		
	}
	return $aData;
}


/*
Function to recived $_POST array and return associative array.
*/
function getPostArray($PostArray=[]){
	$temp= array();
	
	$temp['sMembershipPlan']   = inputPostWeb($PostArray['membership-plan']);
 	$temp['ePhrType']          = inputPostWeb($PostArray['phrType']);
    $temp['sChildPrivilege']   = inputPostWeb($PostArray['childPrivilege']);
    $temp['sName']             = inputPostWeb($PostArray['phrName']);
    $temp['sBdate']            = date('d-m-Y',strtotime($PostArray['phrBdate'])); 
    //var_dump($temp['sBdate']);
    $temp['iAge']              = calculateAge($temp['sBdate']);
    //var_dump($temp['iAge']);
    $temp['eGender']           = inputPostWeb(isset($PostArray['phrGender']) ? $PostArray['phrGender']: '');
    //$temp['sUserImage']        = isset($_FILES['phrImage']['name'])?$_FILES['phrImage']:'';    	
    //var_dump($temp['sUserImage']);
    $temp['sContact']          = inputPostWeb($PostArray['phrContact']);
    $temp['sEmail']            = inputPostWeb($PostArray['phrEmail']);
    $temp['sAddress']          = inputPostWeb($PostArray['phrAddress']);
    $temp['sArea']             = inputPostWeb($PostArray['phrArea']);
    $temp['iCountry']          = inputPostWeb($PostArray['country']);
    $temp['iState']            = inputPostWeb($PostArray['state']);
    $temp['iCity']             = inputPostWeb($PostArray['city']);

    $temp['iPincode']             = inputPostWeb($PostArray['pincode']);    
	return $temp;

}
 
function showPlanAmount($id){
	$phrObj = new custom_PHR();
	$iPlanAmount = null;	
	$iPlanAmount= $phrObj->getPlanAmount($id);
	if($iPlanAmount!=null){

		$idValue=$iPlanAmount[0][0];	
	}else{
		$idValue=null;	
	}
	return $idValue;
}

function generateUniqueCode($idValue){
	$sString="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";		
	$aSplitString=str_split($sString);
	$iLength=count($aSplitString);
	$iLength--;
	$sTemp=null;
	$iIndex=null;

	for($ii=0;$ii<4;$ii++){
		for ($ij=0; $ij <5 ; $ij++) { 
			if($ii%2==0){
				$iIndex=rand(0,$iLength);
				$sTemp=$sTemp.$aSplitString[$iIndex];
			}else{
				$iIndex=rand(0,$iLength);
				$sTemp=$sTemp.ord($aSplitString[$iIndex]);
			}
		}

		$sTemp=$sTemp."-";
		if($ii==2){				// Id value is at [3] i.e. Fourth index after exploding string to array by - delimeter.
			$sTemp=$sTemp.$idValue;
			$sTemp=$sTemp."-";
		}
	}

	Return $sTemp;

}  	
function explodArrayForId($id){
    $aId=array();
    $aId=explode("-", $id);
    $id=$aId[3];		// Id Value Index.
    return $id;
}
function decriptUniqueCode($sVal){
    $aVal=array();
    $aVal=explode("-", $sVal);
    $sReturnVal=$aVal[3];		// Id Value Index.
    return $sReturnVal;
}

function directAndSendMail($sForWhome,$aData){

		switch ($sForWhome) {
			case 'patient':
				
				break;
			case 'doctor':
				
				break;
			case 'pharmacist':				
				$result1=sendToUserPharmaEmail($aData);
				//$result1=true;
				$result2=sendToAdminPharmaEmail($aData);				
				break;
			
			default:
				$result1=false;
				$result2=false;
				break;
		}

	if($result1==true && $result2==true){
		$result=true;		
	}else{
		$result=false;
	}

	return $result;
}


function sendToUserPharmaEmail($aFields){


$mail=new PHPMailer();

	$sSiteURL = "http://thepediatricnetwork.com";
	$sLogoURL = "http://app.thepediatricnetwork.com/img/pediatric_logo.png";
	//$sSupportID = "support@thepediatricnetwork.com";
	
	$mail->IsSMTP();
	$mail->Host = "smtp.zoho.com";
	$mail->Port = 465;
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "support@thepediatricnetwork.com"; // SMTP username
	$mail->Password = "plus91support"; // SMTP password
	$email=$aFields['pharmaEmail'];//"laxmikant.killekar@plus91.in"; // Recipients email ID
	$name=$aFields['pharmaName']; // Recipient's name
	$mail->From = "support@thepediatricnetwork.com";
	$mail->FromName = "The Pediatric Network";
	$mail->AddAddress($email,$name);	
	$mail->WordWrap = 50; // set word wrap
	//$mail->AddAttachment("/var/tmp/file.tar.gz"); // attachment
	//$mail->AddAttachment("/tmp/image.jpg", "new.jpg"); // attachment
	$mail->IsHTML(true); // send as HTML
	$mail->Subject = "Thank You For Sign up At The Pediatric Network";

	$mail->Body = "<div style='border: 1px solid #cccccc;border-radius: 5px;padding: 20px;'>
		<div style='display:inline; padding-left:1em;'>
			<img src={$sLogoURL} style='float:right;width:90px;margin-left:50px;' />
			<h2 style='line-height:36px;margin: 0;'>The Pediatric Network </h2>
		</div>

		<hr style='border: 0px;border-top: 1px solid #ccc;'/>

		<div style='padding: 15px;border-radius: 15px;'>
			<span style='font-weight: bold;font-size: 18px'> Dear {$aFields['pharmaName1']}, </span>
			<br/><br/>
			Thank you for signing up with the Pediatric Network, the portal which helps you to take benefit.
			<br/><br/>
			Your details are send for the verification once it verified The Pediatric Network will contact you.
			<br/>
			Thank you!				
			<br/>
			<br />
			Regards<br />
			The Pediatric Network Team
		</div>
	</div>";




	if(!$mail->Send())
	{
	  //echo "Mailer Error: " . $mail->ErrorInfo;
	  return false;
	}
	else
	{
	  //echo "Message has been sent";
	  return true;
	}

}


function sendToAdminPharmaEmail($aFields){

	$mail=new PHPMailer();

	$sSiteURL = "http://thepediatricnetwork.com";
	$sLogoURL = "http://app.thepediatricnetwork.com/img/pediatric_logo.png";
	//$sSupportID = "support@thepediatricnetwork.com";

	$mail->IsSMTP();
	$mail->Host = "smtp.zoho.com";
	$mail->Port = 465;
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "support@thepediatricnetwork.com"; // SMTP username
	$mail->Password = "plus91support"; // SMTP password
	$email="admin@thepediatricnetwork.com"; // Recipients email ID
	$name="Admin"; // Recipient's name
	$ccEmail="atishladdad@yahoo.co.in";
	$ccEmail="a.patkar@plus91.in";
	$ccName="Admin";
	$mail->From = "support@thepediatricnetwork.com";
	$mail->FromName = "The Pediatric Network";
	$mail->AddAddress($email,$name);
	$mail->AddCC($ccEmail,$ccName);
	$mail->WordWrap = 50; // set word wrap
	$mail->IsHTML(true); // send as HTML
	$mail->Subject = "Pharmacist Interested In Sign up At The Pediatric Network";


$mail->Body = "<div style='border: 1px solid #cccccc;border-radius: 5px;padding: 20px;'>
	<div style='display:inline; padding-left:1em;'>
		<img src='{$sLogoURL}' style='float:right;width:90px;margin-left:50px;' />
		<h2 style='line-height:36px;margin: 0;'>The Pediatric Network </h2>
	</div>

	<hr style='border: 0px;border-top: 1px solid #ccc;'/>

	<div style='padding: 15px;border-radius: 15px;'>
		<span style='font-weight: bold;font-size: 18px'> Dear Admin, </span>
		<br/><br/>
		Following are the detail of Pharmacist : <u> {$aFields['pharmaName']} </u>. 
		<br/><br/>
		Please do verify the following details to add pharmacist in \"The Pediatric Network \".
		<br/>
		Pharmacist Details :<br/>
		Date of Sign Up : {$aFields['doc']} <br/>
		Name of Pharmacy :	{$aFields['pharmaName']} <br/>
		Contact No  : {$aFields['pharmaContact']} <br/>
		Email Id :	{$aFields['pharmaEmail']} <br/>
		Name of Owner :	{$aFields['pharmaName1']} <br/>
		Mobile of Owner :	{$aFields['pharmaContact1']} <br/>
		Email of Owner :	{$aFields['pharmaEmail1']} <br/>
		Designation of Contact Person :	{$aFields['pharmaDesignation1']} <br/>
		Address :	{$aFields['pharmaAddress']} <br/>
		Area :	{$aFields['pharmaArea']} <br/>
		Area Catering To :	{$aFields['pharmaArea2']} <br/>
		Another Area Catering To :	{$aFields['pharmaArea3']} <br/>
		<br /><br />
		Regards<br />
		The Pediatric Network Team
	</div>
</div>";


	if(!$mail->Send())
	{
	 	//echo "Mailer Error: " . $mail->ErrorInfo;
	  	return false;
	}
	else
	{
	  	//echo "Message has been sent";
	 	return true;
	}
	//exit();


}


function sendToAdmin_Phr_PendingPay($aFields){

	$mail=new PHPMailer();
	
	$sSiteURL = "http://thepediatricnetwork.com";
	$sLogoURL = "http://app.thepediatricnetwork.com/img/pediatric_logo.png";
	//$sSupportID = "support@thepediatricnetwork.com";

	$mail->IsSMTP();
	$mail->Host = "smtp.zoho.com";
	$mail->Port = 465;
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "support@thepediatricnetwork.com"; // SMTP username
	$mail->Password = "plus91support"; // SMTP password
	$email="admin@thepediatricnetwork.com"; // Recipients email ID
	//$email="laxmikant.killekar@plus91.in";	
	$name="Admin"; // Recipient's name
	$ccEmail="atishladdad@yahoo.co.in";
	$ccEmail="a.patkar@plus91.in";
	//$ccEmail="laxmikant.killekar@plus91.in";	
	$ccName="Admin";
	$mail->From = "support@thepediatricnetwork.com";
	$mail->FromName = "The Pediatric Network";
	$mail->AddAddress($email,$name);
	$mail->AddCC($ccEmail,$ccName);
	$mail->WordWrap = 50; // set word wrap
	$mail->IsHTML(true); // send as HTML
	$mail->Subject = "Patient Sign up payment is in under process.";

	$sCity=getCityById($aFields['iCity']);	
	$sCountry=getCountryById($aFields['iCountry']);
	$sState=getStateById($aFields['iState']);
	// Get Plan Name.
	if($aFields['phrPlan']==1){
		$plan=1;
		$planName='Gold';

	}elseif($aFields['phrPlan']==2){
		$plan=2;
		$planName='Silver';
	}elseif($aFields['phrPlan']==3){
		$plan=3;
		$planName='Bronze';
	}elseif($aFields['phrPlan']==4){
		$plan=4;
		$planName='Test';
	}else{
		$planName='';
	}
	$dateOfSignUp=date("d-m-Y");
	$mail->Body = "<div style='border: 1px solid #cccccc;border-radius: 5px;padding: 20px;'>
	<div style='display:inline; padding-left:1em;'>
		<img src='{$sLogoURL}' style='float:right;width:90px;margin-left:50px;' />
		<h2 style='line-height:36px;margin: 0;'>The Pediatric Network </h2>
	</div>

	<hr style='border: 0px;border-top: 1px solid #ccc;'/>

	<div style='padding: 15px;border-radius: 15px;'>
		<span style='font-weight: bold;font-size: 18px'> Dear Admin, </span>
		<br/><br/>
		Please do verify that the patient with following details Sign up and the payment is in under Process.
		<br/><br/>
		Check Payment Status and Do Sign Up for the Patient.
		<br/><br/><br/>
		Patient Details :<br/>
		Date of Sign Up : {$dateOfSignUp} <br/>
		Name of Patient :	{$aFields['sName']} <br/>
		Plan  :	{$planName} <br/>
		Child Privilege :	{$aFields['sChildPrivilege']} <br/>		
		Contact No  : {$aFields['sContact']} <br/>
		Email Id :	{$aFields['sEmail']} <br/>
		UID :	{$aFields['iUID']} <br/>
		Birth Date :	{$aFields['sBdate']} <br/>
		Gender :	{$aFields['eGender']} <br/>
		Address :	{$aFields['sAddress']} <br/>
		Area :	{$aFields['sArea']} <br/>
		City :	{$sCity[0][0]} <br/>
		State :	{$sState[0][0]} <br/>
		Country :	{$sCountry[0][0]} <br/>		
		<br /><br />
		Regards<br />
		The Pediatric Network Team
	</div>
</div>";


	if(!$mail->Send())
	{
	 	//echo "Mailer Error: " . $mail->ErrorInfo;
	  	return false;
	}
	else
	{
	  	//echo "Message has been sent";
	 	return true;
	}
	//exit();


}

function sendToAdmin_Phr_PendingReg($aFields, $sPaymentMode){

	$mail=new PHPMailer();

	$sSiteURL = "http://thepediatricnetwork.com";
	$sLogoURL = "http://app.thepediatricnetwork.com/img/pediatric_logo.png";
	//$sSupportID = "support@thepediatricnetwork.com";

	$mail->IsSMTP();
	$mail->Host = "smtp.zoho.com";
	$mail->Port = 465;
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "support@thepediatricnetwork.com"; // SMTP username
	$mail->Password = "plus91support"; // SMTP password
	$email="admin@thepediatricnetwork.com"; // Recipients email ID
	//$email="laxmikant.killekar@plus91.in"; // Recipients email ID
	$name="Admin"; // Recipient's name
	$ccEmail="atishladdad@yahoo.co.in";
	$ccEmail="a.patkar@plus91.in";
	
	//$ccEmail="laxmikant.killekar@plus91.in";	
	$ccName="Admin";
	$mail->From = "support@thepediatricnetwork.com";
	$mail->FromName = "The Pediatric Network";
	$mail->AddAddress($email,$name);
	$mail->AddCC($ccEmail,$ccName);
	$mail->WordWrap = 50; // set word wrap
	$mail->IsHTML(true); // send as HTML
	$mail->Subject = "Patient Sign up registration is in under process.";

	$sCity=getCityById($aFields['iCity']);	
	$sCountry=getCountryById($aFields['iCountry']);
	$sState=getStateById($aFields['iState']);
	// Get Plan Name.
	if($aFields['phrPlan']==1){
		$plan=1;
		$planName='Gold';

	}elseif($aFields['phrPlan']==2){
		$plan=2;
		$planName='Silver';
	}elseif($aFields['phrPlan']==3){
		$plan=3;
		$planName='Bronze';
	}elseif($aFields['phrPlan']==4){
		$plan=4;
		$planName='Test';
	}else{
		$planName='';
	}
	$dateOfSignUp=date("d-m-Y");
	$mail->Body = "<div style='border: 1px solid #cccccc;border-radius: 5px;padding: 20px;'>
	<div style='display:inline; padding-left:1em;'>
		<img src='{$sLogoURL}' style='float:right;width:90px;margin-left:50px;' />
		<h2 style='line-height:36px;margin: 0;'>The Pediatric Network </h2>
	</div>

	<hr style='border: 0px;border-top: 1px solid #ccc;'/>

	<div style='padding: 15px;border-radius: 15px;'>
		<span style='font-weight: bold;font-size: 18px'> Dear Admin, </span>
		<br/><br/>
		Please do verify that the Patient Sign up not completed and payment was done.
		<br/><br/>
		Check Payment Status and Do Sign Up for the Patient.
		<br/><br/><br/>
		Patient Details :<br/>
		Date of Sign Up : {$dateOfSignUp} <br/>
		Name of Patient :	{$aFields['sName']} <br/>
		Plan  :	{$planName} <br/>
		Child Privilege :	{$aFields['sChildPrivilege']} <br/>		
		Contact No  : {$aFields['sContact']} <br/>
		Email Id :	{$aFields['sEmail']} <br/>
		UID :	{$aFields['iUID']} <br/>
		Birth Date :	{$aFields['sBdate']} <br/>
		Gender :	{$aFields['eGender']} <br/>
		Address :	{$aFields['sAddress']} <br/>
		Area :	{$aFields['sArea']} <br/>
		City :	{$sCity[0][0]} <br/>
		State :	{$sState[0][0]} <br/>
		Country :	{$sCountry[0][0]} <br/>	
		Payment Mode : {$sPaymentMode}<br/>			
		<br /><br />
		Regards<br />
		The Pediatric Network Team
	</div>
</div>";


	if(!$mail->Send())
	{
	 	//echo "Mailer Error: " . $mail->ErrorInfo;
	  	return false;
	}
	else
	{
	  	//echo "Message has been sent";
	 	return true;
	}
	//exit();


}


/*
Function to update payment mode
*/

function insert_cash_payment_detail($aCashPaymentDetail){

	$bConnect=db_connect('pediatric');	
	if(!mysqli_connect_errno()){

		$scpd_payment_id = $aCashPaymentDetail['phr_member_payment_id'];
		$scpd_peduser_id = $aCashPaymentDetail['emr_peduser_id'];
		$scpd_cash = $aCashPaymentDetail['phr_cash_payment_cash'];
		$scpd_note = $aCashPaymentDetail['phr_cash_payment_note'];
		$scpd_date = $aCashPaymentDetail['phr_cash_payment_date'];
		$scpd_time = $aCashPaymentDetail['phr_cash_payment_time'];

		 $sQuery  = "INSERT INTO `phr_cash_payment_detail`
				 			(`phr_member_payment_id`, `emr_peduser_id`, `phr_cash_payment_cash`, `phr_cash_payment_note`, `phr_cash_payment_date`, `phr_cash_payment_time`) 
	                 VALUES ('{$scpd_payment_id}',
						      '{$scpd_peduser_id}',
						      '{$scpd_cash}',
						      '{$scpd_note}',
						      '{$scpd_date}',
						      '{$scpd_time}')";
		// var_dump($sQuery);
		if ($result =$bConnect->query($sQuery) === TRUE) {
		    $bResult = true;
		}else{
			$bResult = false;
		}		
		db_disconnect($bConnect);		
	}else{
		
		$bResult = false;
	}
	// var_dump($bResult);
	// exit();
	return ($bResult);
}


/*
This function is to send email to admin when user signup and selected cash payment option.
*/
function sendToAdmin_Phr_CashPaymentReg($aFields,$aCashPaymentDetail){

	$mail=new PHPMailer();

	$sSiteURL = "http://thepediatricnetwork.com";
	$sLogoURL = "http://app.thepediatricnetwork.com/img/pediatric_logo.png";
	//$sSupportID = "support@thepediatricnetwork.com";

	$mail->IsSMTP();
	$mail->Host = "smtp.zoho.com";
	$mail->Port = 465;
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; // turn on SMTP authentication
	$mail->Username = "support@thepediatricnetwork.com"; // SMTP username
	$mail->Password = "plus91support"; // SMTP password
	// $email="a.patkar@plus91.in"; // Recipients email ID
	$email="admin@thepediatricnetwork.com"; // Recipients email ID
	$name="Admin"; // Recipient's name
	$ccEmail="atishladdad@yahoo.co.in ";
	$ccEmail="a.patkar@plus91.in";	
	$ccName="Admin";
	$mail->From = "support@thepediatricnetwork.com";
	$mail->FromName = "The Pediatric Network";
	$mail->AddAddress($email,$name);
	$mail->AddCC($ccEmail,$ccName);
	$mail->WordWrap = 50; // set word wrap
	$mail->IsHTML(true); // send as HTML
	$mail->Subject = "Patient Sign Up - Payment Mode Cash.";

	$sCity=getCityById($aFields['iCity']);	
	$sCountry=getCountryById($aFields['iCountry']);
	$sState=getStateById($aFields['iState']);
	// Get Plan Name.
	if($aFields['phrPlan']==1){
		$plan=1;
		$planName='Basic';

	}elseif($aFields['phrPlan']==2){
		$plan=2;
		$planName='Silver';
	}elseif($aFields['phrPlan']==3){
		$plan=3;
		$planName='Bronze';
	}elseif($aFields['phrPlan']==4){
		$plan=4;
		$planName='Test';
	}else{
		$planName='';
	}
	$ped_name=get_ped_name_byid($aCashPaymentDetail['emr_peduser_id']);
	//var_dump($ped_name); exit();
	$dateOfSignUp=date("d-m-Y");
	$mail->Body = "<div style='border: 1px solid #cccccc;border-radius: 5px;padding: 20px;'>
	<div style='display:inline; padding-left:1em;'>
		<img src='{$sLogoURL}' style='float:right;width:90px;margin-left:50px;' />
		<h2 style='line-height:36px;margin: 0;'>The Pediatric Network </h2>
	</div>

	<hr style='border: 0px;border-top: 1px solid #ccc;'/>

	<div style='padding: 15px;border-radius: 15px;'>
		<span style='font-weight: bold;font-size: 18px'> Dear Admin, </span>
		<br/><br/>
		Please verify that the Patients Sign Up through Cash Payment Mode.
		<br/><br/>
		Following are the patient and payment detail's
		Patient Details :<br/>
		Date of Sign Up : {$dateOfSignUp} <br/>
		Name of Patient :	{$aFields['sName']} <br/>
		Plan  :	{$planName} <br/>
		Child Privilege :	{$aFields['sChildPrivilege']} <br/>		
		Contact No  : {$aFields['sContact']} <br/>
		Email Id :	{$aFields['sEmail']} <br/>
		UID :	{$aFields['iUID']} <br/>
		Birth Date :	{$aFields['sBdate']} <br/>
		Gender :	{$aFields['eGender']} <br/>
		Address :	{$aFields['sAddress']} <br/>
		Area :	{$aFields['sArea']} <br/>
		City :	{$sCity[0][0]} <br/>
		State :	{$sState[0][0]} <br/>
		Country :	{$sCountry[0][0]} <br/>	
		Payment Mode : Cash<br/>	
		<br /><hr/><br />
		<strong>Payment Details :</strong><br/>
		Name of Selected Pediatrician :	{$ped_name} <br/>
		Cash  :	{$aCashPaymentDetail['phr_cash_payment_cash']} <br/>
		Note :	{$aCashPaymentDetail['phr_cash_payment_note']} <br/>
		Date :	{$aCashPaymentDetail['phr_cash_payment_date']} <br/>
		Time :	{$aCashPaymentDetail['phr_cash_payment_time']} <br/>	
		<br />
		Regards<br />
		The Pediatric Network Team
	</div>
</div>";


	if(!$mail->Send())
	{
	 	//echo "Mailer Error: " . $mail->ErrorInfo;
	  	return false;
	}
	else
	{
	  	//echo "Message has been sent";
	 	return true;
	}
	//exit();


}



/*
Function to get the pediatric name list with id
*/
function get_pediatric_list(){	
	$bConnect=db_connect('pediatric');
	
	if(!mysqli_connect_errno()){
	$sQuery="select `emr_peduser_id`,`ped_doc_name` from tpn_emr_ped where emr_peduser_id != 1 and `ped_doc_status` = 1  and  `ped_doc_is_deleted`=0";
	$aLocation=array();
		if ($result =$bConnect->query($sQuery)) {
		    while($row=$result->fetch_array())
		    {
			    $aPed[] =$row ;    
			    
			  }
		$result->close();
		}else{
			$aLocation="null";
		}
		
		db_disconnect($bConnect);
		return ($aPed);
	}else{
		
		return($aPed='null');
	}
	
}



function get_ped_name_byid($iPedId){
	$bConnect=db_connect('pediatric');
	
	if(!mysqli_connect_errno()){
	$sQuery="select `ped_doc_name` from tpn_emr_ped where emr_peduser_id = {$iPedId} and `ped_doc_is_deleted`=0";
	$aLocation=array();
		if ($result =$bConnect->query($sQuery)) {
		    while($row=$result->fetch_array())
		    {
			    $aPed[] =$row ;    
			    
			  }
		$result->close();
		}else{
			$aLocation="null";
		}
		
		db_disconnect($bConnect);
		return ($aPed[0]['ped_doc_name']);
	}else{
		
		return($aPed='null');
	}
}
?>



