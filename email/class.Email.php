<?php
//require_once dirname(__FILE__)."/../config/config.db.php";
//require_once ("class.DBConnManager.php"); //! Connect with DB connection manager class file.
//require_once dirname(__FILE__)."/../mailer/emailFunctions.php"; //! connect with email functions file
//require_once dirname(__FILE__)."/../config/config.Email_SMS.php";
/*require_once "PHPMailer/class.phpmailer.php";
require_once "PHPMailer/class.smtp.php";
*/
class Email{

	public $sSenderName;
	public $sSenderEmailId;

	public $sRecipientName = array();
	public $sRecipientEmailId = array();

	public $sSubject;
	public $sBody;
	public $sAttachments = array();
	public $aEmbeddedImage = array();

	public $dCurrentDate;

	public $iPriority = 1;
	public $iInsertEId;

	private $iEmailStatus;

	private $iRecipientCount = 0;
	private $iAttachmentCount = 0;
	private $iEmbeddedImageCount = 0;

	function __construct($sRecipientEmailId,$sRecipientName,$sSubject,$sBody,$sSenderEmailId,$sSenderName){

		if($sSenderEmailId == '' && $sSenderName == ''){

			$this->sSenderName = "support@thepediatricnetwork.com";
			$this->sSenderEmailId = "plus91support" ;
		}else{
			$this->sSenderName = $sSenderName;
			$this->sSenderEmailId = $sSenderEmailId;
		}
		
		$this->sSubject = $sSubject;
		$this->sBody = $sBody;
		$this->dCurrentDate = date("Y-m-d h:i:s");

		$this->iInsertEId = $this->addEmail();

		$iRecipientId = $this->addRecipient($sRecipientEmailId,$sRecipientName);
		
		$this->iEmailStatus = 0;
	}

	//! brief function to get database connection.
	private function getEmailSysConn(){

		$DBMan = new DBConnManager(); // Initialzing DBConnManager() Class; 
		$oConn =  $DBMan->getConnInstance(); // Requiring connection insatnce
		//var_dump($oConn);exit("Testing..");
		
		return $oConn;
	}

	//! brief function to add basic email info to the DB
	private function addEmail(){

		$iInsertId = 0;
		$sExtra = '';

		$sEmailBody = addslashes(htmlentities($this->sBody)); //! Encode the Email Body Text to prevent sqli injection
		$sEmailSubject = addslashes(htmlentities($this->sSubject)); //! Encode the Email Subject Text to prevent sqli injection

		$sInsertQuery = "INSERT INTO `email_master`(`id`, `from_email_id`, `from_name`, `subject`, `body`, `created_on`, `extra`) 
						VALUES (NULL,'{$this->sSenderEmailId}','{$this->sSenderName}','{$sEmailSubject}','{$sEmailBody}','{$this->dCurrentDate}','{$sExtra}')";
		$oMysqli = $this->getEmailSysConn();

		if($oMysqli != false){
			
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				
				$iInsertId = $oMysqli->insert_id;
			}
		}
		
		$oMysqli->close();

		return $iInsertId;
	}

	////! @brief - Function to Add Receiver Info 
	public function addRecipient($sREmailId,$sRName)
	{
		
		$iInsertId = 0;
		$sExtra = '';

		$sInsertQuery = "INSERT INTO `email_recipient`(`id`, `e_id`, `to_email_id`, `to_name`, `status`, `extra`) 
						VALUES (NULL,'{$this->iInsertEId}','{$sREmailId}','{$sRName}', 0,'{$sExtra}')";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				$iInsertId = $oMysqli->insert_id;
			}
		}
		

		$this->sRecipientEmailId[] = $sREmailId;
		$this->sRecipientName[] = $sRName;

		$this->iRecipientCount++;

		$oMysqli->close();
		return $iInsertId;
	}

	//! @brief - Function to Add Email Attachment
	public function addAttachment($aAttachFile,$sFileName)
	{
		$iInsertId = 0;
		$sExtra = '';

		//! Move file to given folder
		$this->fMoveAttachement($aAttachFile);

		$sRealFileName = basename($aAttachFile);
		$sFilePath = dirname($aAttachFile)."/";
		$sFilesize = filesize($aAttachFile)." bytes";
		$sFileExtension = substr($sRealFileName, strrpos($sRealFileName, '.')+1);
		$sContentType = '';

		$sInsertQuery = "INSERT INTO `email_attachements`(`id`, `e_id`, `real_filename`, `file_path`, `content_type`, `size`, `file_extension`, `valid`, `CID`,'status') 
						VALUES (NULL,'{$this->iInsertEId}','{$sRealFileName}','{$sFilePath}','{$sContentType}','{$sFilesize}','{$sFileExtension}', 0,'',1)";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				$iInsertId = $oMysqli->insert_id;
			}
		}
		
		$this->sAttachments[] = $aAttachFile;
		$this->iAttachmentCount++;

		$oMysqli->close();
		return $iInsertId;
	}

	//! brief function to add embeded images
	public function fAddEmbeddedImage($aAttachFile,$sFileName,$sCID){

		$iInsertId = 0;
		$sExtra = '';

		//! Move file to given folder
		$this->fMoveAttachement($aAttachFile);

		$sRealFileName = basename($aAttachFile);
		$sFilePath = dirname($aAttachFile)."/";
		$sFilesize = filesize($aAttachFile)." bytes";
		$sFileExtension = substr($sRealFileName, strrpos($sRealFileName, '.')+1);
		$sContentType = '';

		$sInsertQuery = "INSERT INTO `email_attachements`(`id`, `e_id`, `real_filename`, `file_path`, `content_type`, `size`, `file_extension`, `valid`, `CID`,'status') 
						VALUES (NULL,'{$this->iInsertEId}','{$sRealFileName}','{$sFilePath}','{$sContentType}','{$sFilesize}','{$sFileExtension}', 0,'{$sCID}',2)";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				$iInsertId = $oMysqli->insert_id;
			}
		}
		
		$this->aEmbeddedImage[] = array("filePath"=>$aAttachFile,"fileName"=>$sFileName,"CID"=>$sCID);

		$this->iAttachmentCount++;

		$oMysqli->close();
		return $iInsertId;
	}

	//! function to delete recipient if any
	public function deleteRecipient($iRecipientId){
		
		$iResult = 0;

		$sUpdateQuery = "UPDATE `email_recipient` SET `status` = '1' WHERE `id` = '{$iRecipientId}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$iResult = $oMysqli->query($sUpdateQuery);
		}

		return $iResult;
	}

	//! function to delete attachment if any
	public function deleteAttachement($iAttachmentId){
		
		$iResult = 0;

		$sUpdateQuery = "UPDATE `email_attachements` SET `valid` = '1' WHERE `id` = '{$iAttachmentId}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$iResult = $oMysqli->query($sUpdateQuery);
		}

		return $iResult;
	}

	//! function to update subject of email
	public function updateSubject($sUpdatedSubject,$iEId){

		$iResult = 0;

		$sUpdateQuery = "UPDATE `email_master` SET `subject` = '{$sUpdatedSubject}' WHERE `id` = '{$iEId}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$iResult = $oMysqli->query($sUpdateQuery);
		}

		return $iResult;
	}

	//! function to update email body
	public function updateBody($sUpdatedBody,$iEId){

		$iResult = 0;

		$sUpdateQuery = "UPDATE `email_master` SET `body` = '{$sUpdatedBody}' WHERE `id` = '{$iEId}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$iResult = $oMysqli->query($sUpdateQuery);
		}

		return $iResult;
	}

	//! brief function to add Email data in to the database
	public function addToQueue(){

		$iInsertId = 0;
		$sExtra = '';

		$sInsertQuery = "INSERT INTO `email_queue`(`id`, `e_id`, `status`, `priority`, `added_on`, `extra`) 
						VALUES (NULL,'{$this->iInsertEId}', '{$this->iEmailStatus}','{$this->iPriority}','{$this->dCurrentDate}','{$sExtra}')";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				$iInsertId = $oMysqli->insert_id;
			}
		}
		
		$oMysqli->close();
		return $iInsertId;
	}

	//! brief function to send a mail using PHPMailer class
	//! For that use Mailer()
	public function sendNow(){

		$aAttacments = implode(",",$this->sAttachments);
		//$aEmbeddedAttachment = implode(",", $this->aEmbeddedImage);
		$aRecipientEIds = implode(",",$this->sRecipientEmailId);
		$aRecipientName = implode(",",$this->sRecipientName);
		
  		$iError = fMailer($this->sSubject,$this->sBody,$aRecipientEIds,$aRecipientName,$aAttacments,$this->aEmbeddedImage,$this->sSenderEmailId,$this->sSenderName);

		if($iError == 1) {
		  	$this->iExceptionId = 2;
		  	$this->sExceptionMsg = 'Not Send';
		  	$this->iEmailStatus = 1;
		  	$this->addToQueue();
		  	$this->addEmailAttempts();
		}else{
		  	$this->iExceptionId = 1;
		  	$this->sExceptionMsg = 'Send Successfully';
		  	$this->iEmailStatus = 0;
		  	$this->addToQueue();
		  	$this->addEmailAttempts();
		}

		return $iError;
	}

	//! brief function to save attachement file in folder and used from it
	private function fMoveAttachement($sAttachedFile){

		$sFileName = basename($sAttachedFile);
		if(file_exists("attachements/".$sFileName)){

			$sTemp = explode(".",$sFileName);
			$sFileName = $sTemp[0].md5("p").".".$sTemp[1];
		}
		//print_r($sFileName);exit();
		$sSourseOfFile = $sAttachedFile;
		$sDestinationOfFile = "attachements/".$sFileName;
		//! Copy the file to stored it in projects folder
		copy($sSourseOfFile,$sDestinationOfFile);
	}

	//! brief function to add email attempts
	private function addEmailAttempts(){

		$sExtra = '';
		$sInsertQuery = "INSERT INTO `email_attempts`(`id`, `e_id`, `exception_id`, `exception_msg`, `attempt_on`, `extra`) 
						VALUES (NULL,'{$this->iInsertEId}','{$this->iExceptionId}','{$this->sExceptionMsg}','{$this->dCurrentDate}','{$sExtra}')";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){
			$sInserQueryResult = $oMysqli->query($sInsertQuery);
			if($sInserQueryResult != false){

				$iInsertId = $oMysqli->insert_id;
			}
		}
		
		$oMysqli->close();
	}

	//! brief function to process email queue
	private function processEmailQueue(){

		$sSelectQuery = "SELECT `e_id` FROM `email_queue` WHERE `status` = 1 AND `priority` = 1";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){

			$sSelectQueryResult = $oMysqli->query($sSelectQuery);
			if($sSelectQueryResult != false){

				while($aRow = $sSelectQueryResult->fetch_array()){

					$iEID = $aRow['e_id'];
					if($iEID != ''){

						$this->sendPendingMails($iEid);
					}
				}
			}
		}
	}

	//! brief function to create email
	public function sendPendingMails($iEID){

		$this->sSubject = '';
		$this->sBody = '';
		$this->sSenderName = '';
		$this->sSenderEmailId = '';
		$this->sRecipientEmailId = '';
		$this->sRecipientEmailName = '';

		$this->getEmailBasic($iEID);
		$this->getEmailRecipient($iEID);
		$this->getEmailAttachment($iEID);
		$iResult = $this->sendSoft();
		if($iResult == 0){

			$this->updateEmailQueue($iEID,$iResult);
		}
	}

	//! brief function to get email
	private function getEmailBasic($iEID){

		$sSelectQuery = "SELECT `email_master`.`from_email_id`, `email_master`.`from_name`, `email_master`.`subject`, `email_master`.`body`, `email_master`.`from_email_id_psw`
						FROM `email_master` WHERE `email_master`.`id` = '{$iEID}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){

			$sSelectQueryResult = $oMysqli->query($sSelectQuery);
			if($sSelectQueryResult != false){

				while($aRow = $sSelectQueryResult->fetch_array()){
					
					$this->sSubject = $aRow['subject'];
					$this->sBody = $aRow['body'];
					$this->sSenderName = $aRow['from_name'];;
					$this->sSenderEmailId = $aRow['from_email_id'];;
				}
			}
		}
	}

	//! brief function to get recipient for email
	private function getEmailRecipient($iEID){

		$aRecipients = array();

		$sSelectQuery = "SELECT `email_recipient`.`to_email_id`, `email_recipient`.`to_name`
						FROM `email_recipient` WHERE `email_recipient`.`e_id` = '{$iEID}' AND `status` =0";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){

			$sSelectQueryResult = $oMysqli->query($sSelectQuery);
			if($sSelectQueryResult != false){

				while($aRow = $sSelectQueryResult->fetch_array()){
					
					$this->sRecipientEmailId[] = $aRow['to_email_id'];
					$this->sRecipientEmailName[] = $aRow['to_name'];
				}
				//$this->sRecipientEmailId = $aRecipientsEid;
				//$this->sRecipientEmailName = $aRecipientsName;
			}
		}
	}

	//! brief function to get email attachment
	private function getEmailAttachment($iEID){

		$aAttacments = array();

		$sSelectQuery = "SELECT `email_attachements`.`real_filename`, `email_attachements`.`generated_filename`,`email_attachements`.`file_path`
						FROM `email_attachements` WHERE `email_attachements`.`e_id` = '{$iEID}' AND `valid` =0";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){

			$sSelectQueryResult = $oMysqli->query($sSelectQuery);
			if($sSelectQueryResult != false){

				while($aRow = $sSelectQueryResult->fetch_array()){
					
					$this->sAttachments[] = $aRow['file_path'].$aRow['real_filename'];
				}
				//$this->sAttachments = $aAttacments;
			}
		}
	}

	//! brief function to send a mail using PHPMailer class
	//! For that use Mailer()
	private function sendSoft(){

		$aAttacments = implode(",",$this->sAttachments);
		$aRecipientEIds = implode(",",$this->sRecipientEmailId);
		$aRecipientName = implode(",",$this->sRecipientName);
		
  		//$iError = fMailer($this->sSubject,$this->sBody,$aRecipientEIds,$aRecipientName,$aAttacments,$this->sSenderName);

		if($iError == 1) {
		  	$this->iExceptionId = 2;
		  	$this->sExceptionMsg = 'Not Send';
		  	$this->iEmailStatus = 0;
		  	$this->addEmailAttempts();
		}else{
		  	$this->iExceptionId = 1;
		  	$this->sExceptionMsg = 'Send Successfully';
		  	$this->iEmailStatus = 1;
		  	$this->addEmailAttempts();
		}

		return $iError;
	}

	//! brief function to update email Queue status
	private function updateEmailQueue($iEID,$iStatus){

		$sUpdateQueryResult = '';
		$sUpdateQuery = "UPDATE `email_queue` SET `status`= '{$iStatus}' WHERE `e_id`= '{$iEID}'";
		$oMysqli = $this->getEmailSysConn();
		if($oMysqli != false){

			$sUpdateQueryResult = $oMysqli->query($sUpdateQuery);
		}

		return $sUpdateQueryResult;
	}
}
?>