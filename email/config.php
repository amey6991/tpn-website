<?php
//* This File will hold all the configuration varibale
//* Required for App

// Absolute path to root
define('ABS_PATH','/tpn');

//URL of the APP.
if($_SERVER['HTTP_HOST'] == 'localhost'){
    define('SITE_URL','http://localhost/tpn/');
    define('SITE_URL_PHR','http://localhost/tpn/phr_login.php/');
    define('SITE_URL_PHARMACY','http://localhost/tpn/pharmacy_login.php/');
    define('WEBSITE_URL',"http://localhost/tpn/");
}else{
	define('SITE_URL','http://app.thepediatricnetwork.com/');
	define('SITE_URL_PHR','http://app.thepediatricnetwork.com/phr_login.php/');
	define('SITE_URL_PHARMACY','http://app.thepediatricnetwork.com/pharmacy_login.php/');
	define('WEBSITE_URL',"http://thepediatricnetwork.com/");
}


//Session time out in minutes
define('SESSION_TIME_OUT','10');

//Allowed file extensions for app 
define('ALLOWED_EXTENSION', ".doc .docx .pdf .jpg .jpeg .png .gif .bmp");

//defining date format constant
define('DATE_FORMAT', 'd-m-Y');

//Start Time for The Clinic
define('START_TIME','09:00 AM');

//End Time For The Clinic
define('END_TIME','10:00 PM');

//Time Gap Between Appointments
define('GAP',20);

//File Path for error Logging
define('LOGPATH',dirname(__FILE__));

//Anticipatory Guide Folders
define('ANTICIPATORYGUIDE',"AnticipatoryGuides");


define('SMS_ON',FALSE);

define('EMAIL_ON',FALSE);

?>
