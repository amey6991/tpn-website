<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();

if ($securimage->check($_POST['captcha_code']) == false) {
  $_SESSION['capchaCode']=0;
 $_SESSION['postBack']=$_POST;
  
//echo "invalid capcha";
  header("Location: doctor-sign-up.php");
  exit();
}else{
  if(isset($_SESSION['postBack'])){
    unset($_SESSION['postBack']);
  }
  if(isset($_SESSION['capchaCode'])){
    unset($_SESSION['capchaCode']);
  }
  
}

require_once 'function.php';


//var_dump($_POST);

if(isset($_POST)){
    $aPostData=$_POST;
    $aDataToSend=getPostData($aPostData); // Function return the data sending to app.

}else{
 
    header('Location:success-doctor-signup.php?msg=E4');    
}

  //create cURL connection

  $curl_connection = curl_init('http://app.thepediatricnetwork.com/registerDoctorAPI.php');
    /*$curl_connection = curl_init('http://plus91hq.linkpc.net:82/tpn/registerDoctorAPI.php');*/

  //$headers = array("Content-Type:multipart/form-data"); // cURL headers for file uploading

  //set options
  curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
  curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
  //curl_setopt($curl_connection, CURLOPT_HEADER, true);
  
  //set data to be posted
  curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $aDataToSend);

  //perform our request
  $result = curl_exec($curl_connection);
//echo $result;

//var_dump($result);
  //close the connection
  curl_close($curl_connection);
  
//exit();
  if(!empty($result)){
    header('Location:success-doctor-signup.php?msg=S4');

  }else{
    header('Location:success-doctor-signup.php?msg=E4');    
  }



function getPostData($aPostData){

    if(isset($aPostData) && !empty($aPostData)){ // creating temp array for data processing
      $temp['ip_address']       = inputPostWeb($aPostData['ip_address']);
      $temp['pedFirstName']     = inputPostWeb($aPostData['pedFirstName']);
      $temp['pedLastName']      = inputPostWeb($aPostData['pedLastName']);
      $temp['pedSpecializtn']   = inputPostWeb($aPostData['pedSpecializtn']);
      $temp['pedMCICode']       = inputPostWeb($aPostData['pedMCICode']); 
      $temp['pedGender']        = inputPostWeb(isset($aPostData['pedGender']) ? $aPostData['pedGender']: '');     
      $temp['pedDOB']           = inputPostWeb($aPostData['pedDOB']);
      $temp['pedMobileNo']      = inputPostWeb($aPostData['pedMobileNo']);
      $temp['pedEmailid']       = inputPostWeb($aPostData['pedEmailid']);
      $temp['pedAddress']       = inputPostWeb($aPostData['pedAddress']);
      $temp['pedCity']          = inputPostWeb($aPostData['pedCity']);
      $temp['pedArea']          = inputPostWeb($aPostData['pedArea']);
      $temp['pedZone']          = inputPostWeb($aPostData['zone']);
      $temp['pedPincode']       = inputPostWeb($aPostData['pincode']);
      $temp['pedState']         = inputPostWeb($aPostData['pedState']);
      $temp['pedCountry']       = inputPostWeb($aPostData['pedCountry']);      
      return $temp;
    }else{
      return '';
    }
}
?>