<html>
	<head>
		<title>Pediatric - About Us</title>		
	<?php include('header.php'); ?> 
<style>
#idActiveMenu2{
	color: #004480 !important;
  border-bottom-color:#004480 !important;
}
#idActiveMenu2 a{
color: #004480 !important;
  border-bottom-color:#004480 !important;
}

</style><!--Style for the active Link-->

	</head>

	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           <!--
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	About
			              </div>
			              	<div class="classAboutUsText">			              		
			              		<p class="classPLeaderText">
				              		<strong>The Pediatric Network </strong> is a company with a sole objective – to be a child’s health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leverage technology to deliver integrated, reliable and cost-effective health services to a child, and peace of mind to the parents. 
				              	</p>
				              	<p class="classPLeaderText">
									We have integrated technology to fill in the lacunae felt by parents while rearing their kids. Our team of pediatricians in concert with your pediatrician will try to fix day to day issues while bringing up your young one. We will not only give you a safe backup in emergencies but will be vigilantly involved in watching his physical, mental and emotional growth over the years.
								</p>
								<p class="classPLeaderText">
									Through this network we have tried to find value for parents, pediatricians and our group of pharmacies.
								</p>
							</div>
						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




