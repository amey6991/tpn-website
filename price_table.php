
<!-- DC Pricing Tables CSS -->
<link type="text/css" rel="stylesheet" href="price_table/tsc_pricingtables.css" />
<!-- DC Pricing Tables JS -->
<script type="text/javascript" src="price_table/tsc_pricingtables.js"></script>

<!-- DC Pricing Tables:1 Start -->
  <div id="tsc_pricingtable01" class="clear">
    <!-- <div class="plan">
      <h3>Bronze<span><div style="line-height: 30px;margin-top: 30px;">3000 pa</div></span></h3>
      <a class="signup" href="patient-plan-sign-up.php?plan=Bronze">Sign up</a>
      <ul>
        <li>Single Child</li>
        <li>Access Electronic Medical Records</li>
        <li>Upload Child Health Records</li>
        <li>Pediatrician Network Access</li>
        <li>Pharmacy Network Access</li>
        <li> -<br/></li>
      </ul>
    </div>
    <div class="plan" id="most-popular">
      <h3>Silver<span><div style="line-height: 30px;margin-top: 30px;">5000 pa</div></span></h3>
      <a class="signup" href="patient-plan-sign-up.php?plan=Silver">Sign up</a>
      <ul>
        <li>Single Child</li>
        <li>Access Electronic Medical Records</li>
        <li>Upload Child Health Records</li>
        <li>Pediatrician Network Access</li>
        <li>Pharmacy Network Access</li>
        <li>24 Hour Doctor Backup</li>
      </ul>
    </div> -->
    <div class="single-plan plan" style="width: 50%">
      <h3>Basic<span><div style="line-height: 30px;margin-top: 30px;">250 pa</div></span></h3>
      <a class="signup" href="patient-plan-sign-up.php?plan=Basic">Sign up</a>
      <ul>
        <li>Multiple Children</li>
        <li>Access Electronic Medical Records</li>
        <li>Upload Child Health Records</li>
        <li>Pediatrician Network Access</li>
        <li>Pharmacy Network Access</li>
        <li>24 Hour Doctor Backup</li>
      </ul>
    </div>
  </div>
<!-- DC Pricing Tables:1 End -->
<div class="tsc_clear"></div> <!-- line break/clear line -->


