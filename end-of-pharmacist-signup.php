<html>
	<head>
		<title>Pediatric - Pharmacist Sign Up Complete</title>		
	<?php include('header.php'); ?> 

	</head>
<?php


$sFields =null;

if(isset($_GET['msg'])){ // Pass id for retrieving data before payment.
	
	$sMsg=$_GET['msg'];
	//var_dump($sIdBeforePayment);
	if($sMsg=='E4'){
		$sFields="Sorry you could not complete the sign up process. Please <a href='doctor-sign-up.php' >try again</a> or contact support at support@thepediatricnetwork.com";

	}else{	
		
		$sFields='<b>Thank You!</b> <br/>For submitting your details. </br>Our team will contact you within 48 hours to verify your claim and welcome you aboard the Pharmacy Network.';
	}

	
}else{
	$sFields="Sorry you could not complete the sign up process. Please <a href='doctor-sign-up.php' >try again</a> or contact support at support@thepediatricnetwork.com";
}

?>
	<body>
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin">

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			          <!-- 
			            <div class="tabs classUlTabCustom">
			              <div id="tab3" class="classDivTabCustom" style="z-index:5;"><a class="classLICustomTab" href="#Tab3Data" active>PHARMACIST</a></div>			              
			              <div id="tab2" class="classDivTabCustom" style="z-index:4;"><a class="classLICustomTab" href="#Tab2Data">DOCTOR</a></div>
			              <div id="tab1" class="classDivTabCustom" style="z-index:3;"><a class="classLICustomTab" href="#Tab1Data">PARENTS</a></div>
			            </div>
			            
			            <ul class="tabs classUlTabCustom">
			              <li id="tab3" style="z-index:5;"><a class="classLICustomTab" href="#">PHARMACIST</a></li>			              
			              <li id="tab2" style="z-index:4;"><a class="classLICustomTab" href="#">DOCTOR</a></li>
			              <li id="tab1" style="z-index:3;"><a class="classLICustomTab" href="#">PRENTS</a></li>
			            </ul>
			        -->

			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Welcome
			              </div>	
			              <div class="classDivTextWrapper">			              		
			              		<p class="classPLeaderText classPMsgWrapper" style="color:#222 !important;">
				              		<?php
				              			echo $sFields;
				              		?>
				              	</p>
				              	
							</div>	             

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




