<div id="slider_holder" class="sixteen columns">
				<div id="sequence">
					<div class="prev" ><span></span></div>
					<div class="next" ><span></span></div>
	
					<ul style="list-style:none!important;">
						<!-- Layer 1 -->
						<li>
							<div class="info ">
								<!-- <div class="classDivInfo"> -->
									<p class="">
										<img class="classImgStartQuote" src="images/req/start.png"/>
										<span class="classSpanStartQuoteText"><strong>From your baby's first cry till puberty, </br>
										From trivia to anxiety, </br>
										Every challenge, big or small, </br>
										Every squeal, step and fall, </br>
										The right advice, the right call. </br>
										Holding your baby's hand always. </br>
										The Pediatric Network. 24X7. </strong></span>
										<img class="classImgEndQuote"src="images/req/end.png"/>
									</p>	
									<p style="width:985px;text-align:justify;">
										<span class="classSpanStartQuoteText"><strong>The Pediatric Network</strong></span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leverage technology to deliver integrated, reliable and cost-effective health services to a child and peace of minds to the parents.
									</p>														
								<!-- </div>														 -->
							</div>
							<img class="slider_bgr" src="images/slider/slider-1.JPG"/>
						</li>						
						
						<!-- Layer 2 -->
						
						<li>
							<div class="info ">
								<!-- <div class="classDivInfo"> -->
									<p class="">
										<img class="classImgStartQuote" src="images/req/start.png"/>
										<span class="classSpanStartQuoteText"><strong>From your baby's first cry till puberty, </br>
										From trivia to anxiety, </br>
										Every challenge, big or small, </br>
										Every squeal, step and fall, </br>
										The right advice, the right call. </br>
										Holding your baby's hand always. </br>
										The Pediatric Network. 24X7. </strong></span>
										<img class="classImgEndQuote"src="images/req/end.png"/>
									</p>	
									<p style="width:985px;text-align:justify;">
										<span class="classSpanStartQuoteText"><strong>The Pediatric Network</strong></span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leverage technology to deliver integrated, reliable and cost-effective health services to a child and peace of minds to the parents.
									</p>														
								<!-- </div>														 -->
							</div>
							<img class="slider_bgr" src="images/slider/slider-2.JPG"/>
						</li>	
						<!-- Layer 3 -->
						<li>
							<div class="info ">
								<!-- <div class="classDivInfo"> -->
									<p class="">
										<img class="classImgStartQuote" src="images/req/start.png"/>
										<span class="classSpanStartQuoteText"><strong>From your baby's first cry till puberty, </br>
										From trivia to anxiety, </br>
										Every challenge, big or small, </br>
										Every squeal, step and fall, </br>
										The right advice, the right call. </br>
										Holding your baby's hand always. </br>
										The Pediatric Network. 24X7. </strong></span>
										<img class="classImgEndQuote"src="images/req/end.png"/>
									</p>	
									<p style="width:985px;text-align:justify;">
										<span class="classSpanStartQuoteText"><strong>The Pediatric Network</strong></span> is a company with a sole objective - to be a child's health support system. Founded by a group of pediatricians and IT professionals, the company seeks to leverage technology to deliver integrated, reliable and cost-effective health services to a child and peace of minds to the parents.
									</p>														
								<!-- </div>														 -->
							</div>
							<img class="slider_bgr" src="images/slider/slider-3.JPG"/>
						</li>	
						<!-- Layer 3 -->		
									
					</ul>
				</div>
			</div>
			<!-- Sequence Slider::END-->

			<script type="text/javascript">	
			var preloader = ($.browser.msie) ? false : true ;
				
			$(document).ready(function(){
				var options = {
					autoPlay: true,
					autoPlayDelay: 10000,
					nextButton: true,
					prevButton: true,
					preloader: preloader,
					animateStartingFrameIn: false,
					transitionThreshold: 0,
					fallback: {
			        	theme: "fade",
			        	speed: 5000
			        }
				};
				
				var sequence = $("#sequence").sequence(options).data("sequence");
	
				sequence.afterLoaded = function(){
					//$(".info").css('display','block'); // To display Next and Previous Buttons
					$(".next").css('display','none');
					$(".prev").css('display','none');
					$(".info").css('display','block');
					$("#sequence").hover(
					        function() {
					        	$(".prev, .next").stop().animate({opacity:0.7},300);	            
					        },
					        function() {        
					        	$(".prev, .next").stop().animate({opacity:0},300);
					        }
					);
					
					$(".prev, .next").hover(
					        function() {
					        	$(this).stop().animate({opacity:1},200);	            
					        },
					        function() {        
					        	$(this).stop().animate({opacity:0.7},200);
					        }		
					);
				}
			})
		</script>

	