<?php
/*Session is just open to maintain captcha*/
session_start(); 
/*Capcha class*/
include('securimage/securimage.php');
/*Capcah class Object*/
$securimage = new securimage();

if ($securimage->check($_POST['captcha_code']) == false) {
  $_SESSION['capchaCode']=0;  
//echo "invalid capcha";
  header("Location: pharmacist-sign-up.php");
  exit();
}else{
  $_SESSION['capchaCode']=1; 
}

/*Required Main Funtion File of Website Part*/
require_once 'function.php';

$aFileds = array();
if(isset($_POST)){
	$aFileds=$_POST;	

	foreach ($_POST as $key => $value) {
		$aFileds[$key]=inputPostWeb($value);
	}

}else{
	unset($aFileds);
}
$aFileds['doc'] = date("m/d/Y h:i:s a", time());


if($aFileds['pharmaArea']!=null && $aFileds['pharmaArea'] !='select' && $aFileds['pharmaArea'] != ''){
	$aTemp = getAreaById($aFileds['pharmaArea']);
	$aFileds['pharmaArea']=$aTemp[0][0];
}else{
	$aFileds['pharmaArea']='';
}
if($aFileds['pharmaArea']!=null && $aFileds['pharmaArea'] !='select' && $aFileds['pharmaArea'] != ''){
	$aTemp = getAreaById($aFileds['pharmaArea2']);
	$aFileds['pharmaArea2']=$aTemp[0][0];
}else{
	$aFileds['pharmaArea2']='';
}

$bReturn=directAndSendMail('pharmacist',$aFileds);

if($bReturn==true){	
	header("Location: end-of-pharmacist-signup.php?msg=S4");
}else{
	header("Location: end-of-pharmacist-signup.php?msg=E4");
}

?>