<?php

function generateUniqueOrderID($customParameter, $amount, $sCustName, $sCustAddress, $sCustCountry, $sCustTel, $sCustEmail, $sCustState, $sCustCity, $sCustZip) {

    $mysqli = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DEFAULT);

    if ($mysqli->connect_error) {
        return false;
    }

    $sCreatedOn = date("Y-m-d H:i:s");
    
    $sQuery = "INSERT INTO `payment_data` (`id`, `custom_param`, `amount`, `cust_name`, `cust_address`, `cust_country`, `cust_tel`, `cust_email`, `cust_state`, `cust_city`, `cust_zip`, `created_on`, `completed_on`, `payment_status`) VALUES (NULL, '{$customParameter}', '$amount', '{$sCustName}', '{$sCustAddress}', '{$sCustCountry}', '{$sCustTel}', '{$sCustEmail}', '{$sCustState}', '{$sCustCity}', '{$sCustZip}', '{$sCreatedOn}', '0000-00-00 00:00:00', '0');";

    $rResult = $mysqli->query($sQuery);

    if($rResult) {
        $iUniID = "ODR-{$mysqli->insert_id}";
        return $iUniID;
    }
    else {
        return false;
    }

}


function getCustomParam($iOrderID) {

    $iID = explode("-", $iOrderID);
    $iID = $iID[1];

    $mysqli = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DEFAULT);

    $sQuery = "SELECT custom_param FROM payment_data WHERE id='{$iID}'";

    $rResult = $mysqli->query($sQuery);
     while($row=$rResult->fetch_array())
    {
        $iCustomParaVal =$row[0] ;    
        
      }    
    if($iCustomParaVal!=null) {
       return $iCustomParaVal;
    }
    else {
        return false;
    }
}

function receivePayment($iOrderID, $aResponse) {

    $mysqli = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_DEFAULT);

    if ($mysqli->connect_error) {
        return false;
    }

    $workingKey = WORKING_KEY;


    $AuthDesc=$aResponse['AuthDesc'];
    $MerchantId=$aResponse['Merchant_Id'];
    $OrderId=$aResponse['Order_Id'];
    $Amount=$aResponse['Amount'];
    $Checksum=$aResponse['Checksum'];
    $veriChecksum=false;

    
    $rcvdString=$MerchantId.'|'.$OrderId.'|'.$Amount.'|'.$AuthDesc.'|'.$workingKey;
    $veriChecksum=verifyChecksum(genchecksum($rcvdString), $Checksum);

    $iStatus = 0;
    //$sCustomParam = getCustomParam($OrderId);
    //$sCustomParam = getCustomParam($OrderId);
    
    $sCustomParam=generateUniqueCodeInPayment(getCustomParam($OrderId));
    if($veriChecksum==TRUE && $AuthDesc==="Y")
    {
        $iStatus = 1;
        $sMsg=generateUniqueCodeInPayment("S4");
        $sURL="postUserAPI.php?paymentid=$sCustomParam&msg=$sMsg";
        //sendNotificationEmail();
        //Here you need to put in the routines for a successful 
        //transaction such as sending an email to customer,
        //setting database status, informing logistics etc etc
    }
    else if($veriChecksum==TRUE && $AuthDesc==="B")
    {
        $sMsg=generateUniqueCodeInPayment("S3");
        $iStatus = 2;
        $sURL="postUserAPI.php?paymentid=$sCustomParam&msg=$sMsg";
       // sendNotificationEmail();
        //Here you need to put in the routines/e-mail for a  "Batch Processing" order
        //This is only if payment for this transaction has been made by an American Express Card
        //since American Express authorisation status is available only after 5-6 hours by mail from ccavenue and at the "View Pending Orders"
    }
    else if($veriChecksum==TRUE && $AuthDesc==="N")
    {
        $sMsg=generateUniqueCodeInPayment("E4");
        $iStatus = -1;        
        $sURL="fail-payment.php?paymentid=$sCustomParam&msg=$sMsg";
        
       // sendNotificationEmail();
        //Here you need to put in the routines for a failed
        //transaction such as sending an email to customer
        //setting database status etc etc
    }
    else
    {    
        $sMsg=generateUniqueCodeInPayment("S3");
        $iStatus = -2;        
        $sURL="fail-payment.php?paymentid=$sCustomParam&msg=$sMsg";
        //sendNotificationEmail();
        //Here you need to simply ignore this and dont need
        //to perform any operation in this condition
    }

    $sCurrentDate = date("Y-m-d H:i:s");
    $sResponse = addslashes(base64_encode(serialize($aResponse)));
    $iID = explode("-", $OrderId);
    $iID = $iID[1];
    
    
    $sQuery = "UPDATE payment_data SET payment_status={$iStatus}, response='{$sResponse}', completed_on='{$sCurrentDate}' WHERE id='{$iID}'";

    $rResult = $mysqli->query($sQuery);

    if($rResult) {
       return $sURL;
    }
    else {
        return false;
    }

}

/*
Similar Logic of generateUniqueCode() function in TPN function.php
generateUniqueCodeInPayment() == generateUniqueCode()
*/
 
function generateUniqueCodeInPayment($idValue){
    $sString="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";        
    $aSplitString=str_split($sString);
    $iLength=count($aSplitString);
    $iLength--;
    $sTemp=null;
    $iIndex=null;

    for($ii=0;$ii<4;$ii++){
        for ($ij=0; $ij <5 ; $ij++) { 
            if($ii%2==0){
                $iIndex=rand(0,$iLength);
                $sTemp=$sTemp.$aSplitString[$iIndex];
            }else{
                $iIndex=rand(0,$iLength);
                $sTemp=$sTemp.ord($aSplitString[$iIndex]);
            }
        }

        $sTemp=$sTemp."-";
        if($ii==2){             // Id value is at [3] i.e. Fourth index after exploding string to array by - delimeter.
            $sTemp=$sTemp.$idValue;
            $sTemp=$sTemp."-";
        }
    }

    Return $sTemp;

}

/*
Similar Logic of decriptUniqueCodeInPayment() function in TPN function.php
decriptUniqueCodeInPayment() == decriptUniqueCode()
*/
function decriptUniqueCodeInPayment($sVal){
    $aVal=array();
    $aVal=explode("-", $sVal);
    $sReturnVal=$aVal[3];       // Id Value Index.
    return $sReturnVal;
}  
  


?>