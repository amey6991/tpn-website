<?php 
	error_reporting(0); 
	require_once('config.php'); 
	include('adler32.php'); 
	include('aes.php'); 
	include('functions.php'); 
?>

<html>
<head>
<title> Checkout</title>
</head>
<body>
<center>

<?php include('../function.php'); ?> 
<?php




//var_dump($viewPHR);
$aFields =array();

if(isset($_GET['id'])){ // Pass id for retrieving data before payment.
	$sIdEncBeforePayment=$_GET['id'];
	$sIdBeforePayment=explodArrayForId($sIdEncBeforePayment);

	$aFields=showDataBeforePayment($sIdBeforePayment);
	$sPlanAmount=showPlanAmount($aFields[0]['phrPlan']);	
	$sCustName=$aFields[0]['phrName'];
	$sCustEmail=$aFields[0]['phrEmail'];
	$sCustUID=$aFields[0]['phrUID'];
	$sCustTel=$aFields[0]['phrContact'];
	$sCustAddress=$aFields[0]['phrAddress'];	
	$sCustPincode=$aFields[0]['pincode'];	
	$aTemp=getCityById($aFields[0]['city']);
	$sCustCity=$aTemp[0][0];
	//$aTemp=getAreaById($aFields[0]['phrArea']);
	$sCustArea=$aFields[0]['phrArea'];
	$aTemp=getCountryById($aFields[0]['country']);
	$sCustCountry=$aTemp[0][0];
	$aTemp=getStateById($aFields[0]['state']);
	$sCustState=$aTemp[0][0];
	
}else{
	$aFields=empty($aFields);
}

$merchant_id= MERCHANT_KEY;  // Merchant id(also User_Id) 
$working_key= WORKING_KEY;	//Put in the 32 bit alphanumeric key in the quotes provided here.
$url= REDIRECT_URL;         //your redirect URL where your customer will be redirected after authorisation from CCAvenue


$customParameter = $sIdBeforePayment; //! Store any data that you want to identify. Has to be string!

$amount=$sPlanAmount;//$_POST['Amount'];    
       
$billing_cust_name=$sCustName;//$_POST['billing_cust_name'];
$billing_cust_address=$sCustAddress;//$_POST['billing_cust_address'];
$billing_cust_country=$sCustCountry;//$_POST['billing_cust_country'];
$billing_cust_state=$sCustState;//$_POST['billing_cust_state'];
$billing_city=$sCustCity;//$_POST['billing_city'];
$billing_zip=$sCustPincode;//$_POST['billing_zip'];
$billing_cust_tel=$sCustTel;//$_POST['billing_cust_tel'];
$billing_cust_email=$sCustEmail;//$_POST['billing_cust_email'];


//! Generate a unique order ID
$iUniOID = generateUniqueOrderID($customParameter, $amount, $sCustName, $sCustAddress, $sCustCountry, $sCustTel, $sCustEmail, $sCustState, $sCustCity, $sCustZip);

$order_id = $iUniOID;




$checksum=getchecksum($merchant_id,$amount,$order_id,$url,$working_key); // Method to generate checksum

$merchant_data= 'Merchant_Id='.$merchant_id.'&Amount='.$amount.'&Order_Id='.$order_id.'&Redirect_Url='.$url.'&billing_cust_name='.$billing_cust_name.'&billing_cust_address='.$billing_cust_address.'&billing_cust_country='.$billing_cust_country.'&billing_cust_state='.$billing_cust_state.'&billing_cust_city='.$billing_city.'&billing_zip_code='.$billing_zip.'&billing_cust_tel='.$billing_cust_tel.'&billing_cust_email='.$billing_cust_email.'&Checksum='.$checksum  ;

$encrypted_data=encrypt($merchant_data,$working_key); // Method for encrypting the data.

?>

<form method="post" name="redirect" action="http://www.ccavenue.com/shopzone/cc_details.jsp"> 
<?php
echo "<input type='hidden' name='encRequest' value='$encrypted_data' >";
echo "<input type='hidden' name='Merchant_Id' value='$merchant_id'>";

?>

</form>

</center>
<script language='javascript'>document.redirect.submit();</script>
</body>
</html>
