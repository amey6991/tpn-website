<html>
	<head>
		<title>Pediatric</title>		
	<?php include('header.php'); ?> 
<style>
#idActiveMenu22{
	color: #004480 !important;
	
}
#idActiveMenu22 a{
color: #004480 !important;
}
</style><!--Style for the active Link-->

	</head>

	<body> 
		<!-- Main Div Start-->
		<div class="classDivMain">

			<!--Main Header Div Start-->
			<div class="classDivMainHeader classDivMainHeaderBottomBorder">
				
				<div class="classDivMainHeader1">
			<?php include('body-header.php'); ?>
			<!-- Main Header Div End-->

			<!--Main Body Div Start-->
			<div class="classDivMainBody classDivMainBodyTopMargin" >

				<div class="classMainBody1">
					
					<div id="example" class="classDivTabContainerCustom">
			           
			            <div class="classDivTabContainCustom classTopMargin30"  id="" >

			              <div class="classDivHeading">
			              	Advisory Board
			              </div>	

			              <div class="classDivInnerPageOfABContainer">			              	
			              	
			              	<div class="classDivInnerPageBodyItem">
			              		<div class="classDivLeaderContianer" id="idDivLeaderImgDesig">
				              		<img class="classImgLeader" src="./images/leaders/atish.jpg" />				              		
				              	</div><!-- Left div contain image and designation-->
				              	<div class="classDivLeaderContianer" id="idDivLeaderText">				              		
				              		<p class="classPLeaderDesig"><span class="classSpanLeaderName">Dr. Atish Laddad </span><br/><span class="classSpanLeaderDesig">Director</span></p>
				              		<p class="classPLeaderText"><b>Dr. Atish Laddad </b> 
				              			is the chief pediatrician at Kohinoor Pediatric Superspeciality Centre, Kurla (W) and CuddlesNCare NICU, Andheri (E). He is the brain behind this initiative. He is a gold medallist from KEM Hospital and worked as an assistant professor at Sir JJ Group of Hospitals for 7 years.Newborn intensive care is his forte.
				              		</p>
				              	</div><!-- Left div contain complete text-->
			              	</div><!-- Div for one individual leader -->

			              	
			              </div><!-- Div for all individual leader -->	             

						</div>

					</div>

				</div>

			</div>
			<!-- Main Body Div End-->

			<!--Main Footer Div Start-->
				<?php include('footer.php'); ?>
			<!-- Main Footer Div End-->

		</div>		
		<!-- Main Div End-->		
	</body>

</html>




