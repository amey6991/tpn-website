
$(document).ready(function(){
	
	$(document).ready(function(){
		if($('#membership-plan').val() == ''){
			 //$('#idPhrNameMsg').html("Please Enter Name");
			 window.location.href = "patient-sign-up.php";
			//$('#idPhrName').focus();
			//return false;
		}
	});
	$('#membership-plan').blur(function(){
		if($('#membership-plan').val() == ''){
			 //$('#idPhrNameMsg').html("Please Enter Name");
			 window.location.href = "patient-sign-up.php";
			//$('#idPhrName').focus();
			//return false;
		}
	});


/*
This is to validate cash payment option form (form is on phr-patient-detail-payment.php)
Cash Payment Mode Code Start.
*/
	$('#idPhrCashSubmit').click(function(){
		if($('#idPhrPedName').find(":selected").val()==''||$('#idPhrPedName').find(":selected").val()==''){
			 $('#idPhrPedNameMsg').html("Please Select Pediatrician Name");

			$('#idPhrPedName').focus();
			return false;
		}else{
			$('#idPhrPedNameMsg').html("");			
		}
		if($('#idPhrCash').val() == ''){
			 $('#idPhrCashMsg').html("Please Enter Cash");

			$('#idPhrCash').focus();
			return false;
		}else{
			$('#idPhrCashMsg').html("");			
		}
	});
/*
Cash Payment Mode Code End.
*/


$('#idPhrSubmit').click(function(){

		if($('#idPhrName').val() == ''){
			 $('#idPhrNameMsg').html("Please Enter Name");

			$('#idPhrName').focus();
			return false;
		}else{
			$('#idPhrNameMsg').html("");			
		}
		if($('#idPhrContact').val() == ''){
            $('#idPhrContactMsg').html("Please Enter Contact No.");
            //alert('Please Enter Contact No.');
            $('#idPhrContact').focus();
            return false;
        }else{
			$('#idPhrContactMsg').html("");			
		}
        if($('#idPhrEmail').val() == ''){
            $('#idPhrEmailMsg').html("Please Enter Email Address");
            //alert('Please Enter Email Address');
            $('#idPhrEmail').focus();
            return false;
        }else{
			$('#idPhrEmailMsg').html("");			
		}
		if($('#idPhrUID').val() == ''){
            $('#idPhrUIDMsg').html("Please Enter UID");
            //alert('Please Enter Email Address');
            $('#idPhrUID').focus();
            return false;
        }else{
			$('#idPhrUIDMsg').html("");			
		}		
        if($('#idPhrBdate').val() == ''){
			$('#idPhrBdateMsg').html("Please Enter Birth Date");
            //alert('Please Enter Birth Date');
			$('#idPhrBdate').focus();
			return false;
		}else{
			$('#idPhrBdateMsg').html("");			
		}        
		if($('input[name="phrGender"]:checked').length == 0){
			$('#idPhrGenderMsg').html("Please Select Gender");
            //alert('Please Select Gender');
            $('#idPhrGender').focus();
			return false;
		}else{
			$('#idPhrGenderMsg').html("");			
		}


		/*Validation for Address Detail Start*/

		if($('#idPhrAddress').val() == ''){			
			$('#idPhrAddressMsg').html("Please Enter your Address");
            //alert('Please Enter Birth Date');
			$('#idPhrAddress').focus();
			return false;
		}else{
			$('#idPhrAddressMsg').html("");			
		}


		if($('#idCity').val() == 'select'){
			$('#idCityMsg').html("Select City");
            //alert('Please Enter Birth Date');
			$('#idCity').focus();
			return false;
		}else{
			$('#idCityMsg').html("");			
		}

		if($('#idArea').val() == ''){
			$('#idAreaMsg').html("Please Enter Area");
            //alert('Please Enter Birth Date');
			$('#idArea').focus();
			return false;
		}else{
			$('#idAreaMsg').html("");			
		}

		if($('#pin-code').val() == ''){
			$('#idPincodeMsg').html("Please Enter Pincode");
            //alert('Please Enter Birth Date');
			$('#pin-code').focus();
			return false;
		}else{
			$('#idPincodeMsg').html("");			
		}

		if($('#idState').val() == 'select'){
			$('#idStateMsg').html("Select State");
            //alert('Please Enter Birth Date');
			$('#idState').focus();
			return false;
		}else{
			$('#idStateMsg').html("");			
		}
		
		if($('#idCountry').val() == 'select'){
			$('#idCountryMsg').html("Select Country");
            //alert('Please Enter Birth Date');
			$('#idCountry').focus();
			return false;
		}else{
			$('#idCountryMsg').html("");			
		}

		if($('#idPhrCapcha').val() == 'select'){
			$('#idPhrCapchaMsg').html("Enter Correct Secure Code");
            //alert('Please Enter Birth Date');
			$('#idPhrCapcha').focus();
			return false;
		}else{
			$('#idPhrCapchaMsg').html("");			
		}

        
	});

	$('#idPhrName').blur(function(){
		if($('#idPhrName').val() != ''){

			if (validateTextOnly('idPhrName')) {
				$('#idPhrNameMsg').html("");
			}else{
				$('#idPhrNameMsg').html("Please Enter Alphabetic Value Only");	
				$('#idPhrName').val('');
				return false;
			}
			return false;
		}else{
			$('#idPhrNameMsg').html("Please Enter Name");			
			return false;
		}
	});	

	$('#idPhrContact').blur(function(){
		if($('#idPhrContact').val() != '' || $('#idPhrContact').val() == '' ){

			if (validatePhone('idPhrContact')) {
				$('#idPhrContactMsg').html("");
			}else{
				$('#idPhrContactMsg').html("Please Enter Valid Number");	
				$('#idPhrContact').val('');
				return false;
			}
			return false;
		}else{
			$('#idPhrContactMsg').html("Please Enter Phone Number");			
			return false;
		}
	});		

	$('#idPhrEmail').blur(function(){
		if($('#idPhrEmail').val() != '' || $('#idPhrEmail').val() == '' ){

			if (validateEmail('idPhrEmail')) {
				$('#idPhrEmailMsg').html("");
			}else{
				$('#idPhrEmailMsg').html("Please Enter Valid Email");	
				$('#idPhrEmail').val('');		
				return false;
			}
			return false;
		}else{
			$('#idPhrEmailMsg').html("Please Enter Email");			
			return false;
		}

		//alert("start Working");
	});
	
	/*Patinet Validation End*/

	/*Doctor Sign up Form Validation*/

	$('#idPedFirstName').blur(function(){
		if($('#idPedFirstName').val() != ''){

			if (validateTextOnly('idPedFirstName')) {
				$('#idPedFirstNameMsg').html("");
			}else{
				$('#idPedFirstNameMsg').html("Please Enter Alphabetic Value Only");	
				$('#idPedFirstName').val('');
				return false;
			}
			return false;
		}else{
			$('#idPedFirstNameMsg').html("Please Enter First Name");			
			return false;
		}
	});		

	$('#idPedLastName').blur(function(){
		if($('#idPedLastName').val() != ''){

			if (validateTextOnly('idPedLastName')) {
				$('#idPedLastNameMsg').html("");
			}else{
				$('#idPedLastNameMsg').html("Please Enter Alphabetic Value Only");	
				$('#idPedLastName').val('');
				return false;
			}
			return false;
		}else{
			$('#idPedLastNameMsg').html("Please Enter Last Name");			
			return false;
		}
	});		

	$('#idPedMobileNo').blur(function(){
		if($('#idPedMobileNo').val() != ''){

			if (validatePhone('idPedMobileNo')) {
				$('#idPedMobileNoMsg').html("");
			}else{
				$('#idPedMobileNoMsg').html("Please Enter Valid Number");
				$('#idPedMobileNo').val('');			
				return false;
			}
			return false;
		}else{
			$('#idPedMobileNoMsg').html("Please Enter Phone Number");			
			return false;
		}
	});		

	$('#idPedEmailid').blur(function(){
		if($('#idPedEmailid').val() != ''){

			if (validateEmail('idPedEmailid')) {
				$('#idPedEmailidMsg').html("");
			}else{
				$('#idPedEmailidMsg').html("Please Enter Valid Email");		
				$('#idPedEmailid').val('');	
				return false;
			}
			return false;
		}else{
			$('#idPedEmailidMsg').html("Please Enter Email");			
			return false;
		}

		//alert("start Working");
	});

	$('#idPedSubmit').click(function(){

		if($('#idPedFirstName').val() == ''){
			
			 $('#idPedFirstNameMsg').html("Please Enter First Name");

			$('#idPedFirstName').focus();
			return false;
		}else{
			
			$('#idPedFirstNameMsg').html("");			
		}
		if($('#idPedLastName').val() == ''){
            $('#idPedLastNameMsg').html("Please Enter Last Name");
            //alert('Please Enter Contact No.');
            $('#idPedLastName').focus();
            return false;
        }else{
			$('#idPhrContactMsg').html("");			
		}
		if($('#idPedSpecializtn').val() == 'select'){
            $('#idPedSpecializtnMsg').html("Please Enter Specialization");
            //alert('Please Enter Email Address');
            $('#idPedSpecializtn').focus();
            return false;
        }else{
			$('#idPedSpecializtnMsg').html("");			
		}

		if($('#idPedMCICode').val() == ''){
            $('#idPedMCICodeMsg').html("Please Enter MCI Code");
            //alert('Please Enter Email Address');
            $('#idPedMCICode').focus();
            return false;
        }else{
			$('#idPedMCICodeMsg').html("");			
		}

		if($('#idPedGender').val() == 'select'){
			$('#idPedGenderMsg').html("Please Select Gender");
            //alert('Please Select Gender');
            $('#idPedGender').focus();
			return false;
		}else{
			$('#idPedGenderMsg').html("");			
		}

		 if($('#dob').val() == ''){
			$('#idPedDOBMsg').html("Please Enter Birth Date");
            //alert('Please Enter Birth Date');
			$('#dob').focus();
			return false;
		}else{
			$('#idPedDOBMsg').html("");			
		}   

		if($('#idPedMobileNo').val() == ''){
            $('#idPedMobileNoMsg').html("Please Enter Mobile Number");
            //alert('Please Enter Email Address');
            $('#idPedMobileNo').focus();
            return false;
        }else{
			$('#idPedMobileNoMsg').html("");			
		}
		
		if($('#idPedEmailid').val() == ''){
            $('#idPedEmailidMsg').html("Please Enter Email Id");
            //alert('Please Enter Email Address');
            $('#idPedEmailid').focus();
            return false;
        }else{
			$('#idPedEmailidMsg').html("");			
		}

        if($('#idPedAddress').val() == ''){
            $('#idPedAddressMsg').html("Please Enter Address");
            //alert('Please Enter Email Address');
            $('#idPedAddress').focus();
            return false;
        }else{
			$('#idPedAddressMsg').html("");			
		}

		if($('#city').val() == 'select'){
            $('#idPedCityMsg').html("Select City");
            //alert('Please Enter Email Address');
            $('#city').focus();
            return false;
        }else{
			$('#idPedCityMsg').html("");			
		}

		if($('#area').val() == 'select'){
            $('#idPedAreaMsg').html("Select Area");
            //alert('Please Enter Email Address');
            $('#area').focus();
            return false;
        }else{
			$('#idPedAreaMsg').html("");			
		}
			
		if($('#pin-code').val() == 'select'){
			$('#idPincodeMsg').html("Select Pincode");
            //alert('Please Enter Birth Date');
			$('#pin-code').focus();
			return false;
		}else{
			$('#idPincodeMsg').html("");			
		}

		if($('#idPedCapcha').val() == ''){
			$('#idPedCapchaMsg').html("Please Enter Secure Code");
            //alert('Please Enter Birth Date');
			$('#idPedCapcha').focus();
			return false;
		}else{
			$('#idPedCapchaMsg').html("");			
		}

	}); /* Doctor Validation End*/

	
	/*Pharmacist Validation Start*/

	$('#idPharmaName1').blur(function(){
		if($('#idPharmaName1').val() != ''){

			if (validateTextOnly('idPharmaName1')) {
				$('#idPharmaName1Msg').html("");
			}else{
				$('#idPharmaName1Msg').html("Please Enter Alphabetic Value Only");	
				$('#idPharmaName1').val('');
				return false;
			}
			return false;
		}else{
			$('#idPharmaName1Msg').html("Please Enter Name");			
			return false;
		}
	});		


	$('#idPharmaContact').blur(function(){
		if($('#idPharmaContact').val() != ''){

			if (validatePhone('idPharmaContact')) {
				$('#idPharmaContactMsg').html("");
			}else{
				$('#idPharmaContactMsg').html("Please Enter Valid Number");	
				$('#idPharmaContact').val('');
				return false;
			}
			return false;
		}else{
			$('#idPharmaContactMsg').html("Please Enter Phone Number");			
			return false;
		}
	});		

	$('#idPharmaEmail').blur(function(){
		if($('#idPharmaEmail').val() != ''){

			if (validateEmail('idPharmaEmail')) {
				$('#idPharmaEmailMsg').html("");
			}else{
				$('#idPharmaEmailMsg').html("Please Enter Valid Email");	
				$('#idPharmaEmail').val('');		
				return false;
			}
			return false;
		}else{
			$('#idPharmaEmailMsg').html("Please Enter Email");			
			return false;
		}

		//alert("start Working");
	});

	$('#idPharmaContact1').blur(function(){
		if($('#idPharmaContact1').val() != ''){

			if (validatePhone('idPharmaContact1')) {
				$('#idPharmaContact1Msg').html("");
			}else{
				$('#idPharmaContact1Msg').html("Please Enter Valid Number");	
				$('#idPharmaContact1').val('');
				return false;
			}
			return false;
		}else{
			$('#idPharmaContact1Msg').html("Please Enter Phone Number");			
			return false;
		}
	});		

	$('#idPharmaEmail1').blur(function(){
		if($('#idPharmaEmail1').val() != ''){

			if (validateEmail('idPharmaEmail1')) {
				$('#idPharmaEmail1Msg').html("");
			}else{
				$('#idPharmaEmail1Msg').html("Please Enter Valid Email");	
				$('#idPharmaEmail1').val('');		
				return false;
			}
			return false;
		}else{
			$('#idPharmaEmail1Msg').html("Please Enter Email");			
			return false;
		}

		//alert("start Working");
	});

	$('#idPharmaSubmit').click(function(){

		if($('#idPharmaName').val() == ''){
			
			 $('#idPharmaNameMsg').html("Please Enter Name");

			$('#idPharmaName').focus();
			return false;
		}else{
			
			$('#idPharmaNameMsg').html("");			
		}
		
		if($('#idPharmaContact').val() == ''){
            $('#idPharmaContactMsg').html("Please Enter Mobile Number");
            //alert('Please Enter Email Address');
            $('#idPharmaContact').focus();
            return false;
        }else{
			$('#idPharmaContactMsg').html("");			
		}


		if($('#idPharmaEmail').val() == ''){
            $('#idPharmaEmailMsg').html("Please Enter Email Id");
            //alert('Please Enter Email Address');
            $('#idPharmaEmail').focus();
            return false;
        }else{
			$('#idPharmaEmailMsg').html("");			
		}

		if($('#idPharmaName1').val() == ''){
			
			 $('#idPharmaName1Msg').html("Please Enter Name");

			$('#idPharmaName1').focus();
			return false;
		}else{
			
			$('#idPharmaName1Msg').html("");			
		}

		if($('#idPharmaContact1').val() == ''){
            $('#idPharmaContact1Msg').html("Please Enter Mobile No.");
            //alert('Please Enter Email Address');
            $('#idPharmaContact1').focus();
            return false;
        }else{
			$('#idPharmaContact1Msg').html("");			
		}

		if($('#idPharmaDesignation1').val() == ''){
			$('#idPharmaDesignation1Msg').html("Please Enter Designation");
            //alert('Please Enter Birth Date');
			$('#idPharmaDesignation1').focus();
			return false;
		}else{
			$('#idPharmaDesignation1Msg').html("");			
		}

		if($('#idPharmaEmail1').val() == ''){
            $('#idPharmaEmail1Msg').html("Please Enter Email Id");
            //alert('Please Enter Email Address');
            $('#idPharmaEmail1').focus();
            return false;
        }else{
			$('#idPharmaEmail1Msg').html("");			
		}
		
		
		if($('#idPharmaArea').val() == 'select'){
			$('#idPharmaAreaMsg').html("Please Enter Area");
            //alert('Please Select Gender');
            $('#idPharmaArea').focus();
			return false;
		}else{
			$('#idPharmaAreaMsg').html("");			
		}
		
        if($('#idPharmaAddress').val() == ''){
            $('#idPharmaAddressMsg').html("Please Enter Address");
            //alert('Please Enter Email Address');
            $('#idPharmaAddress').focus();
            return false;
        }else{
			$('#idPharmaAddressMsg').html("");			
		}

		if($('#idPharmaCapcha').val() == ''){
            $('#idPharmaCapchaMsg').html("Please Enter Secure Code");
            //alert('Please Enter Email Address');
            $('#idPharmaCapcha').focus();
            return false;
        }else{
			$('#idPharmaCapchaMsg').html("");			
		}
		
		

	});

	/*Pharmacist Validation End*/
	

});


function validateTextOnly(txtOnlyId){
	var textOnly = document.getElementById(txtOnlyId).value;
    var filter = /^[a-zA-Z\s]+$/;
    if (filter.test(textOnly)) {
        return true;
    }
    else {
        return false;
    }
}

function validatePhone(txtPhone) {
    var contactNumber = document.getElementById(txtPhone).value;
    var filter = /^[0-9-+]+$/;
    if (filter.test(contactNumber) && ((contactNumber.length > 9)&&(contactNumber.length < 11))) {
        return true;
    }
    else {
        return false;
    }
}

function validateEmail(txtEmail) {
    var email = document.getElementById(txtEmail).value;
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
        return true;
    }
    else {
        return false;
    }
}

function showFileUpload(fileid){
var fupload = document.getElementById(fileid);
var fileName = fupload.value;
if(fileName==''){
	return false;
}else{
	return true;
}
//alert(fileName);
//document.getElementById(spanid).innerHTML=fileName+' added';
}